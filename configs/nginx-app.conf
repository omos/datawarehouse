upstream django {
    server unix:/tmp/datawarehouse-app.sock;
}

server {
    listen 8000 default_server;

    access_log  /logs/nginx-access.json.log json_analytics;
    access_log  /logs/nginx-access.log;
    error_log   /logs/nginx-error.log;

    client_max_body_size 10M;

    server_tokens       off;
    add_header X-XSS-Protection "1; mode=block";
    add_header Content-Security-Policy "default-src 'self';
                                        font-src 'self' fonts.gstatic.com use.fontawesome.com;
                                        img-src 'self' seccdn.libravatar.org;
                                        script-src 'self' 'unsafe-inline' cdnjs.cloudflare.com;
                                        style-src 'self' 'unsafe-inline' fonts.googleapis.com use.fontawesome.com;";
    add_header Permissions-Policy "geolocation=(),
                                   midi=(),
                                   sync-xhr=(),
                                   microphone=(),
                                   camera=(),
                                   magnetometer=(),
                                   gyroscope=(),
                                   fullscreen=(),
                                   payment=()";
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";

    gzip            on;
    gzip_vary       on;
    gzip_proxied    no-cache no-store private expired auth;
    gzip_types      text/plain text/css application/json text/javascript application/javascript;

    location /static {
        # Return "405 Method Not Allowed" for non allowed methods.
        if ( $request_method !~ ^(GET|HEAD)$ ) {
            return 405;
        }
        expires 30d;
        alias /code/static;
    }

    location / {
        # Return "405 Method Not Allowed" for non allowed methods.
        if ( $request_method !~ ^(GET|HEAD|OPTIONS|PUT|POST)$ ) {
            return 405;
        }
        uwsgi_pass  django;
        uwsgi_read_timeout 600;
        include    /etc/nginx/conf.d/uwsgi_params;
    }
}
