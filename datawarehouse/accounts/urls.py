"""Account urls."""
from django.conf import settings
from django.urls import include
from django.urls import path

from datawarehouse.accounts import views

urlpatterns = [
    # https://docs.djangoproject.com/en/dev/topics/auth/default/#using-the-views
    path('-/', include('django.contrib.auth.urls')),
    path('-/delete', views.user_delete, name='account.user.delete'),
    path('-/settings', views.user_settings, name='account.user.settings'),
    path('-/subscriptions', views.user_subscriptions, name='account.user.subscriptions'),
    path('<str:username>', views.user_get, name='account.user.get'),
]

if settings.FF_SIGNUP_ENABLED:
    urlpatterns.extend([
        path('-/signup', views.user_signup, name='account.signup'),
        path('-/signup/done', views.user_signup_done, name='account.signup.done'),
        path('-/signup/complete/<uidb64>/<token>', views.user_signup_complete, name='account.signup.complete'),
    ])
