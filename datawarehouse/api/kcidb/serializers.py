# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# pylint: disable=no-self-use
"""Serializers."""
from rest_framework import serializers

from datawarehouse import models

VERSION = {'major': 4, 'minor': 0}


class NoEmptyFieldSerializer(serializers.ModelSerializer):
    """Don't serialize empty fields."""

    @staticmethod
    def _value_is_empty(value):
        """Return True if the value is empty."""
        return (
            value is None or
            value == {} or
            value == []
        )

    def to_representation(self, instance):
        """Filter out empty values from the representation."""
        result = super().to_representation(instance)
        return {
            key: value for key, value in result.items()
            if not self._value_is_empty(value)
        }


class KCIDBPatchMBOXSerializer(serializers.ModelSerializer):
    """Serializer for Patch KCIDB styled."""

    name = serializers.CharField(source='subject', read_only=True)

    class Meta:
        """Metadata."""

        model = models.Patch
        fields = ('name', 'url')


class KCIDBFileSerializer(serializers.ModelSerializer):
    """Serializer for File KCIDB styled."""

    class Meta:
        """Metadata."""

        model = models.Artifact
        fields = ('url', 'name')


class KCIDBCheckoutSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBCheckout."""

    origin = serializers.CharField(source='origin.name')
    tree_name = serializers.CharField(source='tree.name', read_only=True)
    patchset_files = KCIDBPatchMBOXSerializer(many=True, source='patches')
    log_url = serializers.CharField(source='log.url', read_only=True)
    contacts = serializers.SerializerMethodField()
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBCheckout
        fields = ('id', 'origin', 'tree_name',
                  'git_repository_url', 'git_repository_branch',
                  'git_commit_hash', 'git_commit_name',
                  'patchset_files', 'patchset_hash', 'message_id', 'comment',
                  'start_time', 'log_url', 'log_excerpt',
                  'contacts', 'valid', 'misc',
                  )

    def get_contacts(self, checkout):
        """Return contacts as a flat list of emails."""
        return list(checkout.contacts.values_list('email', flat=True))

    def get_misc(self, checkout):
        """Return misc field."""
        return {
            'kcidb': {'version': VERSION},
            'iid': checkout.iid,
            'is_public': checkout.is_public,
            'nvr': checkout.kernel_version,
        }


class KCIDBBuildSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBBuild."""

    checkout_id = serializers.CharField(source='checkout.id')
    origin = serializers.CharField(source='origin.name')
    architecture = serializers.SerializerMethodField()
    log_url = serializers.CharField(source='log.url', read_only=True)
    compiler = serializers.CharField(source='compiler.name', read_only=True)
    input_files = KCIDBFileSerializer(many=True)
    output_files = KCIDBFileSerializer(many=True)
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBBuild
        fields = ('checkout_id', 'id', 'origin', 'comment',
                  'start_time', 'duration', 'architecture',
                  'command', 'compiler', 'input_files', 'output_files', 'config_name',
                  'config_url', 'log_url', 'log_excerpt', 'valid', 'misc',
                  )

    def get_architecture(self, obj):
        """Return architecture name."""
        return obj.get_architecture_display()

    def get_revision_id(self, build):
        """
        Return compatibility revision_id.

        This is necessary for KCIDB Upstream Forwarder service, and
        should be removed once upstream handles v4 schema.
        """
        revision_id = build.checkout.git_commit_hash
        if build.checkout.patchset_hash:
            revision_id += '+' + build.checkout.patchset_hash

        return revision_id

    def get_misc(self, build):
        """Return misc field."""
        return {
            'kcidb': {'version': VERSION},
            'iid': build.iid,
            'is_public': build.is_public,
            'revision_id': self.get_revision_id(build),
        }


class EnvironmentSerializer(serializers.ModelSerializer):
    """Serializer for Environment (BeakerResource)."""

    comment = serializers.CharField(source='fqdn')

    class Meta:
        """Metadata."""

        model = models.BeakerResource
        fields = ('comment', )


class KCIDBTestSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBTest."""

    build_id = serializers.CharField(source='build.id')
    origin = serializers.CharField(source='origin.name')
    environment = EnvironmentSerializer()
    comment = serializers.CharField(source='test.name', read_only=True)
    path = serializers.CharField(source='test.universal_id', read_only=True)
    status = serializers.SerializerMethodField()
    output_files = KCIDBFileSerializer(many=True)
    misc = serializers.SerializerMethodField()
    log_url = serializers.CharField(source='log.url', read_only=True)

    class Meta:
        """Metadata."""

        model = models.KCIDBTest
        fields = ('build_id', 'id', 'origin', 'environment',
                  'path', 'comment', 'status', 'waived',
                  'start_time', 'duration', 'output_files', 'misc',
                  'log_url', 'log_excerpt',
                  )

    def get_status(self, obj):
        """Return status name."""
        return obj.get_status_display()

    def get_misc(self, test):
        """Return misc field."""
        misc = {
            'kcidb': {'version': VERSION},
            'iid': test.iid,
            'is_public': test.is_public,
        }

        beaker_task = test.beakertask_set.last()
        if beaker_task:
            misc.update({
                'beaker': {
                    'task_id': beaker_task.task_id,
                    'recipe_id': beaker_task.recipe_id,
                }
            })

        return misc
