"""Urls file."""
from django.urls import path

from . import views

urlpatterns = [
    path('builds/<str:id>',
         views.BuildGet.as_view(), name='kcidb.builds.get'),
    path('builds/<str:id>/issues',
         views.BuildIssueOccurrence.as_view(), name='kcidb.builds.issues'),
    path('builds/<str:id>/issues/<int:issue_id>',
         views.BuildIssueOccurrence.as_view(), name='kcidb.builds.issues'),
    path('builds/<str:id>/tests',
         views.TestList.as_view(), name='kcidb.tests.list'),
    path('checkouts',
         views.CheckoutList.as_view(), name='kcidb.checkouts.list'),
    path('checkouts/-/reports/missing',
         views.CheckoutReportMissing.as_view(), name='kcidb.checkouts.reports.missing'),
    path('checkouts/<str:id>',
         views.CheckoutGet.as_view(), name='kcidb.checkouts.get'),
    path('checkouts/<str:id>/builds',
         views.BuildList.as_view(), name='kcidb.builds.list'),
    path('checkouts/<str:id>/issues',
         views.CheckoutIssueOccurrence.as_view(), name='kcidb.checkouts.issues'),
    path('checkouts/<str:id>/issues/<int:issue_id>',
         views.CheckoutIssueOccurrence.as_view(), name='kcidb.checkouts.issues'),
    path('checkouts/<str:id>/reports',
         views.CheckoutReport.as_view(), name='kcidb.checkouts.reports'),
    path('checkouts/<str:id>/reports/<str:report_id>',
         views.CheckoutReport.as_view(), name='kcidb.checkouts.reports'),
    path('tests/<str:id>',
         views.TestGet.as_view(), name='kcidb.tests.get'),
    path('tests/<str:id>/issues',
         views.TestIssueOccurrence.as_view(), name='kcidb.tests.issues'),
    path('tests/<str:id>/issues/<int:issue_id>',
         views.TestIssueOccurrence.as_view(), name='kcidb.tests.issues'),
    path('submit', views.Submit.as_view()),
]
