"""Context processors."""
from django.conf import settings


def settings_values(_):
    """Add values from the settings module into the templates context."""
    data = {
        'PRIVACY_POLICY_URL': settings.PRIVACY_POLICY_URL,
        'FF_SIGNUP_ENABLED': settings.FF_SIGNUP_ENABLED,
        'FF_SAML_ENABLED': settings.FF_SAML_ENABLED,
        'DEFAULT_USER_IMAGE': settings.DEFAULT_USER_IMAGE,
        'DEFAULT_CACHE_TTL_S': settings.DEFAULT_CACHE_TTL_S,
    }
    if settings.FF_SAML_ENABLED:
        data['SAML_NAME'] = settings.SAML_NAME

    return data
