# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Cron file."""
from captcha.models import CaptchaStore
from celery import shared_task
from cki_lib.logger import get_logger
from django.conf import settings
from django.contrib.auth import get_user_model
from django.template import loader
from django.utils import timezone

from datawarehouse import models
from datawarehouse import scripts
from datawarehouse.utils import notify_user

LOGGER = get_logger(__name__)


def delete_expired_artifacts():
    """Delete expired artifacts."""
    to_delete = models.Artifact.objects.filter(expiry_date__lt=timezone.now())
    LOGGER.info("Deleting %i expired artifacts", to_delete.count())
    to_delete.delete()


def delete_old_not_confirmed_users():
    """Delete users older than setup amount days that are not confirmed."""
    if not settings.FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS:
        return
    old_time = timezone.now() - timezone.timedelta(days=settings.FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS)
    users = get_user_model().objects.filter(last_login=None, date_joined__lt=old_time)
    LOGGER.info('Deleting not confirmed users: %s', ", ".join([u.username for u in users]))
    users.delete()


def send_account_deletion_warning():
    """Send mail to accounts that are going to be deleted."""
    if not settings.FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS:
        return
    half_days = int(settings.FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS/2)
    half_old_time = timezone.now() - timezone.timedelta(days=half_days)
    users = get_user_model().objects.filter(last_login=None,
                                            date_joined__lt=half_old_time,
                                            preferences__account_deletion_warning_sent_at=None)
    for user in users:
        user_pref, _ = models.UserPreferences.objects.get_or_create(user=user)
        email_template = loader.get_template('registration/user_delete_warning_email.html')

        email_context = {
            "user": user,
            "days": half_days
        }

        notify_user(
            subject="DataWarehouse account deletion notification",
            message=email_template.render(email_context),
            user=user
        )

        user_pref.account_deletion_warning_sent_at = timezone.now()
        user_pref.save()


@shared_task
def run():
    """Run cron_ tasks."""
    delete_expired_artifacts()
    delete_old_not_confirmed_users()
    send_account_deletion_warning()
    CaptchaStore.remove_expired()
    scripts.update_ldap_group_members()
