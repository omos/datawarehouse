# Generated by Django 3.2.6 on 2021-08-19 07:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0058_issue_policy_auto_public'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='issue',
            name='generic',
        ),
    ]
