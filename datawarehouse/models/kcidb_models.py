"""KCIDB schema models file."""
from email.utils import parseaddr

from cki_lib.misc import get_nested_key
from django.conf import settings
from django.db import models
from django.urls import reverse
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse import signals
from datawarehouse import utils
from datawarehouse.models import authorization_models
from datawarehouse.models import file_models
from datawarehouse.models import issue_models
from datawarehouse.models import patch_models
from datawarehouse.models import pipeline_models
from datawarehouse.models import test_models
from datawarehouse.models.utils import GenericNameManager
from datawarehouse.models.utils import Manager
from datawarehouse.models.utils import Model


class ObjectStatusEnum(models.TextChoices):
    """Status of objects in messages."""

    NEW = 'new'
    NEEDS_TRIAGE = 'needs_triage'
    READY_TO_REPORT = 'ready_to_report'
    UPDATED = 'updated'


class KernelTypeEnum(models.TextChoices):
    """Kernel type."""

    INTERNAL = 'i'
    UPSTREAM = 'u'


class MissingParent(Exception):
    """Object's parent is missing."""

    def __init__(self, obj_cls, obj_id):
        """Craft exception message."""
        super().__init__(f'{obj_cls.__name__} id={obj_id} is not present in the DB')


def is_cki_submission(data):
    """Return True if this data matches a cki pipeline."""
    misc = data.get('misc', {})
    return {'pipeline', 'job'}.issubset(set(misc.keys()))


class Maintainer(EMOM('maintainer'), models.Model):
    """Model for Maintainer."""

    name = models.CharField(max_length=100)
    email = models.EmailField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} <{self.email}>'

    @classmethod
    def create_from_address(cls, address):
        """Create Maintainer from address."""
        name, email = parseaddr(address)
        maintainer = cls.objects.update_or_create(
            email=email,
            defaults={'name': name},
        )[0]
        return maintainer


class KCIDBOrigin(EMOM('kcidb_origin'), models.Model):
    """Model for KCIDBOrigin."""

    name = models.CharField(max_length=100, unique=True)

    objects = GenericNameManager()

    def __str__(self):
        """Return __str__ formatted."""
        return self.name

    @classmethod
    def create_from_string(cls, name):
        """Create KCIDBOrigin from string."""
        return cls.objects.get_or_create(name=name)[0]


class KCIDBCheckoutManager(Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBCheckout."""

    def aggregated(self):
        # pylint: disable=too-many-locals
        """Add aggregated information."""
        checkout = KCIDBCheckout.objects.filter(iid=models.OuterRef('iid'))
        checkout_untriaged = checkout.filter(
            valid=False,
            issues=None
        )

        builds_run = KCIDBBuild.objects.filter(checkout__iid=models.OuterRef('iid'))
        builds_running = builds_run.filter(valid=None)
        builds_pass = builds_run.filter(valid=True)
        builds_fail = builds_run.filter(valid=False)
        builds_untriaged = builds_fail.filter(issues=None)

        tests_run = KCIDBTest.objects.filter(build__checkout__iid=models.OuterRef('iid'))
        tests_running = tests_run.filter(status=None)
        tests_pass = tests_run.filter(status=test_models.ResultEnum.PASS)
        tests_skip = tests_run.filter(status=test_models.ResultEnum.SKIP)
        tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL, waived=False)
        tests_error = tests_run.filter(status=test_models.ResultEnum.ERROR)
        tests_untriaged = (
            KCIDBTest.objects
            .filter(
                build__checkout__iid=models.OuterRef('iid'),
                status__in=KCIDBTest.UNSUCCESSFUL_STATUSES,
                issues=None
            )
        )
        tests_targeted = tests_run.filter(targeted=True)

        counts = {
            'builds_run': builds_run,
            'builds_running': builds_running,
            'builds_pass': builds_pass,
            'builds_fail': builds_fail,
            'tests_run': tests_run,
            'tests_running': tests_running,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_fail': tests_fail,
            'tests_error': tests_error,
        }

        # Look for issues to determine if there are objects triaged.
        checkout_issues = issue_models.Issue.objects.filter(
            kcidbcheckout__iid=models.OuterRef('iid')
        )
        build_issues = issue_models.Issue.objects.filter(
            kcidbbuild__checkout__iid=models.OuterRef('iid')
        )
        test_issues = issue_models.Issue.objects.filter(
            kcidbtest__build__checkout__iid=models.OuterRef('iid')
        )

        annotations = {
            # Child objects passed
            'stats_builds_passed': ~models.Exists(builds_fail),
            'stats_tests_passed': ~models.Exists(tests_fail),

            # Objects have issues
            'stats_checkout_triaged': models.Exists(checkout_issues),
            'stats_builds_triaged': models.Exists(build_issues),
            'stats_tests_triaged': models.Exists(test_issues),

            # Objects have failures without issues
            'stats_checkout_untriaged': models.Exists(checkout_untriaged),
            'stats_builds_untriaged': models.Exists(builds_untriaged),
            'stats_tests_untriaged': models.Exists(tests_untriaged),

            # Checkout has targeted tests
            'stats_tests_targeted': models.Exists(tests_targeted),
            # Checkout has tests with errors
            'stats_tests_errors': models.Exists(tests_error),
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('iid').annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def annotated_by_architecture(self):
        # pylint: disable=too-many-locals
        """Add build and test information by architecture."""
        counts = {}
        for arch in pipeline_models.ArchitectureEnum:
            builds_run = (
                KCIDBBuild.objects
                .filter(checkout__iid=models.OuterRef('iid'), architecture=arch)
                .exclude(valid=None)
            )
            builds_fail = builds_run.filter(valid=False)
            tests_run = (
                KCIDBTest.objects
                .filter(build__checkout__iid=models.OuterRef('iid'), build__architecture=arch)
                .exclude(status=None)
            )

            counts.update({
                f'{arch.name}_builds_ran': builds_run,
                f'{arch.name}_builds_failed': builds_fail,
                f'{arch.name}_builds_with_issues': builds_fail.exclude(issues=None),
                f'{arch.name}_tests_ran': tests_run,
                f'{arch.name}_tests_failed': tests_run.filter(status=test_models.ResultEnum.FAIL, waived=False),
                f'{arch.name}_tests_failed_waived': tests_run.filter(status=test_models.ResultEnum.FAIL, waived=True),
                f'{arch.name}_tests_with_issues': tests_run.exclude(issues=None),
            })

        annotations = {}
        for name, query in counts.items():
            count = query.values('iid').annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBCheckout(EMOM('kcidb_checkout'), Model):
    """Model for KCIDBCheckout."""

    iid = models.AutoField(primary_key=True)

    id = models.CharField(max_length=300, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    tree = models.ForeignKey('GitTree', on_delete=models.PROTECT, null=True, blank=True)
    git_repository_url = models.URLField(max_length=400, null=True, blank=True)
    git_repository_branch = models.CharField(max_length=200, null=True, blank=True)
    git_commit_hash = models.CharField(max_length=40, null=True, blank=True)
    git_commit_name = models.CharField(max_length=100, null=True, blank=True)
    patches = models.ManyToManyField('Patch', related_name='checkouts', blank=True)
    patchset_hash = models.CharField(max_length=64, null=True, blank=True)
    message_id = models.CharField(null=True, blank=True, max_length=200)
    comment = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    contacts = models.ManyToManyField('Maintainer', related_name='checkouts', blank=True)
    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    log_excerpt = models.TextField(null=True, blank=True)
    valid = models.BooleanField(null=True)

    # Fields not belonging to KCIDB schema
    kernel_type = models.CharField(max_length=1, choices=KernelTypeEnum.choices, null=True, blank=True)
    kernel_version = models.CharField(max_length=100, blank=True, null=True)
    issues = models.ManyToManyField(
        to='Issue',
        through='IssueOccurrence',
        # IssueOccurrence is related to KCIDBCheckout twice, so it's
        # necessary to specify which of them should be used in through_fields.
        through_fields=('kcidb_checkout', 'issue')
    )

    policy = models.ForeignKey(authorization_models.Policy, on_delete=models.PROTECT,
                               null=True, blank=True)
    objects = KCIDBCheckoutManager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'policy'

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('-iid',)

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.checkouts', args=[self.iid])

    @property
    def is_public(self):
        """Return True if this KCIDBCheckout is public."""
        return self.kernel_type == KernelTypeEnum.UPSTREAM

    @property
    def is_triaged(self):
        """Return True if this checkout is triaged."""
        if hasattr(self, 'stats_checkout_triaged'):
            return self.stats_checkout_triaged  # pylint: disable=no-member

        return self.issues.exists()

    @property
    def is_missing_triage(self):
        """Return True if this checkout is missing triage."""
        if hasattr(self, 'stats_checkout_untriaged'):
            return self.stats_checkout_untriaged  # pylint: disable=no-member

        return not self.valid and not self.issues.exists()

    @property
    def builds_triaged(self):
        """Return list of triaged builds."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_builds_triaged') and not self.stats_builds_triaged:
            return KCIDBBuild.objects.none()

        return self.kcidbbuild_set.exclude(issues=None)

    @property
    def builds_untriaged(self):
        """Return list of untriaged builds."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_builds_untriaged') and not self.stats_builds_untriaged:
            return KCIDBBuild.objects.none()

        return self.kcidbbuild_set.filter_untriaged()

    @property
    def tests_triaged(self):
        """Return list of triaged tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_triaged') and not self.stats_tests_triaged:
            return KCIDBTest.objects.none()

        return (
            KCIDBTest.objects
            .filter(build__checkout=self)
            .exclude(issues=None)
        )

    @property
    def tests_untriaged(self):
        """Return list of untriaged tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_untriaged') and not self.stats_tests_untriaged:
            return KCIDBTest.objects.none()

        return (
            KCIDBTest.objects
            .filter_untriaged()
            .filter(build__checkout=self)
        )

    @property
    def has_objects_missing_triage(self):
        """Return if there are failures missing triage."""
        try:
            return (
                self.stats_checkout_untriaged or
                self.stats_builds_untriaged or
                self.stats_tests_untriaged
            )
        except AttributeError:
            return None

    @property
    def has_objects_with_issues(self):
        """Return if the checkout or one of it's builds and tests has any issues."""
        try:
            return (
                self.stats_checkout_triaged or
                self.stats_builds_triaged or
                self.stats_tests_triaged
            )
        except AttributeError:
            return None

    @property
    def has_targeted_tests(self):
        """Return True if the checkout has targeted tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_targeted'):
            return self.stats_tests_targeted

        return (
            KCIDBTest.objects
            .filter(build__checkout=self, targeted=True)
            .exists()
        )

    @property
    def tests_passed(self):
        # pylint: disable=no-member
        """Return True if all tests passed."""
        if self.stats_tests_running_count:
            return None
        return not bool(self.stats_tests_fail_count)

    @classmethod
    def create_from_json(cls, data):
        # pylint: disable=too-many-locals
        """Create KCIDBCheckout from kcidb json."""
        misc = data.get('misc', {})

        log_url = data.get('log_url')
        log = file_models.Artifact.create_from_url(log_url) if log_url else None

        tree_name = data.get('tree_name')
        tree = pipeline_models.GitTree.create_from_string(tree_name) if tree_name else None

        origin = KCIDBOrigin.create_from_string(data['origin'])

        checkout, created = cls.objects.update_or_create(
            id=data['id'],
            origin=origin,
            # for an empty data dict, all of these have to be None
            defaults=utils.clean_dict({
                'tree': tree,
                'git_repository_url': data.get('git_repository_url'),
                'git_repository_branch': data.get('git_repository_branch'),
                'git_commit_hash': data.get('git_commit_hash'),
                'git_commit_name': data.get('git_commit_name'),
                'message_id': data.get('message_id'),
                'comment': data.get('comment'),
                'start_time': data.get('start_time'),
                'valid': data.get('valid'),
                'log': log,
                'log_excerpt': data.get('log_excerpt'),
                'kernel_version': get_nested_key(misc, 'job/kernel_version'),
                'patchset_hash': data.get('patchset_hash'),
            })
        )

        if created:
            # Set default values, should not be overwritten when resubmitted without these fields
            checkout.kernel_type = KernelTypeEnum[
                get_nested_key(misc, 'pipeline/variables/kernel_type', 'internal').upper()
            ]
            if checkout.kernel_type == KernelTypeEnum.UPSTREAM:
                checkout.policy = authorization_models.Policy.objects.get(name=authorization_models.Policy.PUBLIC)
            else:
                checkout.policy = authorization_models.Policy.objects.get(name=authorization_models.Policy.INTERNAL)

            checkout.save()

        for contact in data.get('contacts', []):
            maintainer = Maintainer.create_from_address(contact)
            checkout.contacts.add(maintainer)

        # Create the patch instances depending on the cki_pipeline_type.
        patches_data = data.get('patchset_files', [])
        if patches_data:
            patch_urls = [patch['url'] for patch in patches_data]
            patches = patch_models.Patch.create_from_urls(patch_urls)
            checkout.patches.set(patches)

        if is_cki_submission(data):
            pipeline_models.GitlabJob.create_from_misc(misc, kcidb_checkout=checkout)

        signals.kcidb_object.send(
            sender='kcidb.checkout.create_from_json',
            status=ObjectStatusEnum.NEW if created else ObjectStatusEnum.UPDATED,
            object_type='checkout',
            objects=[checkout],
        )

        return checkout


class KCIDBBuildManager(Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBBuild."""

    def aggregated(self):
        """Add aggregated information."""
        tests_run = KCIDBTest.objects.filter(build__id=models.OuterRef('id'))
        tests_running = tests_run.filter(status=None)
        tests_pass = tests_run.filter(status=test_models.ResultEnum.PASS)
        tests_skip = tests_run.filter(status=test_models.ResultEnum.SKIP)
        tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL, waived=False)
        tests_error = tests_run.filter(status=test_models.ResultEnum.ERROR)
        tests_targeted = tests_run.filter(targeted=True)

        counts = {
            'tests_run': tests_run,
            'tests_running': tests_running,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_fail': tests_fail,
            'tests_error': tests_error,
        }

        annotations = {
            'stats_tests_passed': ~models.Exists(tests_fail),
            'stats_tests_targeted': models.Exists(tests_targeted),
            'stats_tests_errors': models.Exists(tests_error),
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('id').annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def filter_untriaged(self):
        """Filter untriaged builds."""
        return self.filter(
            valid=False,
            issues=None
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBBuild(EMOM('kcidb_build'), Model):
    """Model for KCIDBBuild."""

    iid = models.AutoField(primary_key=True)

    checkout = models.ForeignKey('KCIDBCheckout', on_delete=models.CASCADE)
    id = models.CharField(max_length=200, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    comment = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    command = models.TextField(null=True, blank=True)
    compiler = models.ForeignKey('Compiler', on_delete=models.PROTECT, null=True, blank=True)
    input_files = models.ManyToManyField('Artifact', related_name='build_input', blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='build_output', blank=True)
    config_name = models.CharField(max_length=20, null=True, blank=True)
    config_url = models.URLField(max_length=400, null=True, blank=True)
    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    log_excerpt = models.TextField(null=True, blank=True)
    valid = models.BooleanField(null=True)

    architecture = models.IntegerField(choices=pipeline_models.ArchitectureEnum.choices,
                                       null=True, blank=True)

    issues = models.ManyToManyField(to='Issue', through='IssueOccurrence')

    objects = KCIDBBuildManager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'checkout__policy'

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('-iid',)

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.builds', args=[self.iid])

    @property
    def is_public(self):
        """Return True if this KCIDBBuild is public."""
        return self.checkout.kernel_type == KernelTypeEnum.UPSTREAM

    @property
    def tests_passed(self):
        """Return True if all tests passed."""
        # pylint: disable=no-member
        if self.stats_tests_running_count:
            return None
        try:
            return not bool(self.stats_tests_fail_count)
        except AttributeError:
            return not self.kcidbtest_set.filter(status=test_models.ResultEnum.FAIL, waived=False).exists()

    @property
    def has_tests_errors(self):
        """Return True if a test had errors."""
        try:
            return self.stats_tests_errors
        except AttributeError:
            return self.kcidbtest_set.filter(status=test_models.ResultEnum.ERROR).exists()

    @property
    def has_targeted_tests(self):
        """Return True if the build has targeted tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_targeted'):
            return self.stats_tests_targeted

        return (
            KCIDBTest.objects
            .filter(build=self, targeted=True)
            .exists()
        )

    @property
    def all_passed(self):
        """Return True if the build and tests passed."""
        return self.valid and self.tests_passed

    @classmethod
    def create_from_json(cls, data):
        """Create KCIDBBuild from kcidb json."""
        misc = data.get('misc', {})

        arch = pipeline_models.ArchitectureEnum[data['architecture']] if data.get('architecture') else None

        compiler = data.get('compiler')
        compiler = pipeline_models.Compiler.objects.get_or_create(name=compiler)[0] if compiler else None

        log = data.get('log_url')
        log = file_models.Artifact.create_from_url(log) if log else None

        try:
            checkout = KCIDBCheckout.objects.get(id=data['checkout_id'], origin__name=data['origin'])
        except KCIDBCheckout.DoesNotExist as exc:
            raise MissingParent(KCIDBCheckout, data['checkout_id']) from exc

        origin = KCIDBOrigin.create_from_string(data['origin'])
        build, created = cls.objects.update_or_create(
            checkout=checkout,
            id=data['id'],
            origin=origin,
            # for an empty data dict, all of these have to be None
            defaults=utils.clean_dict({
                'comment': data.get('comment'),
                'start_time': data.get('start_time'),
                'duration': data.get('duration'),
                'architecture': arch,
                'command': data.get('command'),
                'compiler': compiler,
                'config_name': data.get('config_name'),
                'config_url': data.get('config_url'),
                'log': log,
                'log_excerpt': data.get('log_excerpt'),
                'valid': data.get('valid'),
            })
        )

        for input_file in data.get('input_files', []):
            file, _ = file_models.Artifact.objects.get_or_create(
                url=input_file['url'],
                defaults={
                    'name': input_file['name'],
                }
            )
            build.input_files.add(file)

        for output_file in data.get('output_files', []):
            file, _ = file_models.Artifact.objects.get_or_create(
                url=output_file['url'],
                defaults={
                    'name': output_file['name'],
                }
            )
            build.output_files.add(file)

        if is_cki_submission(data):
            pipeline_models.GitlabJob.create_from_misc(misc, kcidb_build=build)

        signals.kcidb_object.send(
            sender='kcidb.build.create_from_json',
            status=ObjectStatusEnum.NEW if created else ObjectStatusEnum.UPDATED,
            object_type='build',
            objects=[build],
        )

        return build


class KCIDBTestManager(Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBTestManager."""

    def filter_untriaged(self):
        """Filter untriaged tests."""
        return self.filter(
            status__in=KCIDBTest.UNSUCCESSFUL_STATUSES,
            issues=None
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBTest(EMOM('kcidb_test'), Model):
    """Model for KCIDBTest."""

    iid = models.AutoField(primary_key=True)

    build = models.ForeignKey('KCIDBBuild', on_delete=models.CASCADE)
    id = models.CharField(max_length=200, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    environment = models.ForeignKey('BeakerResource', on_delete=models.PROTECT, null=True, blank=True)
    test = models.ForeignKey('Test', on_delete=models.PROTECT, null=True, blank=True)
    waived = models.BooleanField(null=True)
    start_time = models.DateTimeField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='test', blank=True)

    status = models.CharField(max_length=1, choices=test_models.ResultEnum.choices,
                              null=True, blank=True)

    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    log_excerpt = models.TextField(null=True, blank=True)

    # Fields not belonging to KCIDB schema
    kernel_debug = models.BooleanField(null=True, blank=True)
    targeted = models.BooleanField(null=True, blank=True)
    issues = models.ManyToManyField(to='Issue', through='IssueOccurrence')

    objects = KCIDBTestManager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'build__checkout__policy'

    UNSUCCESSFUL_STATUSES = (test_models.ResultEnum.ERROR, test_models.ResultEnum.FAIL)

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('iid',)

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.tests', args=[self.iid])

    @property
    def is_public(self):
        """Return True if this KCIDBTest is public."""
        return self.build.checkout.kernel_type == KernelTypeEnum.UPSTREAM

    @classmethod
    def create_from_json(cls, data):
        # pylint: disable=too-many-locals
        """Create KCIDBTest from kcidb json."""
        misc = data.get('misc', {})

        try:
            build = KCIDBBuild.objects.get(id=data['build_id'])
        except KCIDBBuild.DoesNotExist as exc:
            raise MissingParent(KCIDBBuild, data['build_id']) from exc

        environment = data.get('environment')
        environment = test_models.BeakerResource.objects.get_or_create(
            fqdn=environment['comment']
        )[0] if environment else None

        test_comment = data.get('comment')
        if test_comment:
            test = test_models.Test.get_and_update(
                name=test_comment,
                universal_id=data.get('path'),
                maintainers=misc.get('maintainers'),
            )
        else:
            test = None

        status = test_models.ResultEnum[data['status']] if data.get('status') else None

        log_url = data.get('log_url')
        log = file_models.Artifact.create_from_url(log_url) if log_url else None

        origin = KCIDBOrigin.create_from_string(data['origin'])
        test, created = cls.objects.update_or_create(
            build=build,
            id=data['id'],
            origin=origin,
            # for an empty data dict, all of these have to be None
            defaults=utils.clean_dict({
                'environment': environment,
                'test': test,
                'status': status,
                'waived': data.get('waived'),
                'start_time': data.get('start_time'),
                'duration': data.get('duration'),
                'log': log,
                'log_excerpt': data.get('log_excerpt'),
            })
        )

        if created:
            # Set default values, should not be overwritten when resubmitted without these fields
            test.kernel_debug = misc.get('debug', False)
            test.targeted = misc.get('targeted', False)
            test.save()

        for output_file in data.get('output_files', []):
            file, _ = file_models.Artifact.objects.get_or_create(
                url=output_file['url'],
                defaults={
                    'name': output_file['name'],
                }
            )
            test.output_files.add(file)

        if is_cki_submission(data):
            pipeline_models.GitlabJob.create_from_misc(misc, kcidb_test=test)
            if 'beaker' in misc.keys():
                test_models.BeakerTask.create_from_misc(test, misc)

        signals.kcidb_object.send(
            sender='kcidb.test.create_from_json',
            status=ObjectStatusEnum.NEW if created else ObjectStatusEnum.UPDATED,
            object_type='test',
            objects=[test],
        )

        return test
