"""Lookup helpers."""
from django.db.models import Lookup
from django.db.models.fields import Field
from django.db.models.lookups import BuiltinLookup


@Field.register_lookup
class NotEqualLookup(Lookup):
    """Implement the SQL <> operator."""

    lookup_name = 'ne'

    def as_sql(self, compiler, connection):
        """Generate the sql query."""
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return f'{lhs} <> {rhs}', params


@Field.register_lookup
class NotInLookup(Lookup):
    """Implement the SQL NOT IN operator.

    Adapted from django.db.models.lookups. Most probably only works for simple
    lists.
    """

    lookup_name = 'not_in'

    def process_rhs(self, compiler, connection):
        """Process in a loop."""
        sqls, sqls_params = self.batch_process_rhs(compiler, connection, self.rhs)
        placeholder = '(' + ', '.join(sqls) + ')'
        return (placeholder, sqls_params)

    def get_db_prep_lookup(self, value, connection):
        """Prepare in a loop."""
        return ('%s', [self.lhs.output_field.get_db_prep_value(v, connection, prepared=True) for v in value])

    def get_prep_lookup(self):
        """Prepare in a loop."""
        return [self.lhs.output_field.get_prep_value(v) for v in self.rhs]

    def as_sql(self, compiler, connection):
        """Generate the sql query."""
        self.rhs = set(self.rhs)
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return f'{lhs} NOT IN {rhs}', params


@Field.register_lookup
class IsNotNull(BuiltinLookup):
    """
    Implement negation of IS NULL.

    Adapted from django.db.models.lookups.IsNull, query for IS NOT NULL
    if rhs is True, IS NULL otherwise.
    """

    lookup_name = 'isnotnull'
    prepare_rhs = False

    def as_sql(self, compiler, _):
        """Generate the sql query."""
        sql, params = compiler.compile(self.lhs)
        if self.rhs:
            return f"{sql} IS NOT NULL", params

        return f"{sql} IS NULL", params
