# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods,too-many-public-methods

"""Pipeline models file."""

from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse.models import utils
from datawarehouse.utils import clean_dict


class ProjectManager(utils.Manager):
    """Natural key for Project."""

    def get_by_natural_key(self, path):
        """Lookup the object by the natural key."""
        return self.get(path=path)


class Project(EMOM('project'), models.Model):
    """Model for Project."""

    project_id = models.IntegerField()
    path = models.CharField(max_length=100)
    instance_url = models.URLField()

    objects = ProjectManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.path}'

    def natural_key(self):
        """Return the natural key."""
        return (self.path, )

    @property
    def web_url(self):
        """Return formatted url of the project."""
        return f'{self.instance_url}/{self.path}'


class GitTree(EMOM('git_tree'), utils.Model):
    """Model for GitTree."""

    name = models.CharField(max_length=50)

    path_to_policy = 'kcidbcheckout__policy'

    objects = utils.GenericNameManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    @classmethod
    def create_from_string(cls, name):
        """Create GitTree from string."""
        return cls.objects.get_or_create(name=name)[0]


class GitlabJob(EMOM('gitlab_job'), utils.Model):
    """Model for Gitlab Job."""

    job_id = models.IntegerField()
    pipeline = models.ForeignKey('Pipeline', on_delete=models.CASCADE)
    kcidb_checkout = models.ForeignKey('KCIDBCheckout', on_delete=models.SET_NULL, null=True)
    kcidb_build = models.ManyToManyField('KCIDBBuild')
    kcidb_test = models.ManyToManyField('KCIDBTest')

    class Meta:
        """Metadata."""

        constraints = [
            models.UniqueConstraint(fields=['job_id', 'pipeline'], name='unique_job')
        ]

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.job_id}'

    @property
    def web_url(self):
        """Return formatted url of job."""
        return f'{self.pipeline.project.web_url}/-/jobs/{self.job_id}'

    @classmethod
    def create_from_misc(cls, misc, kcidb_checkout=None, kcidb_build=None, kcidb_test=None):
        """Create GitlabJob from kcidb misc field."""
        pipeline = Pipeline.create_from_misc(misc, kcidb_checkout=kcidb_checkout)
        gitlab_job = cls.objects.get_or_create(
            job_id=misc['job']['id'],
            pipeline=pipeline,
            kcidb_checkout=kcidb_checkout,
        )[0]
        if kcidb_checkout:
            # This is going to go away after removing the Pipeline -> Patches link
            pipeline.patches.set(kcidb_checkout.patches.all())
        if kcidb_build:
            gitlab_job.kcidb_build.add(kcidb_build)
        if kcidb_test:
            gitlab_job.kcidb_test.add(kcidb_test)

        return gitlab_job


class Pipeline(EMOM('pipeline'), utils.Model):
    """Model for Pipeline."""

    pipeline_id = models.IntegerField()
    project = models.ForeignKey(Project, on_delete=models.PROTECT)
    gittree = models.ForeignKey(GitTree, on_delete=models.PROTECT, null=True)
    kcidb_checkout = models.ForeignKey('KCIDBCheckout', on_delete=models.SET_NULL, null=True)

    path_to_policy = 'kcidb_checkout__policy'

    class Meta:
        """Metadata."""

        ordering = ('-pipeline_id',)
        constraints = [
            models.UniqueConstraint(fields=['pipeline_id', 'project'], name='unique_pipeline')
        ]

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.pipeline_id}'

    def natural_key(self):
        """Return the natural key."""
        return (self.pipeline_id, )

    @property
    def trigger_variables(self):
        """Flat dict of variables."""
        return {var.key: var.value for var in self.variables.all()}

    @property
    def web_url(self):
        """Return formatted url of pipeline."""
        return f'{self.project.web_url}/-/pipelines/{self.pipeline_id}'

    @property
    def report_exists(self):
        """Any report available?."""
        return len(list(self.reports.all())) > 0

    @classmethod
    def create_from_misc(cls, misc, kcidb_checkout=None):
        """Create Pipeline from kcidb misc field."""
        project = Project.objects.get_or_create(
            project_id=misc['pipeline']['project']['id'],
            path=misc['pipeline']['project']['path_with_namespace'],
            instance_url=misc['pipeline']['project']['instance_url'],
        )[0]

        ref = misc['pipeline']['ref']
        if ref.startswith('retrigger'):
            ref = 'retriggers'
        gittree = GitTree.objects.get_or_create(name=ref)[0]

        pipeline, created = cls.objects.update_or_create(
            pipeline_id=misc['pipeline']['id'],
            defaults=clean_dict({
                'project': project,
                'gittree': gittree,
                'kcidb_checkout': kcidb_checkout,
            })
        )

        if created:
            TriggerVariable.create_from_dict(pipeline, misc['pipeline']['variables'])

        return pipeline


class TriggerVariable(EMOM('trigger_variable'), models.Model):
    """Model for TriggerVariable."""

    key = models.CharField(max_length=100)
    value = models.TextField()
    pipeline = models.ForeignKey(Pipeline,
                                 on_delete=models.CASCADE,
                                 related_name='variables')

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.key}: {self.value}'

    @classmethod
    def create_from_dict(cls, pipeline, data):
        """Crate variables for each key: value in a dict."""
        return [
            cls.objects.get_or_create(key=key, value=value, pipeline=pipeline)[0]
            for key, value in data.items()
        ]


class ArchitectureEnum(models.IntegerChoices):
    # pylint: disable=invalid-name
    """Enumeration of the possible architectures."""

    aarch64 = 2, 'aarch64'
    ppc64 = 3, 'ppc64'
    ppc64le = 4, 'ppc64le'
    s390x = 5, 's390x'
    x86_64 = 6, 'x86_64'
    x86_64_debug = 7, 'x86_64_debug'
    i686 = 8, 'i686'


class Compiler(EMOM('compiler'), models.Model):
    """Model for Compiler."""

    name = models.TextField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'
