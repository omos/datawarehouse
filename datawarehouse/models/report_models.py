# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.
"""Report models file."""

from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM


class Recipient(EMOM('recipient'), models.Model):
    """Model for Recipient."""

    email = models.EmailField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.email}'


class Report(EMOM('report'), models.Model):
    """Model for Report."""

    kcidb_checkout = models.ForeignKey('KCIDBCheckout',
                                       related_name='reports',
                                       on_delete=models.CASCADE)

    subject = models.CharField(max_length=200)
    msgid = models.CharField(max_length=200, unique=True)
    body = models.TextField()
    addr_to = models.ManyToManyField(Recipient, related_name='addr_to')
    addr_cc = models.ManyToManyField(Recipient, related_name='addr_cc')
    sent_at = models.DateTimeField()
    raw = models.TextField()

    def __str__(self):
        """Return __str__ formatted."""
        return self.subject
