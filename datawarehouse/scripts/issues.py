"""Issues scripts."""
from datawarehouse import models


def update_issue_occurrences_related_checkout(instance, issues_ids):
    """
    Update IssueOccurrence.related_checkout parameter.

    Called on post_add, this function sets the related_checkout parameter
    on the created IssueOccurrence objects.
    """
    # The call arguments don't contain the IssueOccurrence so it's
    # necessary to query it with the Issue and the KCIDB object.
    if isinstance(instance, models.KCIDBCheckout):
        related_checkout = instance
        issue_occurrence_filter = {'kcidb_checkout': instance}
    elif isinstance(instance, models.KCIDBBuild):
        related_checkout = instance.checkout
        issue_occurrence_filter = {'kcidb_build': instance}
    elif isinstance(instance, models.KCIDBTest):
        related_checkout = instance.build.checkout
        issue_occurrence_filter = {'kcidb_test': instance}
    else:
        raise Exception('Unhandled signal instance')

    models.IssueOccurrence.objects.filter(
        issue__id__in=issues_ids,
        **issue_occurrence_filter
    ).update(
        related_checkout=related_checkout
    )
