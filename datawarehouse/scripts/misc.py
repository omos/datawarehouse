# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Misc scripts file."""
from collections import defaultdict
import datetime

from celery import shared_task
from cki_lib.logger import get_logger
from django.conf import settings
from django.template import loader
from django.utils import timezone

from datawarehouse import models
from datawarehouse import signals
from datawarehouse import utils

LOGGER = get_logger(__name__)


@shared_task
def send_kcidb_object_for_retriage(since_days_ago=14):
    """Add last n days objects to the queue for triaging."""
    date_from = timezone.now() - datetime.timedelta(days=since_days_ago)

    to_retriage = {
        'checkout': models.KCIDBCheckout.objects.filter(
            valid=False,
            start_time__gte=date_from,
        ),
        'build': models.KCIDBBuild.objects.filter(
            valid=False,
            start_time__gte=date_from
        ),
        'test': models.KCIDBTest.objects.filter(
            status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES,
            start_time__gte=date_from
        ),
    }

    for kind, objects in to_retriage.items():
        signals.kcidb_object.send(
            sender='scripts.misc.send_kcidb_object_for_retriage',
            status=models.ObjectStatusEnum.NEEDS_TRIAGE,
            object_type=kind,
            objects=objects,
        )


def notify_issue_regression(obj, issue):
    """Notify that an issue regression was detected."""
    email_template = loader.get_template('email_issue_regression.html')
    email_context = {
        'issue': issue,
        'obj': obj,
    }

    email_subject = f'{issue.kind.tag} | Issue regression detected'
    email_message = email_template.render(email_context, {})
    recipients = defaultdict(set)

    # Filter users with authorization to read the issue
    users = issue.users_read_authorized
    # Exclude users not subscribed to the issue regression
    users = users.exclude(subscriptions__issue_regression_subscribed_at=None)
    # Filter users with authorization to read the object
    users = [u for u in users if u in obj.users_read_authorized]

    recipients['cc'].update(
        u.email for u in users
        if u.subscriptions.issue_regression_visibility == models.SubscriptionVisibility.CC
    )

    recipients['bcc'].update(
        u.email for u in users
        if u.subscriptions.issue_regression_visibility == models.SubscriptionVisibility.BCC
    )

    if isinstance(obj, models.KCIDBTest):
        maintainers_emails = obj.test.maintainers.values_list('email', flat=True)
        recipients['to'].update(maintainers_emails)

    # Sets are not serializable, convert to list.
    recipients = {k: list(v) for k, v in recipients.items()}

    if any(recipients.values()):
        LOGGER.info("notify_issue_regression: issue_id=%d recipients=%s",
                    issue.id, recipients)
        utils.async_send_email.delay(
            email_subject, email_message, recipients
        )


def verify_issue_regression(obj, issue):
    """Verify that a tagged issue is not a regression."""
    if not settings.FF_NOTIFY_ISSUE_REGRESSION:
        return

    if not issue.resolved:
        # Not resolved, not a regression.
        return

    if obj.issues.filter(id=issue.id).exists():
        # Already tagged, not a regression.
        return

    if obj.start_time and issue.resolved_on and obj.start_time < issue.resolved_on:
        # The issue was resolved after this object started
        return

    notify_issue_regression(obj, issue)
