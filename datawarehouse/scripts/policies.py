"""Policy scripts."""
from django.conf import settings
from django.contrib.auth import get_user_model

from datawarehouse import ldap
from datawarehouse import models

User = get_user_model()


def update_issue_policy(issues_ids):
    """
    Update the Issue policy after a new ocurrence is assigned.

    When a post_add signal is received, check if the issue has to be
    updated to public depending on the related objects.
    If an Issue is set to something different to PUBLIC, but it has
    at least one related checkout that is public, set policy to PUBLIC.
    """
    public_policy = models.Policy.objects.get(name=models.Policy.PUBLIC)

    models.Issue.objects.filter(
        id__in=issues_ids,
        # Only the ones with policy_auto_public
        policy_auto_public=True,
        # Only the ones that have a public tree tagged
        issueoccurrence__related_checkout__policy=public_policy,
    ).exclude(
        # Exclude the ones already public
        policy=public_policy
    ).update(
        policy=public_policy
    )


def update_ldap_group_members():
    """Update Group members based on LDAPGroupLink."""
    if not settings.FF_LDAP_GROUP_SYNC:
        return

    with ldap.connection() as conn:
        for link in models.LDAPGroupLink.objects.all():
            group_users = conn.get_users(link.filter_query)
            users = User.objects.raw(
                'SELECT "id" FROM "auth_user" WHERE ("username", "email") IN %s',
                [tuple(group_users)]
            ) if group_users else []
            link.group.user_set.set(users)

            if link.extra_users.exists():
                link.group.user_set.add(*link.extra_users.all())
