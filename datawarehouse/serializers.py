# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Serializers."""
from django.db.models import Min
from rest_framework import serializers

from . import models


# Misc
class GitTreeSerializer(serializers.ModelSerializer):
    """Serializer for GitTree model."""

    class Meta:
        """Metadata."""

        model = models.GitTree
        fields = ('id', 'name',)


class PolicySerializer(serializers.ModelSerializer):
    """Serializer for Policy model."""

    class Meta:
        """Metadata."""

        model = models.GitTree
        fields = ('id', 'name')


# Merge
class PatchSerializer(serializers.ModelSerializer):
    """Serializer for Patch model."""

    class Meta:
        """Metadata."""

        model = models.Patch
        fields = ('url', 'subject')


# Build
class CompilerSerializer(serializers.ModelSerializer):
    """Serializer for Compiler model."""

    class Meta:
        """Metadata."""

        model = models.Compiler
        fields = ('name',)


# Test
class TestMaintainerSerializer(serializers.ModelSerializer):
    """Serializer for TestMaintainer model."""

    class Meta:
        """Metadata."""

        model = models.TestMaintainer
        fields = ('name', 'email')


class TestSerializer(serializers.ModelSerializer):
    """Serializer for Test model."""

    maintainers = TestMaintainerSerializer(many=True)

    class Meta:
        """Metadata."""

        model = models.Test
        fields = ('id', 'name', 'fetch_url', 'maintainers', 'universal_id',)


class TestSimpleSerializer(TestSerializer):
    """Serializer for Test model. Simple."""

    class Meta(TestSerializer.Meta):
        """Metadata."""

        fields = ('id', 'name', 'fetch_url')


class TestConfidenceSerializer(TestSerializer):
    """Serializer for Test model. With confidence metrics."""

    class Meta(TestSerializer.Meta):
        """Metadata."""

        fields = TestSerializer.Meta.fields + ('confidence',)


class ArtifactSerializer(serializers.ModelSerializer):
    """Serializer for Artifact model."""

    # Temporarily keep file support to make the change on the lib.
    file = serializers.CharField(source='url')

    class Meta:
        """Metadata."""

        model = models.Artifact
        fields = ('name', 'file', 'url')


class BeakerResourceSerializer(serializers.ModelSerializer):
    """Serializer for BeakerResource model."""

    class Meta:
        """Metadata."""

        model = models.BeakerResource
        fields = ('id', 'fqdn',)


class BeakerResourceConfidenceSerializer(BeakerResourceSerializer):
    """Serializer for BeakerResource model. With confidence metrics."""

    class Meta(BeakerResourceSerializer.Meta):
        """Metadata."""

        fields = BeakerResourceSerializer.Meta.fields + ('confidence',)


class ProjectSerializer(serializers.ModelSerializer):
    """Serializer for Project model."""

    class Meta:
        """Metadata."""

        model = models.Project
        fields = ('project_id', 'path')


class RecipientSerializer(serializers.ModelSerializer):
    """Serializer for Recipient model."""

    class Meta:
        """Metadata."""

        model = models.Recipient
        fields = ("email",)


class ReportSerializer(serializers.ModelSerializer):
    """Serializer for Report model."""

    addr_to = RecipientSerializer(many=True)
    addr_cc = RecipientSerializer(many=True)

    class Meta:
        """Metadata."""

        model = models.Report
        fields = ("__all__")


# Pipeline
class PipelineSerializer(serializers.ModelSerializer):
    """Serializer for Pipeline model."""

    gittree = GitTreeSerializer()
    project = ProjectSerializer()
    checkout_iid = serializers.IntegerField(source='kcidb_checkout.iid', read_only=True)

    class Meta:
        """Metadata."""

        model = models.Pipeline
        fields = ('project', 'gittree', 'web_url', 'pipeline_id',
                  'trigger_variables', 'checkout_iid')


class IssueKindSerializer(serializers.ModelSerializer):
    """Serializer for IssueKind model."""

    class Meta:
        """Metadata."""

        model = models.IssueKind
        fields = ('id', 'description', 'tag',)


class IssueSerializer(serializers.ModelSerializer):
    """Serializer for Issue model."""

    kind = IssueKindSerializer()
    origin_tree = GitTreeSerializer()
    policy = PolicySerializer()
    first_seen = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.Issue
        fields = ('id', 'kind', 'description', 'ticket_url', 'resolved', 'resolved_on', 'origin_tree', 'policy',
                  'first_seen')

    def get_first_seen(self, issue):
        # pylint: disable=no-self-use
        """Return timestamp of the first checkout where this issue was seen."""
        return (
            # Don't use issue.get_checkouts() as we don't care about the
            # authorization filter, it's just the timestamp.
            issue.issueoccurrence_set
            .annotate(
                first_occurrence=Min('related_checkout__start_time')
            )
            .order_by('first_occurrence')
            .values_list('first_occurrence', flat=True)
            .first()
        )


class IssueRegexSerializer(serializers.ModelSerializer):
    """Serializer for IssueRegex model."""

    issue = IssueSerializer()

    class Meta:
        """Metadata."""

        model = models.IssueRegex
        fields = ('id', 'issue', 'text_match', 'file_name_match', 'test_name_match')
