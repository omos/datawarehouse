"""Receiver functions for signals."""
from django import dispatch
from django.conf import settings
from django.db.models.signals import m2m_changed
from django.db.models.signals import post_save
from django.utils import timezone

from datawarehouse import models
from datawarehouse import scripts
from datawarehouse import signals
from datawarehouse import utils
from datawarehouse.api.kcidb import serializers as kcidb_serializers


@dispatch.receiver(signals.kcidb_object)
def send_kcidb_object_message(**kwargs):
    """Send new kcidb object message."""
    serializers = {
        'checkout': kcidb_serializers.KCIDBCheckoutSerializer,
        'build': kcidb_serializers.KCIDBBuildSerializer,
        'test': kcidb_serializers.KCIDBTestSerializer,
    }

    status = kwargs['status']
    object_type = kwargs['object_type']
    objects = kwargs['objects']

    messages = [{
        'timestamp': timezone.now().isoformat(),
        'status': status,
        'object_type': object_type,
        'object': serializers[object_type](object_instance).data,
        'id': object_instance.id,
        'iid': object_instance.iid,
    } for object_instance in objects]
    utils.send_kcidb_notification(messages)


@dispatch.receiver(post_save, sender=models.IssueRegex)
def send_kcidb_objects_retriage(**_):
    """Send kcidb objects for retriage after regexes are added / modified."""
    if settings.RABBITMQ_CONFIGURED:
        scripts.TIMER_RETRIAGE.start()


@dispatch.receiver(m2m_changed, sender=models.KCIDBCheckout.issues.through)
@dispatch.receiver(m2m_changed, sender=models.KCIDBBuild.issues.through)
@dispatch.receiver(m2m_changed, sender=models.KCIDBTest.issues.through)
def issue_occurrence_assigned(action, sender, instance, pk_set, **_):
    """Signal handler to populate extra fields in IssueOccurrence."""
    # Only process post_add for issue_occurrence m2m.
    if action != 'post_add' or sender != models.IssueOccurrence:
        return

    scripts.update_issue_occurrences_related_checkout(instance, pk_set)
    scripts.update_issue_policy(pk_set)
