"""Add string templatetags."""
import re
import urllib

from django import template

register = template.Library()


@register.filter
def addstr(arg1, arg2):
    """Concatenate arg1 & arg2."""
    return str(arg1) + str(arg2)


@register.filter
def git_url_trim(full_url):
    """
    Convert git url into a shorter string.

    Take an URL with a long path (host/a/b/c/d/e) and
    return a shorter version (host/.../d/e) instead.
    """
    url = urllib.parse.urlparse(full_url)

    try:
        short_path = re.findall(r'.*\/(.*\/.*)', url.path)[0]
    except (IndexError, TypeError):
        return full_url

    return f'{url.scheme}://{url.netloc}/.../{short_path}'
