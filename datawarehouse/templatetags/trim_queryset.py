"""Trim a queryset."""
import collections

from django import template

register = template.Library()


class TrimmedQueryset(collections.abc.Sequence):
    """Trim a queryset to shown only a limited number of results."""

    def __init__(self, queryset, trim_at=10):
        """Instantiate a queryset."""
        queryset_count = queryset.count()
        trim_at = min(trim_at, queryset_count)

        self.elements = queryset[:trim_at]
        self.trimmed_elements = max(0, queryset_count - trim_at)

    def __len__(self):
        """Return the number of objects."""
        return len(self.elements)

    def __getitem__(self, index):
        """Get an object."""
        return self.elements[index]


@register.filter
def trim_queryset(queryset, trim_at=10):
    """Get the first n elements of queryset."""
    return TrimmedQueryset(queryset, trim_at)
