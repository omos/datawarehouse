# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Utils file."""
import datetime
import email
from functools import wraps

from captcha.helpers import captcha_image_url
from captcha.models import CaptchaStore
from celery import shared_task
from cki_lib.logger import get_logger
from cki_lib.timer import ScheduledTask
import dateutil
from django.conf import settings
from django.core.mail import EmailMessage
from django.utils import timezone
from django.views.decorators.cache import cache_page
from requests_futures.sessions import FuturesSession
from rest_framework import generics

from datawarehouse import authorization
from datawarehouse import rabbitmq

LOGGER = get_logger(__name__)
if settings.RABBITMQ_CONFIGURED:
    MSG_QUEUE = rabbitmq.MessageQueue(
        settings.RABBITMQ_HOST,
        settings.RABBITMQ_PORT,
        settings.RABBITMQ_USER,
        settings.RABBITMQ_PASSWORD,
        settings.RABBITMQ_KEEPALIVE_S,
    )
    TIMER_MSG_QUEUE_SEND = ScheduledTask(
        settings.TIMER_MSG_QUEUE_SEND_PERIOD_S,
        MSG_QUEUE.send_thread
    )


def parse_patches_from_urls(patch_urls):
    """
    Get patch subject from list of patches' urls.

    The result respects the order of the input.
    """
    session = FuturesSession(max_workers=settings.REQUESTS_MAX_WORKERS)
    return [{
        'url': p,
        'subject': email.message_from_bytes(s.result().content)['Subject']
    } for p, s in [(p, session.get(p)) for p in patch_urls]]


def timestamp_to_datetime(timestamp):
    """Convert a timestamp string to a _timezone-aware_ datetime, while keeping None values."""
    try:
        time = datetime.datetime.fromisoformat(timestamp)
    except ValueError:
        time = dateutil.parser.parse(timestamp)
    except TypeError:
        return None

    try:
        return timezone.make_aware(time)
    except ValueError:  # tz info already set
        return time


def send_kcidb_notification(messages):
    """Send kcidb notifications over rabbitmq."""
    if not settings.RABBITMQ_CONFIGURED:
        LOGGER.debug("KCIDB notification skipped. No RabbitMQ server configured.")
        return

    messages = messages if isinstance(messages, list) else [messages]
    MSG_QUEUE.bulk_add(
        [
            (message, settings.RABBITMQ_EXCHANGE_KCIDB_NOTIFICATIONS)
            for message in messages
        ]
    )
    TIMER_MSG_QUEUE_SEND.start()


def clean_dict(data):
    """Remove keys with None value from dict."""
    return {
        key: value
        for key, value in data.items()
        if value is not None
    }


class MultipleFieldLookupMixin:
    # pylint: disable=too-few-public-methods
    """
    GET with multiple filters.

    Allows setting parameters as name:field to use a different value as the parameter
    key than the representation on the database.

    For example, checkout_id:checkout__origin_id takes a `checkout_id` keyword parameter but
    filters the database with `checkout__origin_id`.
    """

    lookup_fields = set()

    @staticmethod
    def parse_field(field, value):
        """Replace %%id wildcard with id or iid depending on the value."""
        if field.endswith('%%id'):
            if value.isdigit() and int(value) < 2e9:
                field = field.replace('%%id', 'iid')
            else:
                field = field.replace('%%id', 'id')

        return field

    def get_queryset(self):
        """Override get_queryset."""
        queryset = self.filter_queryset(self.queryset)

        filter_params = {}
        for param, field in self.lookup_fields:

            if param in self.kwargs.keys():
                # It's part of the url parameters
                value = self.kwargs[param]
            else:
                # It's part of the GET parameters
                value = self.request.GET.get(param)

            if not value:
                # It's missing, don't use it to filter.
                continue

            field = self.parse_field(field, value)
            filter_params[field] = value

        queryset = authorization.PolicyAuthorizationBackend.filter_authorized(
            self.request,
            queryset,
        )

        return queryset.filter(**filter_params)

    def get_object(self):
        """Override get_object."""
        queryset = self.get_queryset()
        return generics.get_object_or_404(queryset)


def filter_view(request, queryset, filters_list):
    """
    Filter queryset according to GET parameters.

    From a list of pre defined filter parameters, filter the queryset using
    the fields set on filters_list.

    Returns the filtered queryset and the list of filters applied.
    """
    filters = {}

    for param in request.GET:
        filter_def = filters_list.get(param)
        if not filter_def:
            continue

        if filter_def.get('is_list', False):
            value = request.GET.getlist(param)
        else:
            value = request.GET.get(param)

        if not value:
            continue

        queryset = queryset.filter(**{filter_def['field']: value})
        filters[param] = value

    return queryset, filters


def filter_checkouts_view(request, queryset, path_to_checkout=''):
    """Filter checkouts depending on request's GET parameters."""
    filters_list = {
        'filter_email': {'field': f'{path_to_checkout}contacts__email__icontains'},
        'filter_gittrees': {'field': f'{path_to_checkout}tree__name__in', 'is_list': True},
    }

    return filter_view(request, queryset, filters_list)


def filter_builds_view(request, queryset, path_to_build=''):
    """Filter builds depending on request's GET parameters."""
    filters_list = {
        'filter_architectures': {'field': f'{path_to_build}architecture__in', 'is_list': True},
    }

    return filter_view(request, queryset, filters_list)


@shared_task
def async_send_email(subject, body, recipients, headers=None):
    """
    Send email asyncronusly using celery shared task.

    Arguments:
        - subject: (str) Subject of the message.
        - body: (str) Content of the message.
        - recipients: (str|list|dict) Recipients for the message. If string or list is provided
          the addresses are treated as 'to'. If dict, 'to', 'cc' and 'bcc' keys are allowed
          containing a list of recipients for each field.
        - headers: (dict) Custom headers to include on the message.
    """
    if isinstance(recipients, str):
        recipients = {'to': [recipients]}
    elif isinstance(recipients, list):
        recipients = {'to': recipients}

    LOGGER.info("async_send_email: subject=%s recipients=%s", subject, recipients)

    EmailMessage(
        subject=subject,
        body=body,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=recipients.get('to', []),
        cc=recipients.get('cc', []),
        bcc=recipients.get('bcc', []),
        headers=headers or {},
    ).send()


def notify_user(subject, message, user):
    """Send notification to user."""
    async_send_email.delay(
        subject, message, user.email
    )


def datetime_bool(bool_value):
    """
    Return a timestamp depending if bool_value is True.

    This is useful for DateTimeFields used as boolean flags.
    """
    return timezone.now() if bool_value else None


def get_captcha():
    """Return new Captcha key with img url for challenge."""
    captcha = {
        'key': CaptchaStore.generate_key()
    }
    captcha['image'] = captcha_image_url(captcha['key'])
    return captcha


def verify_captcha(hashkey, solution):
    """Check captcha solution."""
    if settings.CAPTCHA_TEST_MODE:
        return solution.lower() == "passed"

    try:
        store = CaptchaStore.objects.get(hashkey=hashkey)
    except CaptchaStore.DoesNotExist:
        return False

    store.delete()
    return solution == store.challenge


def query_id_or_iid(value):
    """Return query content for id or iid depending on value."""
    if value.isdigit() and int(value) < 2e9:
        return {'iid': value}

    return {'id': value}


def cache_anonymous(ttl=settings.DEFAULT_CACHE_TTL_S):
    """Cache anonymous requests."""

    def _decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            # When FF_CACHE_ANONYMOUS is disabled, ignore conditions.
            if not settings.FF_CACHE_ANONYMOUS:
                return view_func(request, *args, **kwargs)

            # For authenticated users, do not cache.
            if request.user.is_authenticated:
                return view_func(request, *args, **kwargs)

            # If not authenticated, return cached page.
            return cache_page(ttl)(view_func)(request, *args, **kwargs)
        return _wrapped_view

    return _decorator


def group_issue_occurrences(issue_occurrences):
    """
    Group issue_occurrences by issue.

    Given a list of issue occurrences, group them by issue to make
    them easier to group into cards on the UI.
    """
    result = {
        io.issue: {'issue': io.issue, 'checkouts': [], 'builds': [], 'tests': []}
        for io in issue_occurrences
    }

    for occurrence in issue_occurrences:
        if occurrence.kcidb_checkout:
            result[occurrence.issue]['checkouts'].append(occurrence)
        if occurrence.kcidb_build:
            result[occurrence.issue]['builds'].append(occurrence)
        if occurrence.kcidb_test:
            result[occurrence.issue]['tests'].append(occurrence)

    return list(sorted(result.values(), key=lambda x: x['issue'].id, reverse=True))
