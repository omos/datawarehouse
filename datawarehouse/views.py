# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Views file."""

import datetime

from cki_lib.logger import get_logger
from cki_lib.misc import strtobool
from django.contrib.auth.decorators import permission_required
from django.core.exceptions import PermissionDenied
from django.db.models import Count
from django.db.models import F
from django.db.models import Max
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseNotAllowed
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse
from django.utils import timezone

from . import authorization
from . import cron
from . import models
from . import pagination
from . import utils

LOGGER = get_logger(__name__)


@permission_required('datawarehouse.change_issue',
                     raise_exception=True)
def issue_resolve(request, issue_id):
    """Resolve or unresolve issue."""
    if request.method == "POST":
        redirect_to = request.POST.get('redirect_to')
        issue = models.Issue.objects.get(id=issue_id)

        if not issue.is_write_authorized(request):
            raise Http404()

        # Toggle resolved_on value
        issue.resolved_on = utils.datetime_bool(not issue.resolved)
        issue.last_edited_by = request.user
        issue.save()

        LOGGER.info('action="resolve issue" user="%s" issue_id="%s"',
                    request.user.username, issue.id)

        return HttpResponseRedirect(redirect_to)

    return HttpResponseNotAllowed(['POST'])


def issue_new_or_edit(request):
    # pylint: disable=too-many-locals
    """Create or edit issue depending on issue_id being present."""
    if request.method == "POST":
        permissions = {
            'new': 'datawarehouse.add_issue',
            'edit': 'datawarehouse.change_issue',
        }

        issue_id = request.POST.get('issue_id') or None

        # If we don't have the issue_id, create a new issue. Otherwise, edit that one.
        action = 'edit' if issue_id else 'new'

        if not request.user.has_perm(permissions[action]):
            raise PermissionDenied()

        description = request.POST.get('description', '').strip()
        ticket_url = request.POST.get('ticket_url', '').strip()
        kind_id = request.POST.get('kind_id')
        redirect_to = request.POST.get('redirect_to')
        resolved_on = utils.datetime_bool(request.POST.get('resolved') == 'on')
        origin_tree_id = request.POST.get('origin_tree_id', 0)
        policy_id = request.POST.get('policy_id')
        policy_auto_public = request.POST.get('policy_auto_public') == 'on'

        policy = models.Policy.objects.get(id=policy_id)
        issue_kind = models.IssueKind.objects.get(id=kind_id)
        if issue_kind.kernel_code_related and int(origin_tree_id) != 0:
            tree = models.GitTree.objects.get(id=origin_tree_id)
        else:
            tree = None

        # Can't edit the issue to match another issue's ticket_url.
        if models.Issue.objects.exclude(id=issue_id).filter(ticket_url=ticket_url).exists():
            return HttpResponseBadRequest(f'Issue already exists with ticket URL {ticket_url}')

        if action == 'new':
            issue = models.Issue(
                description=description,
                ticket_url=ticket_url,
                kind=issue_kind,
                origin_tree=tree,
                policy=policy,
                created_by=request.user,
                policy_auto_public=policy_auto_public,
            )

            if not issue.is_write_authorized(request):
                raise PermissionDenied()

            issue.save()
            issue_id = issue.id

        elif action == 'edit':
            models.Issue.objects.filter_authorized(request).filter(id=issue_id).update(
                description=description,
                ticket_url=ticket_url,
                kind=issue_kind,
                resolved_on=resolved_on,
                origin_tree=tree,
                policy=policy,
                last_edited_by=request.user,
                policy_auto_public=policy_auto_public,
            )

        LOGGER.info('action="%s issue" user="%s" issue_id="%s"',
                    action, request.user.username, issue_id)

        redirect_to = redirect_to or reverse('views.issue.get', args=[issue_id])
        return HttpResponseRedirect(redirect_to)

    if request.method == "GET":
        template = loader.get_template('web/issue.html')

        context = {
            'issue_kinds': models.IssueKind.objects.all().order_by('id'),
            'git_trees': models.GitTree.objects.filter_authorized(request).order_by('name').distinct(),
            'policies': authorization.PolicyAuthorizationBackend.get_policies_write_authorized(request),
        }

        return HttpResponse(template.render(context, request))

    return HttpResponseNotAllowed(['GET', 'POST'])


def cron_run(request):
    """Run cron tasks to fetch new data."""
    cron.run.delay()

    return JsonResponse({})


def confidence(request, group):
    # pylint: disable=too-many-locals, too-many-branches
    """Confidence dashboard. Can be by tests or hosts."""
    template = loader.get_template('web/confidence.html')
    days_ago = request.GET.get('days_ago', 7)
    search = request.GET.get('search')

    if days_ago == 'ever':
        date_from = timezone.make_aware(datetime.datetime.min)
    else:
        date_from = timezone.now() - datetime.timedelta(days=int(days_ago))

    if group == 'tests':
        group_item = models.Test
        related_item = 'kcidbtest'
    elif group == 'hosts':
        group_item = models.BeakerResource
        related_item = 'kcidbtest'
    else:
        raise Http404()

    authorized_items = (
        group_item.objects
        # This is a really slow query because of the huge path to get from
        # BeakerResource -> Policy
        .filter_authorized(request)
        .values_list('id', flat=True)
    )

    all_items = (
        group_item.objects
        .filter(id__in=authorized_items)
        .filter(**{f'{related_item}__start_time__gte': date_from})
    )

    if search:
        if group == 'tests':
            all_items = all_items.filter(name__icontains=search)
        elif group == 'hosts':
            all_items = all_items.filter(fqdn__icontains=search)

    for result in models.ResultEnum:
        all_items = all_items.annotate(
            **{result.label: Count(related_item, filter=Q(**{f'{related_item}__status': result}))})

    all_items = all_items.annotate(total=Count(related_item))

    items_map = []
    for item in all_items:
        test_results = {}
        test_results_percent = {}

        if not item.total:
            continue

        for result in models.ResultEnum:
            count = getattr(item, result.label)
            test_results[result.label] = count
            test_results_percent[result.label] = 100 / item.total * count

        confidence_index = item.PASS / item.total

        items_map.append({
            'item': item,
            'confidence': confidence_index,
            'results': test_results,
            'results_percent': test_results_percent,
            'total': item.total})

    items_map = sorted(items_map, key=lambda t: t.get('confidence'))

    context = {'map': items_map, 'group': group, 'since': days_ago, 'search': search}
    return HttpResponse(template.render(context, request))


def details(request, group, item_id):
    # pylint: disable=too-many-locals
    """Show tests run by test."""
    template = loader.get_template('web/details.html')
    page = request.GET.get('page')
    result_filter = request.GET.get('result')

    if group == 'test':
        item = models.Test
        related_name = 'kcidbtest'
        table_by = models.BeakerResource
        testrun_to_item = 'test'

    elif group == 'host':
        item = models.BeakerResource
        related_name = 'kcidbtest'
        table_by = models.Test
        testrun_to_item = 'environment'

    else:
        raise Http404()

    item = get_object_or_404(
        item.objects.filter_authorized(request).distinct(),
        id=item_id
    )

    checkouts = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .filter(
            **{f'kcidbbuild__kcidbtest__{testrun_to_item}': item}
        )
    )

    table_by = table_by.objects.filter(**{f'{related_name}__{testrun_to_item}': item})\
                               .annotate(total_runs=Count(related_name))\
                               .order_by('-total_runs')

    if result_filter:
        result_filter = getattr(models.ResultEnum, result_filter)
        checkouts = checkouts.filter(
            **{f'kcidbbuild__kcidbtest__{testrun_to_item}': item,
               'kcidbbuild__kcidbtest__status': result_filter})
        table_by = table_by.filter(
            **{f'{related_name}__{testrun_to_item}': item, f'{related_name}__status': result_filter})

    paginator = pagination.EndlessPaginator(
        checkouts.values_list('iid', flat=True),
        30
    )
    checkouts_iids = paginator.get_page(page)
    checkouts = models.KCIDBCheckout.objects.filter(iid__in=checkouts_iids)

    runs_list = []

    for checkout in checkouts:
        runs = models.KCIDBTest.objects.filter(
            build__checkout=checkout,
            **{testrun_to_item: item}
        ).select_related(
            'build',
            'environment',
        ).prefetch_related(
            'output_files',
            'gitlabjob_set',
            'beakertask_set',
            'issues',
        )

        if result_filter:
            runs = runs.filter(status=result_filter)

        runs_list.append({
            'checkout': checkout,
            'tests': runs,
        })

    for result in models.ResultEnum:
        table_by = table_by.annotate(
            **{result.label: Count(related_name, filter=Q(**{f'{related_name}__status': result}))}
        )

    context = {
        'item': item,
        'paginator': checkouts_iids,
        'runs': runs_list,
        'type': group,
        'results': models.ResultEnum,
        'result_filter': result_filter,
        'table': table_by
    }

    return HttpResponse(template.render(context, request))


def issue_list(request):
    """Get all issues."""
    template = loader.get_template('web/issues_list.html')
    page = request.GET.get('page')
    search = request.GET.get('search', '')
    resolved = request.GET.get('resolved')

    issues = (
        models.Issue.objects.filter_authorized(request)
        .annotate(
            last_occurrence=Max(
                'issueoccurrence__related_checkout__start_time'
            )
        )
        .order_by(
            F('last_occurrence').desc(nulls_last=True),
            '-id',
        )
        .select_related(
            'kind'
        )
    )

    try:
        resolved = strtobool(resolved)
    except ValueError:
        resolved = None
    else:
        issues = issues.filter(resolved_on__isnotnull=resolved)

    if search:
        issues = issues.filter(
            Q(description__icontains=search) |
            Q(ticket_url__icontains=search)
        )

    for issue in issues:
        issue.checkouts = issue.get_checkouts(request)

    paginator = pagination.EndlessPaginator(issues, 10)

    context = {
        'issues': paginator.get_page(page),
        'search': search,
        'resolved': resolved,
    }

    return HttpResponse(template.render(context, request))


def issue_regex_view(request):
    # pylint: disable=too-many-locals,too-many-return-statements
    """Mark a pipeline's issue."""
    if request.method == "POST":
        action = request.POST.get('action')

        permissions = {
            'delete': 'datawarehouse.delete_issueregex',
            'edit': 'datawarehouse.change_issueregex',
            'new': 'datawarehouse.add_issueregex',
        }

        try:
            perm_required = permissions[action]
        except KeyError:
            return HttpResponseBadRequest(f'Heh, no idea how to do {action}.')

        if not request.user.has_perm(perm_required):
            raise PermissionDenied()

        issue_id = request.POST.get('issue_id_select')
        issue_regex_id = request.POST.get('issue_regex_id')
        text_match = request.POST.get('text_match', '').strip() or None
        file_name_match = request.POST.get('file_name_match', '').strip() or None
        test_name_match = request.POST.get('test_name_match', '').strip() or None

        if issue_id:
            issue = get_object_or_404(models.Issue.objects, id=issue_id)
            if not issue.is_write_authorized(request):
                raise PermissionDenied()

        if action == 'new':
            issue_regex = models.IssueRegex(
                issue=issue,
                text_match=text_match,
                file_name_match=file_name_match,
                test_name_match=test_name_match,
                created_by=request.user,
            )

            if not issue_regex.is_write_authorized(request):
                raise PermissionDenied()

            issue_regex.save()
            issue_regex_id = issue_regex.id
            redirect = reverse('views.issue_regex.get', args=[issue_regex.id])

        elif action == 'edit':
            issue_regex = get_object_or_404(models.IssueRegex.objects, id=issue_regex_id)
            if not issue_regex.is_write_authorized(request):
                raise PermissionDenied()

            issue_regex.issue = issue
            issue_regex.text_match = text_match
            issue_regex.file_name_match = file_name_match
            issue_regex.test_name_match = test_name_match
            issue_regex.last_edited_by = request.user

            issue_regex.save()
            redirect = reverse('views.issue_regex.get', args=[issue_regex.id])

        elif action == 'delete':
            issue_regex = get_object_or_404(models.IssueRegex.objects, id=issue_regex_id)

            if not issue_regex.is_write_authorized(request):
                raise PermissionDenied()

            issue_regex.delete()
            redirect = reverse('issue_regex')

        LOGGER.info('action="%s issueregex" user="%s" issueregex_id="%s"',
                    action, request.user.username, issue_regex_id)

    elif request.method == "GET":
        template = loader.get_template('web/issue_regexes.html')
        page = request.GET.get('page')

        issue_regexes = models.IssueRegex.objects.filter_authorized(request).order_by('-id')

        paginator = pagination.EndlessPaginator(issue_regexes, 30)
        context = {
            'issue_regexes': paginator.get_page(page),
            'issues': (
                models.Issue.objects
                .filter_authorized(request)
                .filter(resolved_on__isnull=True)
                .select_related('kind')
            )
        }

        return HttpResponse(template.render(context, request))

    return HttpResponseRedirect(redirect)


def status(request):
    """Status endpoint to check that everything is running fine."""
    # Test database
    if models.KCIDBCheckout.objects.first():
        # Evaluate and trigger lazy query execution
        pass

    return HttpResponse('👍')


def issue_get(request, issue_id):
    """Get a single issue."""
    if request.method == "GET":
        template = loader.get_template('web/issue.html')

        issue = get_object_or_404(
            models.Issue.objects.filter_authorized(request).distinct(),
            id=issue_id
        )

        checkouts = issue.get_checkouts(request)
        tests = (
            models.KCIDBTest.objects.filter_authorized(request)
            .filter(
                build__checkout__in=checkouts,
                issues=issue
            )
            .exclude(
                environment=None
            )
            .select_related(
                'build',
                'test',
            )
            .prefetch_related(
                'issues',
                'environment'
            )
        )

        affected_hosts = (
            tests.values('environment__fqdn')
                 .annotate(total_hits=Count('environment__fqdn'))
                 .order_by('-total_hits')
        )

        affected_archs = (
            tests.values('build__architecture')
                 .annotate(total_hits=Count('build__architecture'))
                 .order_by('-total_hits')
        )

        for arch in affected_archs:
            arch['name'] = dict(models.pipeline_models.ArchitectureEnum.choices)[arch['build__architecture']]

        context = {
            'issue': issue,
            'checkouts': checkouts,
            'affected_hosts': affected_hosts,
            'affected_archs': affected_archs,
            'tests': tests,
            'issues': (
                models.Issue.objects
                .filter_authorized(request)
                .filter(resolved_on__isnull=True)
                .select_related('kind')
            ),
            'issue_kinds': models.IssueKind.objects.all().order_by('id'),
            'git_trees': models.GitTree.objects.filter_authorized(request).order_by('name').distinct(),
            'policies': authorization.PolicyAuthorizationBackend.get_policies_write_authorized(request),
        }

        return HttpResponse(template.render(context, request))

    return HttpResponseNotAllowed(['GET'])


def issue_regex_get(request, issue_regex_id):
    """Get a single issue."""
    if request.method == "GET":
        template = loader.get_template('web/issue_regex.html')

        issue_regex = get_object_or_404(
            models.IssueRegex.objects.filter_authorized(request).distinct(),
            id=issue_regex_id
        )

        issues = (
            models.Issue.objects.filter_authorized(request)
            .filter(Q(resolved_on__isnull=True) | Q(id=issue_regex.issue.id))
            .select_related('kind')
        )

        context = {
            'issue_regex': issue_regex,
            'issues': issues,
        }

        return HttpResponse(template.render(context, request))

    return HttpResponseNotAllowed(['GET'])
