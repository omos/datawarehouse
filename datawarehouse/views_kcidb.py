"""KCIDB Views."""
import datetime

from cki_lib.logger import get_logger
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse
from django.utils import timezone

from . import authorization
from . import models
from . import pagination
from . import utils

LOGGER = get_logger(__name__)


def checkouts_list(request):
    """Get list of checkouts."""
    template = loader.get_template('web/kcidb/checkouts.html')
    page = request.GET.get('page')

    checkouts = models.KCIDBCheckout.objects.filter_authorized(request)
    checkouts, filters = utils.filter_checkouts_view(request, checkouts)

    paginator = pagination.EndlessPaginator(
        checkouts.values_list('iid', flat=True),
        30
    )
    checkout_iids = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .aggregated()
        .filter(iid__in=checkout_iids)
        .prefetch_related(
            'tree',
            'gitlabjob_set',
            'gitlabjob_set__pipeline',
        )
    )

    context = {
        'checkouts': checkouts,
        'paginator': checkout_iids,
        # Filter parameters
        'gittrees': models.GitTree.objects.filter_authorized(request).order_by('name').distinct(),
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def checkouts_list_by_failure(request, stage):
    """Show failed checkouts classified by stage."""
    template = loader.get_template('web/kcidb/checkouts_failures.html')
    page = request.GET.get('page')

    checkouts = models.KCIDBCheckout.objects.filter_authorized(request)

    objects = {
        'checkout': checkouts.filter(valid=False),
        'build': checkouts.filter(kcidbbuild__valid=False),
        'test': checkouts.filter(
            # Filter failed tests and exclude the waived ones.
            # .exclude() generated a *really* slow query. See cki-project/datawarehouse!258
            # waived__in=(False, None) doesn't handle NULL. See https://code.djangoproject.com/ticket/13579
            #
            # status in UNSUCCESSFUL_STATUSES & (waived==False | waived is NULL)
            Q(kcidbbuild__kcidbtest__status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES) &
            (
                Q(kcidbbuild__kcidbtest__waived=False) |
                Q(kcidbbuild__kcidbtest__waived__isnull=True)
            )
        )
    }

    if stage not in ['all'] + list(objects):
        return HttpResponseBadRequest(f'Not sure what {stage} is.')

    stages = list(objects) if stage == 'all' else [stage]

    # Get a list of iids of the checkouts with failures
    checkout_iids = []
    for stage_name in stages:
        checkouts, filters = utils.filter_checkouts_view(request, objects[stage_name])
        checkout_iids.extend(
            checkouts.values_list('iid', flat=True)
        )

    # Sort ids descendingly and remove duplicates
    checkout_iids = sorted(list(set(checkout_iids)), reverse=True)

    paginator = pagination.EndlessPaginator(checkout_iids, 30)
    checkout_iids_page = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .aggregated()
        .filter(iid__in=checkout_iids_page)
        .prefetch_related(
            'tree',
            'gitlabjob_set',
            'gitlabjob_set__pipeline',
        )
    )

    context = {
        'paginator': checkout_iids_page,
        'checkouts': checkouts,
        'stage_filter': stage,
        'stages': list(objects),
        # Filter parameters
        'gittrees': models.GitTree.objects.filter_authorized(request).order_by('name').distinct(),
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def checkouts_baselines_list(request):
    """Get list of baselines."""
    template = loader.get_template('web/kcidb/checkouts_baselines.html')

    last_2_months = timezone.now() - datetime.timedelta(days=60)
    checkouts_iids = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .filter(start_time__gte=last_2_months)
        .exclude(gitlabjob__pipeline__variables__key='mr_url')  # Remove checkouts linked to MRs
        .exclude(git_repository_url=None)  # Remove checkouts without necessary data
        .order_by('git_repository_url', 'git_repository_branch', 'tree__name', '-iid')
        .distinct('git_repository_url', 'git_repository_branch', 'tree__name')
        .values_list('iid', flat=True)
    )

    checkouts = (
        models.KCIDBCheckout.objects
        .annotated_by_architecture()
        .filter(iid__in=list(checkouts_iids))
        .order_by('git_repository_url', 'git_repository_branch', 'tree__name')
        .select_related('tree')
    )

    for checkout in checkouts:
        checkout.by_architecture = {}

        # Turn annotated fields into a dict for easier access in the template
        for arch in models.ArchitectureEnum:
            arch_data = {
                'builds': {
                    'ran': getattr(checkout, f'stats_{arch.name}_builds_ran_count'),
                    'failed': getattr(checkout, f'stats_{arch.name}_builds_failed_count'),
                },
                'tests': {
                    'ran': getattr(checkout, f'stats_{arch.name}_tests_ran_count'),
                    'failed': getattr(checkout, f'stats_{arch.name}_tests_failed_count'),
                    'failed_waived': getattr(checkout, f'stats_{arch.name}_tests_failed_waived_count'),
                },
                'known_issues': (
                    getattr(checkout, f'stats_{arch.name}_builds_with_issues_count') +
                    getattr(checkout, f'stats_{arch.name}_tests_with_issues_count')
                )
            }
            checkout.by_architecture[arch.name] = arch_data

    context = {
        'checkouts': checkouts,
        'architectures': models.ArchitectureEnum,
    }

    return HttpResponse(template.render(context, request))


def checkouts_get(request, checkout_id):
    """Get a single checkout."""
    template = loader.get_template('web/kcidb/checkout.html')

    checkout_filter = utils.query_id_or_iid(checkout_id)

    checkout = (
        models.KCIDBCheckout.objects.aggregated()
        .select_related(
            'log',
            'origin',
            'tree',
        )
        .prefetch_related(
            'patches',
            'kcidbbuild_set',
            'kcidbbuild_set__kcidbtest_set',
            'kcidbbuild_set__kcidbtest_set__test',
        )
    )
    checkout = get_object_or_404(checkout, **checkout_filter)

    if not checkout.is_read_authorized(request):
        raise Http404()

    tests = (
        models.KCIDBTest.objects
        .filter(
            build__checkout=checkout
        )
        .select_related(
            'build',
            'test',
        )
        .prefetch_related(
            'issues',
        )
    )

    builds = (
        models.KCIDBBuild.objects
        .aggregated()
        .filter(
            checkout=checkout
        )
        .prefetch_related(
            'kcidbtest_set',
            'kcidbtest_set__test',
        )
    )

    issues = (
        models.Issue.objects
        .filter(
            resolved_on__isnull=True
        )
        .select_related(
            'kind'
        )
    )

    grouped_issues = utils.group_issue_occurrences(
        models.IssueOccurrence.objects.filter(
            Q(kcidb_checkout=checkout) |
            Q(kcidb_build__in=builds) |
            Q(kcidb_test__in=tests)
        )
    )

    context = {
        'builds': builds,
        'builds_failed': builds.exclude(valid=True),
        'issues': issues,
        'grouped_issues': grouped_issues,
        'checkout': checkout,
        'checkouts_failed': [checkout] if not checkout.valid else [],
        'tests': tests,
        'tests_failed': tests.exclude(status=models.ResultEnum.PASS),
    }

    return HttpResponse(template.render(context, request))


def builds_list(request):
    """Get list of builds."""
    template = loader.get_template('web/kcidb/builds.html')
    page = request.GET.get('page')

    builds = models.KCIDBBuild.objects.filter_authorized(request)
    builds, filters = utils.filter_checkouts_view(request, builds, path_to_checkout='checkout__')
    builds, filters_build = utils.filter_builds_view(request, builds)

    filters.update(filters_build)

    paginator = pagination.EndlessPaginator(
        builds.values_list('iid', flat=True),
        30
    )
    build_iids = paginator.get_page(page)

    builds = (
        models.KCIDBBuild.objects
        .aggregated()
        .filter(iid__in=build_iids)
        .prefetch_related(
            'checkout',
            'checkout__tree',
            'gitlabjob_set',
            'gitlabjob_set__pipeline',
        )
    )

    context = {
        'builds': builds,
        'paginator': build_iids,
        # Filter parameters
        'architectures': models.ArchitectureEnum,
        'gittrees': models.GitTree.objects.filter_authorized(request).order_by('name').distinct(),
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def builds_get(request, build_id):
    """Get a single build."""
    template = loader.get_template('web/kcidb/build.html')

    build_filter = utils.query_id_or_iid(build_id)

    build = (
        models.KCIDBBuild.objects.aggregated()
        .select_related(
            'compiler',
            'log',
            'origin',
            'checkout',
        )
        .prefetch_related(
            'input_files',
            'output_files',
            'kcidbtest_set',
            'kcidbtest_set__output_files',
            'kcidbtest_set__test',
        )
    )

    build = get_object_or_404(build, **build_filter)

    if not build.is_read_authorized(request):
        raise Http404()

    tests = (
        models.KCIDBTest.objects
        .filter(
            build=build
        )
        .select_related(
            'build',
            'test',
        )
        .prefetch_related(
            'issues',
        )
    )

    issues = (
        models.Issue.objects
        .filter(
            resolved_on__isnull=True
        )
        .select_related(
            'kind'
        )
    )

    grouped_issues = utils.group_issue_occurrences(
        models.IssueOccurrence.objects.filter(
            Q(kcidb_build=build) |
            Q(kcidb_test__in=tests)
        )
    )

    context = {
        'build': build,
        'builds_failed': [build] if not build.valid else [],
        'issues': issues,
        'grouped_issues': grouped_issues,
        'tests': tests,
        'tests_failed': tests.exclude(status=models.ResultEnum.PASS),
    }

    return HttpResponse(template.render(context, request))


def tests_get(request, test_id):
    """Get a single test."""
    template = loader.get_template('web/kcidb/test.html')

    test_filter = utils.query_id_or_iid(test_id)

    test = (
        models.KCIDBTest.objects
        .select_related(
            'build',
            'build__checkout',
            'environment',
            'origin',
            'test',
        )
        .prefetch_related(
            'output_files',
        )
    )

    test = get_object_or_404(test, **test_filter)

    if not test.is_read_authorized(request):
        raise Http404()

    issues = (
        models.Issue.objects
        .filter(
            resolved_on__isnull=True
        )
        .select_related(
            'kind'
        )
    )

    grouped_issues = utils.group_issue_occurrences(
        models.IssueOccurrence.objects.filter(
            Q(kcidb_test=test)
        )
    )

    context = {
        'test': test,
        'tests_failed': [test] if not test.status or test.status != models.ResultEnum.PASS else [],
        'issues': issues,
        'grouped_issues': grouped_issues,
    }

    return HttpResponse(template.render(context, request))


def kcidb_issue(request):
    """Link/Unlink checkouts, builds or tests to a given issue."""
    if request.method == "POST":
        objects = {
            'checkout': {
                'permission': 'datawarehouse.change_kcidbcheckout',
                'elements': models.KCIDBCheckout.objects.filter(
                    iid__in=request.POST.getlist('checkout_iids')
                ),
            },
            'build': {
                'permission': 'datawarehouse.change_kcidbbuild',
                'elements': models.KCIDBBuild.objects.filter(
                    iid__in=request.POST.getlist('build_iids')
                ),
            },
            'test': {
                'permission': 'datawarehouse.change_kcidbtest',
                'elements': models.KCIDBTest.objects.filter(
                    iid__in=request.POST.getlist('test_iids')
                ),
            },
        }

        # Check all permissions before performing any change.
        for obj in objects.values():
            if obj['elements'] and not request.user.has_perm(obj['permission']):
                raise PermissionDenied()

            all_objects_authorized = authorization.PolicyAuthorizationBackend.all_objects_authorized(
                request,
                obj['elements'],
            )
            if not all_objects_authorized:
                raise Http404()

        issue_id = request.POST.get('issue_id')
        issue = models.Issue.objects.get(id=issue_id)

        action = request.POST.get('action', 'add')
        for obj in objects.values():
            for element in obj['elements']:
                if action == 'add':
                    element.issues.add(issue, through_defaults={'created_by': request.user})
                elif action == 'remove':
                    element.issues.remove(issue)
                else:
                    HttpResponseBadRequest(f'Action {action} unknown.')

                LOGGER.info('action="%s issue on %s" user="%s" issue_id="%s" iid="%s"',
                            action, element.__class__.__name__, request.user.username, issue.id, element.iid)

    return HttpResponseRedirect(request.POST.get('redirect_to'))


def search(request):
    """Search for checkouts by id or by pipeline_id."""
    template = loader.get_template('web/search.html')
    page = request.GET.get('page')
    query = request.GET.get('q', '').strip()

    if not query:
        return HttpResponse(template.render({}, request))

    query_filter = (
        Q(id__icontains=query)
    )

    if query.isdigit():
        query_filter |= (
            Q(iid=query) |
            Q(gitlabjob__job_id=query) |
            Q(gitlabjob__pipeline__pipeline_id=query)
        )

    checkouts = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .filter(query_filter)
    )

    paginator = pagination.EndlessPaginator(
        checkouts.values_list('iid', flat=True),
        30
    )
    checkout_iids = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .aggregated()
        .filter(iid__in=checkout_iids)
        .prefetch_related(
            'tree',
            'gitlabjob_set',
            'gitlabjob_set__pipeline',
        )
    )

    context = {
        'checkouts': checkouts,
        'paginator': checkout_iids,
        'query': query,
    }

    return HttpResponse(template.render(context, request))


def revision_redirect(request, revision_iid):
    """
    Resolve revision urls into checkouts.

    Keep /kcidb/revisions/{iid} compatibility for old links.
    """
    return HttpResponseRedirect(
        reverse('views.kcidb.checkouts', args=[revision_iid])
    )
