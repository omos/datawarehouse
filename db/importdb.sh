#!/bin/bash

set -eu

POSTGRES_USER="${POSTGRES_USER:-datawarehouse}"
POSTGRES_NAME="${POSTGRES_NAME:-datawarehouse}"
DUMP_FILE="${DUMP_FILE:-database-dump.sql.gz}"
DUMP_FILE_PATH="docker-entrypoint-initdb.d/${DUMP_FILE}"

if [ -r "$DUMP_FILE_PATH" ]; then
    echo "Importing $DUMP_FILE"

    (
        echo 'DO $$ DECLARE'
        echo '    r RECORD;'
        echo 'BEGIN'
        echo '    FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP'
        echo "        EXECUTE 'DROP TABLE ' || quote_ident(r.tablename) || ' CASCADE';"
        echo '    END LOOP;'
        echo 'END $$;'
        zcat "$DUMP_FILE_PATH"
    ) | psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_NAME"
else
    echo "No database to import"
fi
