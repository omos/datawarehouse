# Patches

## List

Get a list of merged patches in a pipeline.

`GET /api/1/pipeline/$pipeline_id/patches`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `pipeline_id` | `int` | Yes | ID of the pipeline. |

Example of response:

```json

{
    "count": 1,
    "next": null,
    "previous": null,
    "results": {
        "patches": [
            {
                "url": "http://patchwork/api/patches/2134368/",
                "web_url": "http://patchwork/patch/2134368/",
                "subject": "[1/1] Patch subject",
                "series": [
                    {
                        "series_id": 58047,
                        "url": "http://patchwork/api/series/58047/",
                        "web_url": "http://patchwork/project/foobar/list/?series=58047",
                        "name": "Patch subject",
                        "submitter": {
                            "name": "Mr Red Hat",
                            "email": "mr@redhat.com",
                            "submitter_id": 21
                        },
                        "project": {
                            "name": "foobar",
                            "project_id": 24
                        },
                        "submitted_date": "2020-04-22T12:57:53Z"
                    }
                ],
                "msgid": "<20200422125755.32594-3-mr@redhat.com>",
                "mbox": "http://patchwork/patch/2134368/mbox/",
                "patch_id": 2134368
            },
        ]
    }
}
```
