# Database

DataWarehouse uses [PostgreSQL] as database backend.

## Schema

The database schema changes frequently.
Running the following command it's possible to get an up to date visualization.

- Podman:

```bash
podman exec -it datawarehouse-web ./manage.py graph_models -a > dw.dot
dot -Tx11 dw.dot
```

- Docker Compose:

```bash
docker-compose exec web ./manage.py graph_models -a > dw.dot
dot -Tx11 dw.dot
```

That will give you a nice diagram of the Django models used in the
app. You might have to install the `graphviz` package if you haven't
already.

[PostgreSQL]:https://www.postgresql.org/
