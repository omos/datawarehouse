# Feature flags

Some features are enabled or disabled by setting feature flags on the
deployment environment.

These configurations allow customization of certain behaviours of the
Datawarehouse.

<!-- markdownlint-disable line-length -->
| Variable Name                                 | Default | Description                                                         |
|-----------------------------------------------|---------|---------------------------------------------------------------------|
| `FF_ALLOW_DEBUG_PIPELINES`                    | `False` | Allow submitting debug pipelines                                    |
| `FF_CACHE_ANONYMOUS`                          | `False` | Cache anonymous requests                                            |
| `FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS`  |  `30`   | Amount of days before not confirmed accounts are deleted            |
| `FF_EXTERNAL_AVATAR_ENABLED`                  | `True`  | Use external sevice for user avatars. By default Librabatar is used |
| `FF_LDAP_GROUP_SYNC`                          | `False` | Enable LDAP groups synchronization                                  |
| `FF_NOTIFY_ISSUE_REGRESSION`                  | `False` | Enable sending of Issue Regression emails                           |
| `FF_SAML_ENABLED`                             | `False` | Enable SAML2                                                        |
| `FF_SIGNUP_ENABLED`                           | `False` | Enable the creation of new accounts on the UI                       |
<!-- markdownlint-restore -->
