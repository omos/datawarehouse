"""Test for issues API."""
import json

from datawarehouse import models
from datawarehouse import serializers
from tests import utils


class TestCheckoutIssues(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Test issues in checkouts."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/issues.yaml',
    ]
    _obj = models.KCIDBCheckout
    _iid = 1
    _kcidb_endpoint = 'kcidb/checkouts/{self._iid}'
    permission_name = 'kcidbcheckout'

    @property
    def obj(self):
        """Return the tested object."""
        return self._obj.objects.get(iid=1)

    @property
    def kcidb_endpoint(self):
        """Return the endpoint formatted."""
        return self._kcidb_endpoint.format(self=self)

    def setUp(self):
        """Set up data."""
        self.issue_1 = models.Issue.objects.get(id=1)
        self.issue_2 = models.Issue.objects.get(id=2)

    def test_create_issues(self):
        """Test adding an issue."""
        response = self.assert_authenticated_post(
            201, f'add_{self.permission_name}',
            f'/api/1/{self.kcidb_endpoint}/issues',
            json.dumps({'issue_id': self.issue_1.id}), content_type="application/json")

        self.assertDictEqual(
            serializers.IssueSerializer(self.issue_1).data,
            response.json()
        )

        self.assertEqual(1, self.obj.issues.count())
        self.assertEqual(self.issue_1.id, self.obj.issues.get().id)

        response = self.assert_authenticated_post(
            201, f'add_{self.permission_name}',
            f'/api/1/{self.kcidb_endpoint}/issues',
            json.dumps({'issue_id': self.issue_2.id}), content_type="application/json")

        self.assertEqual(2, self.obj.issues.count())

    def test_get_issue_single(self):
        """Test getting an issue."""
        self.obj.issues.add(self.issue_1)
        self.assertEqual(1, self.obj.issues.count())

        response_single = self.client.get(
            f'/api/1/{self.kcidb_endpoint}/issues/{self.issue_1.id}',
        )
        self.assertEqual(
            serializers.IssueSerializer(self.issue_1).data,
            response_single.json()
        )

    def test_get_issue_list(self):
        """Test getting issues."""
        self.obj.issues.add(self.issue_1, self.issue_2)
        self.assertEqual(2, self.obj.issues.count())

        response_list = self.client.get(
            f'/api/1/{self.kcidb_endpoint}/issues',
        )
        self.assertEqual(
            serializers.IssueSerializer([self.issue_2, self.issue_1], many=True).data,
            response_list.json()['results']
        )

    def test_delete_issues(self):
        """Test adding an issue."""
        self.obj.issues.add(self.issue_1, self.issue_2)
        self.assertEqual(2, self.obj.issues.count())

        self.assert_authenticated_delete(
            204, f'delete_{self.permission_name}',
            f'/api/1/{self.kcidb_endpoint}/issues/{self.issue_1.id}',
            content_type="application/json")

        self.assertEqual(1, self.obj.issues.count())
        self.assertEqual(self.issue_2.id, self.obj.issues.get().id)


class TestBuildIssues(TestCheckoutIssues):
    """Test issues in builds."""

    _obj = models.KCIDBBuild
    _iid = 1
    _kcidb_endpoint = 'kcidb/builds/{self._iid}'
    permission_name = 'kcidbbuild'


class TestTestIssues(TestCheckoutIssues):
    """Test issues in tests."""

    _obj = models.KCIDBTest
    _iid = 1
    _kcidb_endpoint = 'kcidb/tests/{self._iid}'
    permission_name = 'kcidbtest'
