# pylint: disable=too-many-lines
"""Test model creation from KCIDB data."""
import datetime
import json
import os
import pathlib
from unittest import mock

from django.db.models.query import QuerySet
import responses

from datawarehouse import models
from tests import utils


def load_json(name):
    """Load json file from assets."""
    file_name = os.path.join(utils.ASSETS_DIR, name)
    file_content = pathlib.Path(file_name).read_text()

    return json.loads(file_content)


def mock_patch():
    """Mock patch requests."""
    patch_body = """MIME-Version: 1.0
Subject: [RHEL PATCH 206/206] fix some stuff, and break some other
commit 56887cffe946bb0a90c74429fa94d6110a73119d
Author: Patch Author <patch@author.com>
Date:   Mon Feb 22 10:48:09 2021 +0100

    fix some stuff, and break some other
    """
    responses.add(responses.GET, 'http://patchwork.server/patch/2322797/mbox/', body=patch_body)


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestPipeline(utils.TestCase):
    """Test creation of Pipeline model."""

    def test_create_pipeline_models(self):
        """Check pipeline model is created correctly."""
        misc = {
            'job': {
                'id': 887316,
            },
            'pipeline': {
                'id': 592705,
                'variables': {
                    'cki_pipeline_type': 'patchwork',
                    'kernel_type': 'internal'
                },
                'ref': 'rhel8',
                'project': {
                    'id': 2,
                    'path_with_namespace': 'cki-project/cki-pipeline',
                    'instance_url': 'https://gitlab.com'
                }
            }
        }

        pipeline = models.Pipeline.create_from_misc(misc)
        self.assertEqual(2, pipeline.project.project_id)
        self.assertEqual('cki-project/cki-pipeline', pipeline.project.path)
        self.assertEqual('https://gitlab.com', pipeline.project.instance_url)
        self.assertEqual('rhel8', pipeline.gittree.name)

        self.assertDictEqual(
            {'cki_pipeline_type': 'patchwork', 'kernel_type': 'internal'},
            pipeline.trigger_variables
        )

    def test_create_with_checkout(self):
        """Test creating a Pipeline with a linked KCIDBCheckout."""
        misc = {
            'pipeline': {
                'id': 592705,
                'ref': 'rhel8',
                'variables': {},
                'project': {
                    'id': 2,
                    'path_with_namespace': 'cki-project/cki-pipeline',
                    'instance_url': 'https://gitlab.com'
                }
            }
        }

        checkout = models.KCIDBCheckout.create_from_json(
            {'id': 'redhat:123', 'origin': 'redhat'}
        )

        pipeline = models.Pipeline.create_from_misc(misc, checkout)
        self.assertEqual(pipeline.kcidb_checkout, checkout)

        # Call create_from_misc again, without checkout parameter.
        # Ensure kcidb_checkout was not deleted.
        pipeline = models.Pipeline.create_from_misc(misc)
        self.assertEqual(pipeline.kcidb_checkout, checkout)


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBCheckoutFromJson(utils.TestCase):
    """Test creation of KCIDBCheckout model."""

    def test_basic(self):
        """Submit only id."""
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3+'
                   '14f1076637e351743158c458afa5ee5032dc26844a4c923dabb4846c3d0fa197'),
            'origin': 'redhat',
        }
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsInstance(checkout, models.KCIDBCheckout)

    def test_re_submit(self):
        """Submit twice. Updates current data."""
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
        }
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsNone(checkout.message_id)

        # Re submit with new data.
        data['message_id'] = '<foo@bar.com>'
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(checkout.message_id, data['message_id'])

        # Re submit with new data, without the previous value.
        del data['message_id']
        data['valid'] = True
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsNotNone(checkout.message_id)
        self.assertEqual(checkout.valid, data['valid'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBCheckout.objects.filter(id=data['id']).count()
        )

    @responses.activate
    def test_patch(self):
        """Submit email patches."""
        body = (
            b'From foo@baz Mon 25 Nov 2019 02:27:19 PM CET\n'
            b'From: Some One <some-one@redhat.com>\n'
            b'Date: Tue, 19 Nov 2019 23:47:33 +0100\n'
            b'Subject: fix something somewhere\n'
            b'..')
        responses.add(responses.GET, 'http://some-patch.server/patch/1234', body=body)
        responses.add(responses.GET, 'http://some-patch.server/patch/1235', body=body)
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'patchset_files': [
                {'url': 'http://some-patch.server/patch/1234', 'name': '1234'},
                {'url': 'http://some-patch.server/patch/1235', 'name': '1235'}
            ],
        }

        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(2, checkout.patches.count())
        self.assertFalse(hasattr(checkout.patches.first(), 'patchworkpatch'))

    def test_log_url(self):
        """Submit log_url."""
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'log_url': 'http://log.server/log.name',
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertIsInstance(checkout.log, models.Artifact)
        self.assertEqual('http://log.server/log.name', checkout.log.url)
        self.assertEqual('log.name', checkout.log.name)

    def test_contacts(self):
        """Test contact submission."""
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>']
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertEqual(2, checkout.contacts.count())
        contact_1 = checkout.contacts.get(email='someone@email.com')
        self.assertEqual('', contact_1.name)
        contact_2 = checkout.contacts.get(email='some-other@mail.com')
        self.assertEqual('Some Other', contact_2.name)

    @responses.activate
    def test_all_data(self):
        """Check it creates all the pipeline models."""
        mock_patch()
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'tree_name': 'arm',
            'git_repository_url': 'https://repository.com/repo/kernel-rhel',
            'git_repository_branch': 'rhel-1.2.3',
            'git_commit_hash': '403cbf29a4e277ad4872515ec3854b175960bbdf',
            'git_commit_name': 'commit name',
            'patchset_files': [{'url': 'http://patchwork.server/patch/2322797/mbox/',
                                'name': 'mbox'}],
            'patchset_hash': '21c9a43d22cd02babb34b45a9defb881ae8228f0d034a0779b1321e851cad6a4',
            'message_id': '<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>',
            'comment': 'this is the comment',
            'start_time': '2020-06-01T06:47:41.108Z',
            'valid': True,
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>'],
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'misc': {'pipeline': {'variables': {'cki_pipeline_type': 'patchwork'}}},
        }

        models.KCIDBCheckout.create_from_json(data)

        # Submit a base version without data to make sure resubmitting empty does not
        # override missing fields,
        checkout = models.KCIDBCheckout.create_from_json({
            'id': data['id'],
            'origin': data['origin']
        })

        self.assertEqual(
            ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
             'decd6167bf4f6bec1284006d0522381b44660df3'),
            checkout.id
        )
        self.assertIsInstance(checkout.origin, models.KCIDBOrigin)
        self.assertEqual('redhat', checkout.origin.name)

        self.assertIsInstance(checkout.tree, models.GitTree)
        self.assertEqual('arm', checkout.tree.name)

        self.assertEqual('https://repository.com/repo/kernel-rhel', checkout.git_repository_url)
        self.assertEqual('rhel-1.2.3', checkout.git_repository_branch)
        self.assertEqual('403cbf29a4e277ad4872515ec3854b175960bbdf', checkout.git_commit_hash)
        self.assertEqual('commit name', checkout.git_commit_name)
        self.assertEqual('<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>', checkout.message_id)
        self.assertEqual('this is the comment', checkout.comment)
        self.assertEqual(True, checkout.valid)
        self.assertEqual('21c9a43d22cd02babb34b45a9defb881ae8228f0d034a0779b1321e851cad6a4', checkout.patchset_hash)
        self.assertEqual('Some\nLog\nLines', checkout.log_excerpt)
        self.assertEqual(models.KernelTypeEnum.INTERNAL, checkout.kernel_type)
        self.assertEqual(models.Policy.INTERNAL, checkout.policy.name)
        self.assertEqual(
            datetime.datetime(2020, 6, 1, 6, 47, 41, 108000, tzinfo=datetime.timezone.utc),
            checkout.start_time
        )

    @responses.activate
    def test_create_pipeline_models(self):
        """Check it creates all the pipeline models."""
        mock_patch()
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'patchset_files': [{'url': 'http://patchwork.server/patch/2322797/mbox/',
                                'name': 'mbox'}],
            'log_url': 'http://log.server/log.name',
            'valid': True,
            'misc': {
                'job': {
                    'id': 887316,
                    'name': 'merge',
                    'stage': 'merge',
                    'started_at': '2020-06-03T15:11:19.327Z',
                    'created_at': '2020-06-03T15:08:05.512Z',
                    'finished_at': '2020-06-03T15:14:55.148Z',
                    'duration': 215.820987,
                    'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                    'tag': '-209.el8',
                    'commit_message_title': '[redhat] kernel',
                    'kernel_version': '1.2.3'
                },
                'pipeline': {
                    'id': 592705,
                    'variables': {
                        'cki_pipeline_type': 'patchwork',
                        'kernel_type': 'upstream',
                    },
                    'started_at': '2020-06-03T15:08:09.957Z',
                    'created_at': '2020-06-03T15:08:05.288Z',
                    'finished_at': None,
                    'duration': None,
                    'ref': 'rhel8',
                    'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                    'project': {
                        'id': 2,
                        'path_with_namespace': 'cki-project/cki-pipeline',
                        'instance_url': 'https://gitlab.com'
                    }
                }
            }
        }

        checkout = models.KCIDBCheckout.create_from_json(data)
        gitlab_job = checkout.gitlabjob_set.first()

        self.assertIsNotNone(gitlab_job)
        self.assertIsNotNone(gitlab_job.pipeline)
        self.assertEqual(887316, gitlab_job.job_id)
        self.assertEqual(592705, gitlab_job.pipeline.pipeline_id)
        self.assertEqual(gitlab_job.pipeline.kcidb_checkout, checkout)

        self.assertTrue(gitlab_job.pipeline.patches.count())
        self.assertEqual(
            sorted([patch.url for patch in checkout.patches.all()]),
            sorted([patch.url for patch in gitlab_job.pipeline.patches.all()])
        )

        self.assertEqual(models.KernelTypeEnum.UPSTREAM, checkout.kernel_type)
        self.assertEqual(models.Policy.PUBLIC, checkout.policy.name)
        self.assertEqual('1.2.3', checkout.kernel_version)


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBBuildFromJson(utils.TestCase):
    """Test creation of KCIDBBuild model."""

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set Up."""
        models.KCIDBCheckout.create_from_json(
            {
                'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git'
                       '@decd6167bf4f6bec1284006d0522381b44660df3'),
                'origin': 'redhat',
            }
        )

    def test_basic(self):
        """Submit only id."""
        data = {
            'checkout_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
        }
        build = models.KCIDBBuild.create_from_json(data)

        self.assertIsInstance(build, models.KCIDBBuild)
        self.assertIsInstance(build.checkout, models.KCIDBCheckout)
        self.assertEqual(
            ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
             'decd6167bf4f6bec1284006d0522381b44660df3'),
            build.checkout.id
        )
        self.assertEqual('redhat:887318', build.id)
        self.assertIsInstance(build.origin, models.KCIDBOrigin)
        self.assertEqual('redhat', build.origin.name)

    def test_re_submit(self):
        """Submit submit multiple times. Updates current data."""
        data = {
            'checkout_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
        }
        build = models.KCIDBBuild.create_from_json(data)
        self.assertIsNone(build.valid)

        # Re submit with new data.
        data['valid'] = False
        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual(build.valid, data['valid'])

        # Re submit with new data, without the previous value.
        del data['valid']
        data['comment'] = 'foobar'
        build = models.KCIDBBuild.create_from_json(data)
        self.assertIsNotNone(build.valid)
        self.assertEqual(build.comment, data['comment'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBBuild.objects.filter(id=data['id']).count()
        )

    def test_submit_files(self):
        """Test files submission. log_url, input_files and output_files."""
        data = {
            'checkout_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        build = models.KCIDBBuild.create_from_json(data)

        # log_url
        self.assertIsInstance(build.log, models.Artifact)
        self.assertEqual('http://log.server/log.name', build.log.url)
        self.assertEqual('log.name', build.log.name)

        # input_files
        self.assertListEqual(
            [{'url': 'http://log.server/input.file', 'name': 'input.file'},
             {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'}],
            [{'url': file.url, 'name': file.name} for file in build.input_files.all()]
        )

        # output_files
        self.assertListEqual(
            [{'url': 'http://log.server/output.file', 'name': 'output.file'},
             {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'}],
            [{'url': file.url, 'name': file.name} for file in build.output_files.all()]
        )

    def test_re_submit_files(self):
        """Submit files multiple times."""
        data = {
            'checkout_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        models.KCIDBBuild.create_from_json(data)
        self.assertEqual(
            4, models.Artifact.objects.count()
        )

        models.KCIDBBuild.create_from_json(data)
        self.assertEqual(
            4, models.Artifact.objects.count()
        )

    def test_all_data(self):
        """Test complete object submission."""
        data = {
            'checkout_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            'start_time': '2020-06-03T15:14:57.215Z',
            'duration': 632,
            'architecture': 'aarch64',
            'command': 'make rpmbuild ...',
            'compiler': 'aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)',
            'config_name': 'fedora',
            'valid': True,
        }
        models.KCIDBBuild.create_from_json(data)

        # Submit a base version without data to make sure resubmitting empty does not
        # override missing fields,
        build = models.KCIDBBuild.create_from_json({
            'checkout_id': data['checkout_id'],
            'id': data['id'],
            'origin': data['origin']
        })

        self.assertEqual(
            datetime.datetime(2020, 6, 3, 15, 14, 57, 215000, tzinfo=datetime.timezone.utc),
            build.start_time
        )
        self.assertEqual(632, build.duration)
        self.assertEqual('make rpmbuild ...', build.command)
        self.assertEqual('fedora', build.config_name)
        self.assertEqual(True, build.valid)

        self.assertEqual(models.ArchitectureEnum['aarch64'], build.architecture)
        self.assertIsInstance(build.compiler, models.Compiler)
        self.assertEqual('aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)', build.compiler.name)
        self.assertEqual('Some\nLog\nLines', build.log_excerpt)

    def test_create_pipeline_models(self):
        """Check it creates all the pipeline models."""
        data = {
            'checkout_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            'start_time': '2020-06-03T15:14:57.215Z',
            'duration': 632,
            'architecture': 'aarch64',
            'command': 'make rpmbuild ..',
            'compiler': 'aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)',
            'config_name': 'fedora',
            'valid': True,
            'misc': {
                'job': {
                    'id': 887318,
                    'name': 'build aarch64',
                    'stage': 'build',
                    'started_at': '2020-06-03T15:14:57.215Z',
                    'created_at': '2020-06-03T15:08:05.615Z',
                    'finished_at': '2020-06-03T15:27:00.666Z',
                    'duration': 723.450641,
                    'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                    'tag': '-209.el8',
                    'commit_message_title': '[redhat] kernel-4.18.0-209.el8',
                    'kernel_version': '4.18.0-209.el8.cki'
                },
                'pipeline': {
                    'id': 592705,
                    'variables': {
                        'cki_pipeline_type': 'patchwork',
                    },
                    'started_at': '2020-06-03T15:08:09.957Z',
                    'created_at': '2020-06-03T15:08:05.288Z',
                    'finished_at': '2020-06-04T00:50:23.262Z',
                    'duration': 34916,
                    'ref': 'rhel8',
                    'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                    'project': {
                        'id': 2,
                        'path_with_namespace': 'cki-project/cki-pipeline',
                        'instance_url': 'https://gitlab.com'
                    }
                }
            }
        }

        build = models.KCIDBBuild.create_from_json(data)
        gitlab_job = build.gitlabjob_set.first()

        self.assertIsNotNone(gitlab_job)
        self.assertIsNotNone(gitlab_job.pipeline)
        self.assertEqual(887318, gitlab_job.job_id)
        self.assertEqual(592705, gitlab_job.pipeline.pipeline_id)


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBTestFromJson(utils.TestCase):
    """Test creation of KCIDBTest model."""

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set Up."""
        models.KCIDBCheckout.create_from_json(
            {
                'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git'
                       '@decd6167bf4f6bec1284006d0522381b44660df3'),
                'origin': 'redhat',
            }
        )
        models.KCIDBBuild.create_from_json(
            {
                'checkout_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git'
                                '@decd6167bf4f6bec1284006d0522381b44660df3'),
                'origin': 'redhat',
                'id': 'redhat:887318',
                'architecture': 'aarch64',
            }
        )

    def test_basic(self):
        """Submit only id."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertIsInstance(test, models.KCIDBTest)
        self.assertIsInstance(test.build, models.KCIDBBuild)
        self.assertEqual('redhat:887318', test.build.id)

        # These are False by default
        self.assertFalse(test.targeted)
        self.assertFalse(test.kernel_debug)

    def test_re_submit(self):
        """Submit multiple times. Updates current data."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        test = models.KCIDBTest.create_from_json(data)
        self.assertIsNone(test.waived)

        # Re submit with new data.
        data['waived'] = True
        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(test.waived, data['waived'])

        # Re submit with new data, without the previous value.
        del data['waived']
        data['duration'] = 123
        test = models.KCIDBTest.create_from_json(data)
        self.assertIsNotNone(test.waived)
        self.assertEqual(test.duration, data['duration'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBTest.objects.filter(id=data['id']).count()
        )

    def test_output_files(self):
        """Check created artifacts for output_files."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertListEqual(
            [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            [{'url': file.url, 'name': file.name} for file in test.output_files.all()]
        )

    def test_maintainers_empty(self):
        """Test maintainers field as comma separated string."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
            'misc': {'maintainers': None}
        }
        models.KCIDBTest.create_from_json(data)

    def test_maintainers_not_empty(self):
        """Test maintainers field as dict."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
            'misc': {
                'maintainers': [
                    {'name': 'Maintainer One', 'email': 'one@maintainer.com'},
                    {'email': 'two@maintainer.com', 'gitlab': 'two'},
                    {'email': 'three@maintainer.com'},
                ]
            }
        }
        test = models.KCIDBTest.create_from_json(data)

        cases = [
            ('Maintainer One', 'one@maintainer.com', None),
            ('', 'two@maintainer.com', 'two'),
            ('', 'three@maintainer.com', None),
        ]

        for name, email, gitlab_username in cases:
            self.assertTrue(
                test.test.maintainers.filter(
                    name=name, email=email, gitlab_username=gitlab_username).exists(),
                (name, email)
            )

    def test_maintainers_overwrite(self):
        """Test behaviour with empty maintainers list. Should not overwrite."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
            'misc': {
                'maintainers': [
                    {'name': 'Maintainer One', 'email': 'one@maintainer.com'},
                ]
            }
        }
        models.KCIDBTest.create_from_json(data)

        # Resubmit test without maintainers
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
        }
        kcidb_test = models.KCIDBTest.create_from_json(data)

        self.assertEqual(
            'Maintainer One',
            kcidb_test.test.maintainers.get().name
        )

    def test_all_data(self):
        """Test complete object submission."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'environment': {
                'comment': 'hostname.redhat.com'
            },
            'path': 'boot',
            'comment': 'Boot test',
            'waived': False,
            'start_time': '2020-06-03T15:52:25Z',
            'duration': 158,
            'status': 'PASS',
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
            ],
            'misc': {
                'maintainers': [
                    {'name': 'Cosme Fulanito', 'email': 'cosme@fulanito.com'},
                    {'name': 'Pepe', 'email': 'pe@pe.com'}
                ],
                'debug': True,
                'targeted': True,
            }
        }
        models.KCIDBTest.create_from_json(data)

        # Submit a base version without data to make sure resubmitting empty does not
        # override missing fields,
        test = models.KCIDBTest.create_from_json({
            'build_id': data['build_id'],
            'id': data['id'],
            'origin': data['origin'],
        })

        self.assertIsInstance(test.environment, models.BeakerResource)
        self.assertEqual('hostname.redhat.com', test.environment.fqdn)

        self.assertEqual('P', test.status)

        self.assertIsInstance(test.test, models.Test)
        self.assertEqual('boot', test.test.universal_id)
        self.assertEqual('Boot test', test.test.name)
        self.assertEqual(2, test.test.maintainers.count())
        self.assertTrue(
            test.test.maintainers.filter(name='Cosme Fulanito', email='cosme@fulanito.com').exists()
        )
        self.assertTrue(
            test.test.maintainers.filter(name='Pepe', email='pe@pe.com').exists()
        )

        self.assertEqual(False, test.waived)
        self.assertEqual(
            datetime.datetime(2020, 6, 3, 15, 52, 25, 0, tzinfo=datetime.timezone.utc),
            test.start_time
        )
        self.assertEqual(158, test.duration)

        self.assertEqual(1, test.output_files.count())
        self.assertTrue(test.kernel_debug)
        self.assertTrue(test.targeted)

        self.assertEqual('http://log.server/log.name', test.log.url)
        self.assertEqual('log.name', test.log.name)
        self.assertEqual('Some\nLog\nLines', test.log_excerpt)

    def test_create_pipeline_models(self):
        """Check it creates all the pipeline models."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'environment': {
                'comment': 'hostname.redhat.com'
            },
            'path': 'boot',
            'comment': 'Boot test',
            'waived': False,
            'start_time': '2020-06-03T15:52:25Z',
            'duration': 158,
            'status': 'PASS',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
            ],
            'misc': {
                'debug': False,
                'targeted': True,
                'job': {
                    'id': 887330,
                    'name': 'test aarch64',
                    'stage': 'test',
                    'started_at': '2020-06-03T15:41:43.940Z',
                    'created_at': '2020-06-03T15:08:06.237Z',
                    'finished_at': '2020-06-03T18:02:53.430Z',
                    'duration': 8469.490879,
                    'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                    'tag': '-209.el8',
                    'commit_message_title': '[redhat] kernel-4.18.0-209.el8',
                    'kernel_version': '4.18.0-209.el8.cki'},
                'pipeline': {
                    'id': 592705,
                    'variables': {
                        'cki_pipeline_type': 'patchwork',
                    },
                    'started_at': '2020-06-03T15:08:09.957Z',
                    'created_at': '2020-06-03T15:08:05.288Z',
                    'finished_at': '2020-06-04T00:50:23.262Z',
                    'duration': 34916,
                    'ref': 'rhel8',
                    'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                    'project': {
                        'id': 2,
                        'path_with_namespace': 'cki-project/cki-pipeline',
                        'instance_url': 'https://gitlab.com'
                    }
                }
            }
        }
        test = models.KCIDBTest.create_from_json(data)
        gitlab_job = test.gitlabjob_set.first()

        self.assertIsNotNone(gitlab_job)
        self.assertIsNotNone(gitlab_job.pipeline)
        self.assertEqual(887330, gitlab_job.job_id)
        self.assertEqual(592705, gitlab_job.pipeline.pipeline_id)

    def test_create_pipeline_models_beaker(self):
        """Check it creates all the pipeline models."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'environment': {
                'comment': 'hostname.redhat.com'
            },
            'path': 'boot',
            'comment': 'Boot test',
            'waived': False,
            'start_time': '2020-06-03T15:52:25Z',
            'duration': 158,
            'status': 'PASS',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
            ],
            'misc': {
                'debug': False,
                'targeted': True,
                'beaker': {
                    'task_id': 111218982,
                    'recipe_id': 8378739,
                    'finish_time': '2020-06-03T16:46:05+00:00',
                    'retcode': 0,
                },
                'job': {
                    'id': 887330,
                    'name': 'test aarch64',
                    'stage': 'test',
                    'started_at': '2020-06-03T15:41:43.940Z',
                    'created_at': '2020-06-03T15:08:06.237Z',
                    'finished_at': '2020-06-03T18:02:53.430Z',
                    'duration': 8469.490879,
                    'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                    'tag': '-209.el8',
                    'commit_message_title': '[redhat] kernel-4.18.0-209.el8',
                    'kernel_version': '4.18.0-209.el8.cki'},
                'pipeline': {
                    'id': 592705,
                    'variables': {
                        'cki_pipeline_type': 'patchwork',
                    },
                    'started_at': '2020-06-03T15:08:09.957Z',
                    'created_at': '2020-06-03T15:08:05.288Z',
                    'finished_at': '2020-06-04T00:50:23.262Z',
                    'duration': 34916,
                    'ref': 'rhel8',
                    'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                    'project': {
                        'id': 2,
                        'path_with_namespace': 'cki-project/cki-pipeline',
                        'instance_url': 'https://gitlab.com'
                    }
                }
            }
        }
        test = models.KCIDBTest.create_from_json(data)
        gitlab_job = test.gitlabjob_set.first()

        self.assertIsNotNone(gitlab_job)
        self.assertIsNotNone(gitlab_job.pipeline)
        self.assertEqual(887330, gitlab_job.job_id)
        self.assertEqual(592705, gitlab_job.pipeline.pipeline_id)

        beaker_task = test.beakertask_set.first()
        self.assertIsNotNone(beaker_task)

        self.assertEqual(111218982, beaker_task.task_id)
        self.assertEqual(8378739, beaker_task.recipe_id)

    def test_re_submit_files(self):
        """Submit files multiple times."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        models.KCIDBTest.create_from_json(data)
        self.assertEqual(
            2, models.Artifact.objects.count()
        )

        models.KCIDBTest.create_from_json(data)
        self.assertEqual(
            2, models.Artifact.objects.count()
        )


class TestKCIDBNotifications(utils.TestCase):
    """Test kcidb notifications are sent."""

    @staticmethod
    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout(signal):
        """Test send_kcidb_notification."""
        data = {
            'id': 'https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            'origin': 'redhat',
        }

        # Create call
        models.KCIDBCheckout.create_from_json(data)
        # Update call
        rev = models.KCIDBCheckout.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.create_from_json',
                status='new',
                object_type='checkout',
                objects=[rev]
            ),
            mock.call(
                sender='kcidb.checkout.create_from_json',
                status='updated',
                object_type='checkout',
                objects=[rev]
            ),
        ])

    @staticmethod
    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build(signal):
        """Test send_kcidb_notification."""
        rev = models.KCIDBCheckout.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
        }

        # Create call
        models.KCIDBBuild.create_from_json(data)
        # Update call
        build = models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.create_from_json',
                status='new',
                object_type='build',
                objects=[build]
            ),
            mock.call(
                sender='kcidb.build.create_from_json',
                status='updated',
                object_type='build',
                objects=[build]
            )
        ])

    @staticmethod
    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test(signal):
        """Test send_kcidb_notification."""
        rev = models.KCIDBCheckout.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }

        # Create call
        models.KCIDBTest.create_from_json(data)
        # Update call
        test = models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.create_from_json',
                status='new',
                object_type='test',
                objects=[test]
            ),
            mock.call(
                sender='kcidb.test.create_from_json',
                status='updated',
                object_type='test',
                objects=[test]
            )
        ])


class TestVisibility(utils.TestCase):
    """Test objects visibility."""

    @staticmethod
    def _create_pipeline():
        """Create Pipeline."""
        next_id = models.Pipeline.objects.count()
        return models.Pipeline.objects.create(
            pipeline_id=next_id,
            project=models.Project.objects.create(project_id=next_id, path='test-proj'),
            gittree=models.GitTree.objects.create(name=f'test-tree-{next_id}'),
        )

    @staticmethod
    def _create_gitlabjob(pipeline, checkout=None, build=None, test=None):
        """Create MergeRun."""
        next_id = models.GitlabJob.objects.count()
        gitlab_job = models.GitlabJob.objects.create(
            job_id=next_id,
            pipeline=pipeline,
            kcidb_checkout=checkout,
        )
        if build:
            gitlab_job.kcidb_build.add(build)
        if test:
            gitlab_job.kcidb_test.add(test)

        return gitlab_job

    def test_checkout(self):
        """Test checkout visibility."""
        checkout = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        # By default is private.
        self.assertFalse(checkout.is_public)

        checkout.kernel_type = models.KernelTypeEnum.INTERNAL
        checkout.save()
        self.assertFalse(checkout.is_public)

        checkout.kernel_type = models.KernelTypeEnum.UPSTREAM
        checkout.save()
        self.assertTrue(checkout.is_public)

    def test_build(self):
        """Test build visibility."""
        rev = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        build = models.KCIDBBuild.create_from_json({
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
        })

        # By default is private.
        self.assertFalse(build.is_public)

        build.checkout.kernel_type = models.KernelTypeEnum.INTERNAL
        build.checkout.save()
        self.assertFalse(build.is_public)

        build.checkout.kernel_type = models.KernelTypeEnum.UPSTREAM
        build.checkout.save()
        self.assertTrue(build.is_public)

    def test_test(self):
        """Test test visibility."""
        rev = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        test = models.KCIDBTest.create_from_json({
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        })

        # By default is private.
        self.assertFalse(test.is_public)

        test.build.checkout.kernel_type = models.KernelTypeEnum.INTERNAL
        test.build.checkout.save()
        self.assertFalse(test.is_public)

        test.build.checkout.kernel_type = models.KernelTypeEnum.UPSTREAM
        test.build.checkout.save()
        self.assertTrue(test.is_public)


class TestCheckoutAggregated(utils.TestCase):
    """Test aggregated data on a checkout."""

    def setUp(self):
        data = {
            'version': {'major': 4, 'minor': 0},
            'checkouts': [
                {'origin': 'redhat', 'id': 'decd6167bf4f6bec1284006d0522381b44660df3', 'valid': False},
            ],
            'builds': [
                {'origin': 'redhat', 'checkout_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1', 'valid': False},
                {'origin': 'redhat', 'checkout_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-2', 'valid': False},
                {'origin': 'redhat', 'checkout_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-3', 'valid': True},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1', 'status': 'FAIL'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-2', 'status': 'ERROR'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-3', 'status': 'PASS'},
            ]
        }
        self.assert_authenticated_post(
            201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def _test_rev(self, cases, only_aggregated=False):
        """Test attrs on both rev and rev_aggregated."""
        rev_id = 'decd6167bf4f6bec1284006d0522381b44660df3'
        rev = models.KCIDBCheckout.objects.get(id=rev_id)
        rev_aggregated = models.KCIDBCheckout.objects.aggregated().get(id=rev_id)

        for attr, value in cases:
            if isinstance(value, QuerySet):
                check = self.assertQuerySetEqual
            else:
                check = self.assertEqual

            check(getattr(rev_aggregated, attr), value, attr)
            if not only_aggregated:
                check(getattr(rev, attr), value, attr)

    def test_tests_errors(self):
        """Test stats_tests_errors aggregated property."""
        # Set all as Pass
        models.KCIDBTest.objects.update(status=models.ResultEnum.PASS)
        self._test_rev([('stats_tests_errors', False)], only_aggregated=True)
        # Set only one as Error
        models.KCIDBTest.objects.filter(id='redhat:test-1').update(status=models.ResultEnum.ERROR)
        self._test_rev([('stats_tests_errors', True)], only_aggregated=True)

    def test_nothing_triaged(self):
        """Test none objects were triaged."""
        rev = models.KCIDBCheckout.objects.get(id='decd6167bf4f6bec1284006d0522381b44660df3')

        # Without aggregated call these methods are not available.
        self.assertFalse(hasattr(rev, 'stats_checkout_triaged'))
        self.assertFalse(hasattr(rev, 'stats_checkout_untriaged'))
        self.assertFalse(hasattr(rev, 'stats_tests_triaged'))
        self.assertFalse(hasattr(rev, 'stats_tests_untriaged'))
        self.assertFalse(hasattr(rev, 'stats_builds_triaged'))
        self.assertFalse(hasattr(rev, 'stats_builds_untriaged'))
        self.assertIsNone(rev.has_objects_missing_triage)
        self.assertIsNone(rev.has_objects_with_issues)

        # No issues but failed jobs. All untriaged, no triaged.
        cases = [
            ('stats_checkout_untriaged', True),
            ('stats_builds_untriaged', True),
            ('stats_tests_untriaged', True),
            ('stats_checkout_triaged', False),
            ('stats_builds_triaged', False),
            ('stats_tests_triaged', False),
            # No issues, missing triage.
            ('has_objects_missing_triage', True),
            ('has_objects_with_issues', False),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_triaged', models.KCIDBBuild.objects.none()),
            ('builds_untriaged', models.KCIDBBuild.objects.filter(id__in=('redhat:build-1', 'redhat:build-2'))),
            ('tests_triaged', models.KCIDBTest.objects.none()),
            ('tests_untriaged', models.KCIDBTest.objects.filter(id__in=('redhat:test-1', 'redhat:test-2'))),
            # Checkout is not triaged.
            ('is_triaged', False),
            ('is_missing_triage', True),
        ]
        self._test_rev(cases)

    def test_partially_triaged(self):
        """Test some objects were triaged and some others not."""
        # Add some issues to some builds and tests.
        issue = models.Issue.objects.create(
            kind=models.IssueKind.objects.create(description="fail 1", tag="1"),
            description='foo bar',
            ticket_url='http://some.url',
        )
        models.KCIDBCheckout.objects.get(id='decd6167bf4f6bec1284006d0522381b44660df3').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-1').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-1').issues.add(issue)

        # We now have both triaged and untriaged jobs.
        cases = [
            ('stats_checkout_triaged', True),
            ('stats_checkout_untriaged', False),
            ('stats_builds_triaged', True),
            ('stats_builds_untriaged', True),
            ('stats_tests_triaged', True),
            ('stats_tests_untriaged', True),
            # Some issues, missing triage.
            ('has_objects_missing_triage', True),
            ('has_objects_with_issues', True),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_triaged', models.KCIDBBuild.objects.filter(id='redhat:build-1')),
            ('builds_untriaged', models.KCIDBBuild.objects.filter(id='redhat:build-2')),
            ('tests_triaged', models.KCIDBTest.objects.filter(id='redhat:test-1')),
            ('tests_untriaged', models.KCIDBTest.objects.filter(id='redhat:test-2')),
            # Checkout is triaged.
            ('is_triaged', True),
            ('is_missing_triage', False),
        ]
        self._test_rev(cases)

    def test_fully_triaged(self):
        """Test all objects were triaged."""
        # Add issue to all failures.
        issue = models.Issue.objects.create(
            kind=models.IssueKind.objects.create(description="fail 1", tag="1"),
            description='foo bar',
            ticket_url='http://some.url',
        )
        models.KCIDBCheckout.objects.get(id='decd6167bf4f6bec1284006d0522381b44660df3').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-1').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-2').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-1').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-2').issues.add(issue)

        # We now have all issues triaged.
        cases = [
            ('stats_builds_triaged', True),
            ('stats_builds_untriaged', False),
            ('stats_tests_triaged', True),
            ('stats_tests_untriaged', False),
            # No failures missing triage.
            ('has_objects_missing_triage', False),
            ('has_objects_with_issues', True),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_untriaged', models.KCIDBBuild.objects.none()),
            ('builds_triaged', models.KCIDBBuild.objects.filter(id__in=('redhat:build-1', 'redhat:build-2'))),
            ('tests_untriaged', models.KCIDBTest.objects.none()),
            ('tests_triaged', models.KCIDBTest.objects.filter(id__in=('redhat:test-1', 'redhat:test-2')))
        ]
        self._test_rev(cases)


class TestKCIDBProperties(utils.TestCase):
    """Test objects properties."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_checkout_web_url(self):
        """Test KCIDBCheckout's web_url property."""
        checkout = models.KCIDBCheckout.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/checkouts/{checkout.iid}', checkout.web_url)

    def test_build_web_url(self):
        """Test KCIDBBuild's web_url property."""
        build = models.KCIDBBuild.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/builds/{build.iid}', build.web_url)

    def test_test_web_url(self):
        """Test KCIDBTest's web_url property."""
        test = models.KCIDBTest.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/tests/{test.iid}', test.web_url)


class TestBuildAggregated(utils.TestCase):
    """Test aggregated data on a build."""

    def setUp(self):
        data = {
            'version': {'major': 4, 'minor': 0},
            'checkouts': [
                {'origin': 'redhat', 'id': 'decd6167bf4f6bec1284006d0522381b44660df3', 'valid': False},
            ],
            'builds': [
                {'origin': 'redhat', 'checkout_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1', 'valid': False},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1', 'status': 'FAIL'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-2', 'status': 'ERROR'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-3', 'status': 'PASS'},
            ]
        }
        self.assert_authenticated_post(
            201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def _test_build(self, cases, only_aggregated=False):
        """Test attrs on both build and build_aggregated."""
        build_id = 'redhat:build-1'
        build = models.KCIDBBuild.objects.get(id=build_id)
        build_aggregated = models.KCIDBBuild.objects.aggregated().get(id=build_id)

        for attr, value in cases:
            if isinstance(value, QuerySet):
                check = self.assertQuerySetEqual
            else:
                check = self.assertEqual

            check(getattr(build_aggregated, attr), value, attr)
            if not only_aggregated:
                check(getattr(build, attr), value, attr)

    def test_tests_errors(self):
        """Test stats_tests_errors aggregated property."""
        # Set all as Pass
        models.KCIDBTest.objects.update(status=models.ResultEnum.PASS)
        self._test_build([('has_tests_errors', False)])
        # Set only one as Error
        models.KCIDBTest.objects.filter(id='redhat:test-1').update(status=models.ResultEnum.ERROR)
        self._test_build([('has_tests_errors', True)])


class TestCheckoutAnnotatedByArchitecture(utils.TestCase):
    """Test annotated_by_architecture data on a checkout."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_multiple_architectures.yaml',
    ]

    def test_counters(self):
        """Test annotated_by_architecture counters."""
        rev = (
            models.KCIDBCheckout.objects
            .annotated_by_architecture()
            .get(id='redhat:d1c47b385764aaa488bce182d944fa22bb1325d1')
        )

        test_cases = {
            'aarch64': {
                'builds_ran': 1,
                'builds_failed': 0,
                'builds_with_issues': 0,
                'tests_ran': 1,
                'tests_failed': 1,
                'tests_failed_waived': 0,
                'tests_with_issues': 0,
            },
            'ppc64': {
                'builds_ran': 1,
                'builds_failed': 0,
                'builds_with_issues': 0,
                'tests_ran': 1,
                'tests_failed': 0,
                'tests_failed_waived': 0,
                'tests_with_issues': 0,
            },
            'ppc64le': {
                'builds_ran': 1,
                'builds_failed': 1,
                'builds_with_issues': 1,
                'tests_ran': 3,
                'tests_failed': 1,
                'tests_failed_waived': 1,
                'tests_with_issues': 2,
            },
        }

        for arch, fields in test_cases.items():
            for key, value in fields.items():
                self.assertEqual(getattr(rev, f'stats_{arch}_{key}_count'), value, (arch, key))

        # All architectures not included in test_case should be zero.
        for arch in models.ArchitectureEnum:
            if arch.name in test_cases:
                continue

            for key in test_cases['aarch64']:
                self.assertEqual(getattr(rev, f'stats_{arch.name}_{key}_count'), 0, (arch.name, key))
