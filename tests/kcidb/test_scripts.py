"""Test the views module."""
import json
from unittest import mock

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from datawarehouse.api.kcidb import views
from tests import utils


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestSubmit(utils.TestCase):
    """Test submit endpoint."""

    def test_schema_version(self):
        """Test schema version is validated."""
        data = {'version': {'major': 1, 'minor': 0}}
        self.assert_authenticated_post(
            400, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

        data = {'version': {'major': 4, 'minor': 0}}
        self.assert_authenticated_post(
            201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def test_full_submit(self):
        """Test submitting all the data."""
        data = {
            'version': {'major': 4, 'minor': 0},
            'checkouts': [
                {'origin': 'redhat', 'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'},
            ],
            'builds': [
                {'origin': 'redhat', 'checkout_id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1'},
                {'origin': 'redhat', 'checkout_id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-2'},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1'},
                {'origin': 'redhat', 'build_id': 'redhat:build-2', 'id': 'redhat:test-2'},
            ]
        }

        self.assert_authenticated_post(
            201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

        for checkout in data['checkouts']:
            self.assertTrue(models.KCIDBCheckout.objects.filter(id=checkout['id']).exists())

        for build in data['builds']:
            self.assertTrue(models.KCIDBBuild.objects.filter(id=build['id']).exists())

        for test in data['tests']:
            self.assertTrue(models.KCIDBTest.objects.filter(id=test['id']).exists())

    def test_error_missing_parent(self):
        """Test submitting an object without it's parent."""
        data = {
            'version': {'major': 4, 'minor': 0},
            'builds': [
                {'origin': 'redhat', 'checkout_id': 'redhat:5678',
                 'id': 'redhat:1234'},
            ]
        }

        self.assertFalse(
            models.KCIDBCheckout.objects.filter(
                id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
            ).exists()
        )
        response = self.assert_authenticated_post(
            400, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )
        self.assertDictEqual(
            {
                'errors': [
                    [
                        'builds',
                        'redhat:1234',
                        'KCIDBCheckout id=redhat:5678 is not present in the DB'
                    ]
                ]
            },
            response.json()
        )

    def test_retrigger(self):
        """Test submitting retriggered pipeline."""
        data = {
            'version': {'major': 4, 'minor': 0},
            'checkouts': [
                {
                    'origin': 'redhat',
                    'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                    'misc': {'pipeline': {'variables': {'retrigger': 'true'}}}
                },
            ]
        }
        data = {
            'version': {'major': 4, 'minor': 0},
            'checkouts': [
                {
                    'id': 'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                    'origin': 'redhat',
                    'valid': True,
                    'misc': {
                        'job': {
                            'id': 887316,
                            'name': 'merge',
                            'stage': 'merge',
                            'started_at': '2020-06-03T15:11:19.327Z',
                            'created_at': '2020-06-03T15:08:05.512Z',
                            'finished_at': '2020-06-03T15:14:55.148Z',
                            'duration': 215.820987,
                            'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                            'tag': '-209.el8',
                            'commit_message_title': '[redhat] kernel',
                            'kernel_version': None
                        },
                        'pipeline': {
                            'id': 592705,
                            'variables': {
                                'cki_pipeline_type': 'patchwork',
                                'retrigger': 'true'
                            },
                            'started_at': '2020-06-03T15:08:09.957Z',
                            'created_at': '2020-06-03T15:08:05.288Z',
                            'finished_at': None,
                            'duration': None,
                            'ref': 'retrigger-67609c5a-d0c6-43ed-b445-2c5c985871b4',
                            'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                            'project': {
                                'id': 2,
                                'path_with_namespace': 'cki-project/cki-pipeline',
                                'instance_url': 'https://gitlab.com'
                            }
                        }
                    }
                }
            ]
        }

        # Submit the pipeline with FF_ALLOW_DEBUG_PIPELINES=False
        with mock.patch('datawarehouse.api.kcidb.views.settings.FF_ALLOW_DEBUG_PIPELINES', False):
            response = self.assert_authenticated_post(
                201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
                content_type="application/json"
            )

        self.assertDictEqual(
            {
                'warnings': [
                    [
                        'checkouts',
                        'https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                        'Retrigger pipeline ignored'
                    ]
                ]
            },
            response.json()
        )

        self.assertFalse(
            models.KCIDBCheckout.objects.filter(
                id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
            ).exists()
        )

        # Submit the pipeline with FF_ALLOW_DEBUG_PIPELINES=True
        with mock.patch('datawarehouse.api.kcidb.views.settings.FF_ALLOW_DEBUG_PIPELINES', True):
            self.assert_authenticated_post(
                201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
                content_type="application/json"
            )

        checkout = models.KCIDBCheckout.objects.get(
            id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
        )
        self.assertEqual(
            'retriggers',
            models.Pipeline.objects.get(gitlabjob__kcidb_checkout=checkout).gittree.name
        )

    def test_sorted(self):
        """Test sorting the data."""
        checkouts = [
            {'origin': 'redhat', 'id': 'redhat:1'},
        ]
        tests = [
            {'origin': 'redhat', 'build_id': 'redhat:1', 'id': 'redhat:2'},
            {'origin': 'redhat', 'build_id': 'redhat:1', 'id': 'redhat:1',
             'misc': {'rerun_index': 2}},
            {'origin': 'redhat', 'build_id': 'redhat:1', 'id': 'redhat:1',
             'misc': {'rerun_index': 1}},
        ]

        # Check it doesn't break with something else than tests
        self.assertEqual(
            views.Submit.sorted(checkouts),
            checkouts
        )

        self.assertEqual(
            [
                {'build_id': 'redhat:1', 'id': 'redhat:1', 'origin': 'redhat',
                 'misc': {'rerun_index': 1}},
                {'build_id': 'redhat:1', 'id': 'redhat:1', 'origin': 'redhat',
                 'misc': {'rerun_index': 2}},
                {'build_id': 'redhat:1', 'id': 'redhat:2', 'origin': 'redhat'}
            ],
            views.Submit.sorted(tests)
        )


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestEndpointsAnonymous(utils.KCIDBTestCase):
    """Test kcidb get/list endpoints."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
    ]

    anonymous = True
    groups = []

    def test_checkouts_list(self):
        """Test checkouts list endpoint."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )
        response = self.client.get('/api/1/kcidb/checkouts')
        self.assertEqual(
            serializers.KCIDBCheckoutSerializer(authorized_checkouts, many=True).data,
            response.json()['results']
        )

    def test_checkouts_get(self):
        """Test checkouts get endpoint. Both iid and id queries."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response_iid = self.client.get(f'/api/1/kcidb/checkouts/{checkout.iid}')
            response_id = self.client.get(f'/api/1/kcidb/checkouts/{checkout.id}')

            if checkout not in authorized_checkouts:
                self.assertEqual(404, response_iid.status_code)
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(
                response_id.json(),
                response_iid.json()
            )
            self.assertEqual(
                serializers.KCIDBCheckoutSerializer(checkout).data,
                response_id.json()
            )

    def test_builds_list(self):
        """Test builds list endpoint."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response = self.client.get(f'/api/1/kcidb/checkouts/{checkout.iid}/builds')

            builds = checkout.kcidbbuild_set.all() if checkout in authorized_checkouts else []
            self.assertEqual(
                serializers.KCIDBBuildSerializer(builds, many=True).data,
                response.json()['results']
            )

    def test_builds_get(self):
        """Test builds get endpoint. Both iid and id queries."""
        self._ensure_test_conditions('read')
        authorized_builds = models.KCIDBBuild.objects.filter(
            checkout__id__in=self.checkouts_authorized['read']
        )

        for build in models.KCIDBBuild.objects.all():
            response_iid = self.client.get(f'/api/1/kcidb/builds/{build.iid}')
            response_id = self.client.get(f'/api/1/kcidb/builds/{build.id}')

            if build not in authorized_builds:
                self.assertEqual(404, response_iid.status_code)
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(
                response_id.json(),
                response_iid.json()
            )
            self.assertEqual(
                serializers.KCIDBBuildSerializer(build).data,
                response_id.json()
            )

    def test_tests_list(self):
        """Test builds list endpoint."""
        self._ensure_test_conditions('read')
        authorized_builds = models.KCIDBBuild.objects.filter(
            checkout__id__in=self.checkouts_authorized['read']
        )

        for build in models.KCIDBBuild.objects.all():
            response = self.client.get(f'/api/1/kcidb/builds/{build.id}/tests')

            tests = build.kcidbtest_set.all() if build in authorized_builds else []
            self.assertEqual(
                serializers.KCIDBTestSerializer(tests, many=True).data,
                response.json()['results']
            )

    def test_tests_get(self):
        """Test tests get endpoint. Both iid and id queries."""
        self._ensure_test_conditions('read')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__checkout__id__in=self.checkouts_authorized['read']
        )

        for test in models.KCIDBTest.objects.all():
            response_iid = self.client.get(f'/api/1/kcidb/tests/{test.iid}')
            response_id = self.client.get(f'/api/1/kcidb/tests/{test.id}')

            if test not in authorized_tests:
                self.assertEqual(404, response_iid.status_code)
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(
                response_id.json(),
                response_iid.json()
            )
            self.assertEqual(
                serializers.KCIDBTestSerializer(test).data,
                response_id.json()
            )


class TestEndpointsNoGroups(TestEndpointsAnonymous):
    """TestEndpoints with no groups."""

    anonymous = False
    groups = []


class TestEndpointsReadGroup(TestEndpointsAnonymous):
    """TestEndpoints with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestEndpointsWriteGroup(TestEndpointsAnonymous):
    """TestEndpoints with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestEndpointsAllGroups(TestEndpointsAnonymous):
    """TestEndpoints with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']
