"""Test the views module."""
import datetime

from django.utils import timezone

from datawarehouse import models
from tests import utils


class TestKCIDBViewsAnonymous(utils.KCIDBTestCase):
    """Test KCIDB frontend views."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
    ]
    anonymous = True
    groups = []

    def test_checkout_view_with_id(self):
        """Test checkouts view querying with id."""
        self._ensure_test_conditions('read')

        for checkout in models.KCIDBCheckout.objects.all():
            response_iid = self.client.get(f'/kcidb/checkouts/{checkout.iid}')
            response_id = self.client.get(f'/kcidb/checkouts/{checkout.id}')

            self.assertEqual(response_iid.status_code, response_id.status_code)
            if response_id.status_code == 200:
                self.assertEqual(
                    response_iid.context['checkout'],
                    response_id.context['checkout']
                )

    def test_checkout_view(self):
        """Test checkouts view."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response = self.client.get(f'/kcidb/checkouts/{checkout.iid}')

            if checkout not in authorized_checkouts:
                self.assertEqual(404, response.status_code)
                continue

            all_issues_affected = set(
                list(models.Issue.objects.filter(kcidbcheckout=checkout).values_list('id', flat=True)) +
                list(models.Issue.objects.filter(kcidbbuild__checkout=checkout).values_list('id', flat=True)) +
                list(models.Issue.objects.filter(kcidbtest__build__checkout=checkout).values_list('id', flat=True))
            )
            self.assertContextEqual(
                response.context,
                {
                    'grouped_issues': [
                        {
                            'issue': issue,
                            'checkouts': models.IssueOccurrence.objects.filter(
                                kcidb_checkout=checkout, issue=issue),
                            'builds': models.IssueOccurrence.objects.filter(
                                kcidb_build__checkout=checkout, issue=issue),
                            'tests': models.IssueOccurrence.objects.filter(
                                kcidb_test__build__checkout=checkout, issue=issue),
                        } for issue in models.Issue.objects.filter(id__in=all_issues_affected)
                    ],
                    'issues': models.Issue.objects.filter(resolved_on=None).order_by('-id'),
                    'tests': models.KCIDBTest.objects.filter(build__checkout=checkout),
                    'tests_failed': (
                        models.KCIDBTest.objects.filter(build__checkout=checkout)
                        .exclude(status=models.ResultEnum.PASS)
                    ),
                    'builds': models.KCIDBBuild.objects.filter(checkout=checkout),
                    'builds_failed': models.KCIDBBuild.objects.filter(checkout=checkout).exclude(valid=True),
                    'checkout': checkout,
                    'checkouts_failed': [checkout] if not checkout.valid else [],
                },
            )

    def test_build_view_with_id(self):
        """Test build view querying with id."""
        self._ensure_test_conditions('read')

        for build in models.KCIDBBuild.objects.all():
            response_iid = self.client.get(f'/kcidb/builds/{build.iid}')
            response_id = self.client.get(f'/kcidb/builds/{build.id}')

            self.assertEqual(response_iid.status_code, response_id.status_code)
            if response_id.status_code == 200:
                self.assertEqual(
                    response_iid.context['build'],
                    response_id.context['build']
                )

    def test_build_view(self):
        """Test build view."""
        self._ensure_test_conditions('read')
        authorized_builds = models.KCIDBBuild.objects.filter(
            checkout__id__in=self.checkouts_authorized['read']
        )

        for build in models.KCIDBBuild.objects.all():
            response = self.client.get(f'/kcidb/builds/{build.iid}')

            if build not in authorized_builds:
                self.assertEqual(404, response.status_code)
                continue

            all_issues_affected = set(
                list(models.Issue.objects.filter(kcidbbuild=build).values_list('id', flat=True)) +
                list(models.Issue.objects.filter(kcidbtest__build=build).values_list('id', flat=True))
            )
            self.assertContextEqual(
                response.context,
                {
                    'grouped_issues': [
                        {
                            'issue': issue,
                            'checkouts': [],
                            'builds': models.IssueOccurrence.objects.filter(kcidb_build=build, issue=issue),
                            'tests': models.IssueOccurrence.objects.filter(kcidb_test__build=build, issue=issue),
                        } for issue in models.Issue.objects.filter(id__in=all_issues_affected)
                    ],
                    'issues': models.Issue.objects.filter(resolved_on=None).order_by('-id'),
                    'tests': models.KCIDBTest.objects.filter(build=build),
                    'tests_failed': (
                        models.KCIDBTest.objects.filter(build=build).exclude(status=models.ResultEnum.PASS)
                    ),
                    'build': build,
                    'builds_failed': [build] if not build.valid else [],
                },
            )

    def test_test_view_with_id(self):
        """Test test view querying with id."""
        self._ensure_test_conditions('read')

        for test in models.KCIDBTest.objects.all():
            response_iid = self.client.get(f'/kcidb/tests/{test.iid}')
            response_id = self.client.get(f'/kcidb/tests/{test.id}')

            self.assertEqual(response_iid.status_code, response_id.status_code)
            if response_id.status_code == 200:
                self.assertEqual(
                    response_iid.context['test'],
                    response_id.context['test']
                )

    def test_test_view(self):
        """Test Test view."""
        self._ensure_test_conditions('read')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__checkout__id__in=self.checkouts_authorized['read']
        )

        for test in models.KCIDBTest.objects.all():
            response = self.client.get(f'/kcidb/tests/{test.iid}')

            if test not in authorized_tests:
                self.assertEqual(404, response.status_code)
                continue

            all_issues_affected = set(
                list(models.Issue.objects.filter(kcidbtest=test).values_list('id', flat=True))
            )
            self.assertContextEqual(
                response.context,
                {
                    'grouped_issues': [
                        {
                            'issue': issue,
                            'checkouts': [],
                            'builds': [],
                            'tests': models.IssueOccurrence.objects.filter(kcidb_test=test, issue=issue),
                        } for issue in models.Issue.objects.filter(id__in=all_issues_affected)
                    ],
                    'issues': models.Issue.objects.filter(resolved_on=None).order_by('-id'),
                    'test': test,
                    'tests_failed': [test] if not test.status or test.status.name != 'PASS' else [],
                },
            )

    def test_create_issues_checkout(self):
        """Test linking an issue to a checkout."""
        self._ensure_test_conditions('write')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['write']
        )

        issue = models.Issue.objects.first()

        for checkout in models.KCIDBCheckout.objects.all():
            checkout.issues.clear()

            authorized = checkout in authorized_checkouts and not self.anonymous
            response_code = 302 if authorized else 404

            self.assert_authenticated_post(
                response_code,
                'change_kcidbcheckout',
                '/kcidb/issues/occurrences',
                {
                    'issue_id': issue.id,
                    'checkout_iids': [
                        checkout.iid
                    ],
                },
                user=self.user
            )

            if not authorized:
                continue

            self.assertEqual(
                issue,
                checkout.issues.get()
            )

    def test_create_issues_build(self):
        """Test linking an issue to builds."""
        self._ensure_test_conditions('write')
        authorized_builds = models.KCIDBBuild.objects.filter(
            checkout__id__in=self.checkouts_authorized['write']
        )

        issue = models.Issue.objects.first()

        for build in models.KCIDBBuild.objects.all():
            build.issues.clear()

            authorized = build in authorized_builds and not self.anonymous
            response_code = 302 if authorized else 404

            self.assert_authenticated_post(
                response_code,
                'change_kcidbbuild',
                '/kcidb/issues/occurrences',
                {
                    'issue_id': issue.id,
                    'build_iids': [
                        build.iid
                    ],
                },
                user=self.user
            )

            if not authorized:
                continue

            self.assertEqual(
                issue,
                build.issues.get()
            )

    def test_create_issues_test(self):
        """Test linking an issue to some tests."""
        self._ensure_test_conditions('write')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__checkout__id__in=self.checkouts_authorized['write']
        )

        issue = models.Issue.objects.first()

        for test in models.KCIDBTest.objects.all():
            test.issues.clear()

            authorized = test in authorized_tests and not self.anonymous
            response_code = 302 if authorized else 404

            self.assert_authenticated_post(
                response_code,
                'change_kcidbtest',
                '/kcidb/issues/occurrences',
                {
                    'issue_id': issue.id,
                    'test_iids': [
                        test.iid
                    ],
                },
                user=self.user
            )

            if not authorized:
                continue

            self.assertEqual(
                issue,
                test.issues.get()
            )

    def test_create_issues_all(self):
        """
        Test linking an issue to checkouts, builds and tests.

        At least doesn't have authorization to one of the checkouts, so it should fail.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        checkouts = models.KCIDBCheckout.objects.all()
        builds = models.KCIDBBuild.objects.all()
        tests = models.KCIDBTest.objects.all()

        self.assert_authenticated_post(
            404,
            ['change_kcidbcheckout', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'issue_id': issue.id,
                'checkout_iids': list(checkouts.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

    def test_create_issues_all_allowed(self):
        """
        Test linking an issue to checkouts, builds and tests.

        All the submitted objects are allowed to this user.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['write'])
        builds = models.KCIDBBuild.objects.filter(checkout__id__in=self.checkouts_authorized['write'])
        tests = models.KCIDBTest.objects.filter(build__checkout__id__in=self.checkouts_authorized['write'])

        [checkout.issues.clear() for checkout in checkouts]
        [build.issues.clear() for build in builds]
        [test.issues.clear() for test in tests]

        self.assert_authenticated_post(
            302,
            ['change_kcidbcheckout', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'issue_id': issue.id,
                'checkout_iids': list(checkouts.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

        if self.anonymous:
            return

        for obj in checkouts:
            self.assertEqual(issue, obj.issues.get())
        for obj in builds:
            self.assertEqual(issue, obj.issues.get())
        for obj in tests:
            self.assertEqual(issue, obj.issues.get())

    def test_delete_issues_all(self):
        """
        Test deleting an issue from checkouts, builds and tests.

        At least doesn't have authorization to one of the checkouts, so it should fail.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        checkouts = models.KCIDBCheckout.objects.all()
        builds = models.KCIDBBuild.objects.all()
        tests = models.KCIDBTest.objects.all()

        self.assert_authenticated_post(
            404,
            ['change_kcidbcheckout', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'action': 'remove',
                'issue_id': issue.id,
                'checkout_iids': list(checkouts.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

    def test_delete_issues_all_allowed(self):
        """
        Test deleting an issue from checkouts, builds and tests.

        All the submitted objects are allowed to this user.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['write'])
        builds = models.KCIDBBuild.objects.filter(checkout__id__in=self.checkouts_authorized['write'])
        tests = models.KCIDBTest.objects.filter(build__checkout__id__in=self.checkouts_authorized['write'])

        [checkout.issues.add(issue) for checkout in checkouts]
        [build.issues.add(issue) for build in builds]
        [test.issues.add(issue) for test in tests]

        self.assert_authenticated_post(
            302,
            ['change_kcidbcheckout', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'action': 'remove',
                'issue_id': issue.id,
                'checkout_iids': list(checkouts.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

        if self.anonymous:
            return

        for obj in checkouts:
            self.assertFalse(obj.issues.filter(id=issue.id).exists())
        for obj in builds:
            self.assertFalse(obj.issues.filter(id=issue.id).exists())
        for obj in tests:
            self.assertFalse(obj.issues.filter(id=issue.id).exists())


class TestKCIDBViewsNoGroup(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with a group with no groups assigned."""

    anonymous = False
    groups = []


class TestKCIDBViewsReadGroups(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestKCIDBViewsWriteGroups(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestKCIDBViewsAllGroups(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestCheckoutsListAnonymous(utils.KCIDBTestCase):
    """Test the checkouts list view."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
    ]
    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized[method])
        no_auth_checkouts = models.KCIDBCheckout.objects.exclude(id__in=self.checkouts_authorized[method])

        checks = [
            (auth_checkouts, 'No authorized checkouts'),
            (no_auth_checkouts, 'No unauthorized checkouts'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def setUp(self):
        """Set up data."""
        origin = models.KCIDBOrigin.objects.get(name='redhat')
        public_policy = models.Policy.objects.get(name='public')
        restricted_policy = models.Policy.objects.get(name='restricted')

        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'public_{index}',
                    policy=public_policy,
                )
                for index in range(40)
            ]
        )
        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'restricted_{index}',
                    policy=restricted_policy,
                )
                for index in range(40)
            ]
        )
        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'unavailable_{index}',
                    policy=None,
                )
                for index in range(40)
            ]
        )

        super().setUp()

    def test_checkout_list(self):
        """Test checkouts list."""
        checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['read'])
        response = self.client.get('/kcidb/checkouts')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': checkouts[:30],
            },
        )

    def test_checkout_list_page_2(self):
        """Test checkouts list pagination."""
        checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['read'])
        response = self.client.get('/kcidb/checkouts?page=2')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': checkouts[30:60],
            },
        )

    def test_checkout_list_page_invalid(self):
        """Test checkouts list pagination with wrong page numbers."""
        checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['read'])
        for page in ('...', 'foobar', '3%3BSELECT%20sleep%2829%29%3B%20--', '0', '-1', '-1000'):
            response = self.client.get(f'/kcidb/checkouts?page={page}')
            self.assertContextEqual(
                response.context,
                {
                    'checkouts': checkouts[:30],
                },
            )

    def test_checkout_list_aggregated(self):
        """Test checkouts list returns aggregated results."""
        response = self.client.get('/kcidb/checkouts')
        self.assertTrue(
            hasattr(
                response.context['checkouts'][0],
                'stats_tests_fail_count'
            )
        )

    def test_failures_filtered(self):
        """Test checkouts list filtered."""
        models.KCIDBCheckout.objects.all().delete()

        origin = models.KCIDBOrigin.objects.first()

        policy_permissive = models.Policy.objects.create(
            name='permissive', write_group=None, read_group=None
        )
        tree_1 = models.GitTree.objects.create(name='tree_1')
        tree_2 = models.GitTree.objects.create(name='tree_2')

        rev_1 = models.KCIDBCheckout.objects.create(id='1', origin=origin, tree=tree_1, policy=policy_permissive)
        rev_2 = models.KCIDBCheckout.objects.create(id='2', origin=origin, tree=tree_2, policy=policy_permissive)

        # Filter tree_1
        response = self.client.get('/kcidb/checkouts?filter_gittrees=tree_1')
        self.assertContextEqual(response.context, {'checkouts': [rev_1]})

        # Filter tree_2
        response = self.client.get('/kcidb/checkouts?filter_gittrees=tree_2')
        self.assertContextEqual(response.context, {'checkouts': [rev_2]})


class TestCheckoutsListNoGroups(TestCheckoutsListAnonymous):
    """TestCheckoutList with no groups."""

    anonymous = False
    groups = []


class TestCheckoutsListReadGroups(TestCheckoutsListAnonymous):
    """TestCheckoutList with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestCheckoutsListWriteGroups(TestCheckoutsListAnonymous):
    """TestCheckoutList with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestCheckoutsListAllGroups(TestCheckoutsListAnonymous):
    """TestCheckoutList with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestCheckoutsFailuresAnonymous(utils.KCIDBTestCase):
    """Test the checkouts failures list view."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_failed_checkouts.yaml'
    ]
    anonymous = True
    groups = []

    def test_failures_all(self):
        """Test failures list. All."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/all')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': (
                    models.KCIDBCheckout.objects
                    .filter(id__in=self.checkouts_authorized['read'])
                    .exclude(iid__in=(5, 10, 15))  # These ones do not have failures
                )
            },
        )

    def test_failures_checkouts(self):
        """Test failures list. Checkouts."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/checkout')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': (
                    models.KCIDBCheckout.objects
                    .filter(id__in=self.checkouts_authorized['read'])
                    .filter(valid=False)
                )
            },
        )

    def test_failures_builds(self):
        """Test failures list. Builds."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/build')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': (
                    models.KCIDBCheckout.objects
                    .filter(id__in=self.checkouts_authorized['read'])
                    .filter(kcidbbuild__valid=False)
                )
            },
        )

    def test_failures_tests(self):
        """Test failures list. Tests."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/test')
        print(models.KCIDBTest.objects.filter(build__checkout__id='restricted_checkout_4'))
        self.assertContextEqual(
            response.context,
            {
                'checkouts': (
                    models.KCIDBCheckout.objects
                    .filter(id__in=self.checkouts_authorized['read'])
                    .filter(kcidbbuild__kcidbtest__status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES)
                )
            },
        )

    def test_pagination(self):
        """Test list pagination."""
        self._ensure_test_conditions('read')
        # Remove previous objects
        models.KCIDBCheckout.objects.all().delete()

        origin = models.KCIDBOrigin.objects.first()
        public_policy = models.Policy.objects.get(name='public')
        models.KCIDBCheckout.objects.bulk_create([
            models.KCIDBCheckout(
                id=f'public_checkout_{index}',
                origin=origin,
                valid=False,
                policy=public_policy,
            ) for index in range(40)
        ])

        response = self.client.get('/kcidb/failures/checkout?page=2')

        self.assertContextEqual(
            response.context,
            {
                'checkouts': models.KCIDBCheckout.objects.all()[30:],
            },
        )

    def test_list_aggregated(self):
        """Test checkouts list returns aggregated results."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/all')
        self.assertTrue(
            hasattr(
                response.context['checkouts'][0],
                'stats_tests_fail_count'
            )
        )

    def test_unknown(self):
        """Test checkouts list returns aggregated results."""
        response = self.client.get('/kcidb/failures/foobar')
        self.assertEqual(400, response.status_code)
        self.assertEqual(b'Not sure what foobar is.', response.content)

    def test_failures_filtered(self):
        """Test failures list. Filtered."""
        checkout = models.KCIDBCheckout.objects.last()
        checkout.tree = models.GitTree.objects.create(name='tree_2')
        checkout.policy = models.Policy.objects.create(
            name='permissive', write_group=None, read_group=None
        )
        checkout.save()

        response = self.client.get('/kcidb/failures/all?filter_gittrees=tree_2')
        self.assertTrue(models.KCIDBCheckout.objects.filter(tree__name='tree_2').exists())
        self.assertContextEqual(
            response.context,
            {
                'checkouts': (
                    models.KCIDBCheckout.objects.filter(tree__name='tree_2')
                    .exclude(id='restricted_checkout_5')  # Successful
                )
            },
        )


class TestCheckoutsFailuresNoGroups(TestCheckoutsFailuresAnonymous):
    """TestCheckoutsFailures with no groups."""

    anonymous = False
    groups = []


class TestCheckoutsFailuresReadGroup(TestCheckoutsFailuresAnonymous):
    """TestCheckoutsFailures with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestCheckoutsFailuresWriteGroup(TestCheckoutsFailuresAnonymous):
    """TestCheckoutsFailures with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestCheckoutsFailuresAllGroups(TestCheckoutsFailuresAnonymous):
    """TestCheckoutsFailures with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestCheckoutsSearchAnonymous(utils.KCIDBTestCase):
    """Test the checkouts search view."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/pipeline_kcidb.yaml',
    ]

    anonymous = False
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized[method])
        no_auth_checkouts = models.KCIDBCheckout.objects.exclude(id__in=self.checkouts_authorized[method])
        auth_pipelines = models.Pipeline.objects.filter(
            gitlabjob__kcidb_checkout__id__in=self.checkouts_authorized[method]
        )
        no_auth_pipelines = models.Pipeline.objects.exclude(
            gitlabjob__kcidb_checkout__id__in=self.checkouts_authorized[method]
        )

        checks = [
            (auth_checkouts, 'No authorized checkouts'),
            (no_auth_checkouts, 'No unauthorized checkouts'),
            (auth_pipelines, 'No authorized pipelines'),
            (no_auth_pipelines, 'No unauthorized pipelines'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_search_empty(self):
        """Test search view with empty query."""
        self._ensure_test_conditions('read')
        response = self.client.get('/search')
        self.assertContextEqual(response.context, {})

        response = self.client.get('/search?q=')
        self.assertContextEqual(response.context, {})

    def test_search_not_found(self):
        """Test search view with empty query."""
        self._ensure_test_conditions('read')
        response = self.client.get('/search?q=foobar')
        self.assertContextEqual(response.context, {})

    def test_search_by_id(self):
        """Test search view with id."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            # add some spaces to test the stripping
            response = self.client.get(f'/search?q=+{checkout.id}+')
            self.assertEqual(200, response.status_code)

            if checkout not in authorized_checkouts:
                expected = {'checkouts': []}
            else:
                expected = {'checkouts': [checkout]}

            self.assertContextEqual(response.context, expected)

    def test_search_by_iid(self):
        """Test search view with iid."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response = self.client.get(f'/search?q={checkout.iid}')
            self.assertEqual(200, response.status_code)

            if checkout not in authorized_checkouts:
                expected = {'checkouts': []}
            else:
                expected = {'checkouts': [checkout]}

            self.assertContextEqual(response.context, expected)

    def test_search_by_pipeline_id(self):
        """Test search view with pipeline_id."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response = self.client.get(f'/search?q={checkout.gitlabjob_set.first().pipeline.pipeline_id}')
            self.assertEqual(200, response.status_code)

            if checkout not in authorized_checkouts:
                expected = {'checkouts': []}
            else:
                expected = {'checkouts': [checkout]}

            self.assertContextEqual(response.context, expected)

    def test_search_by_job_id(self):
        """Test search view with job id."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response = self.client.get(f'/search?q={checkout.gitlabjob_set.first().job_id}')
            self.assertEqual(200, response.status_code)

            if checkout not in authorized_checkouts:
                expected = {'checkouts': []}
            else:
                expected = {'checkouts': [checkout]}

            self.assertContextEqual(response.context, expected)


class TestCheckoutsSearchReadGroup(TestCheckoutsSearchAnonymous):
    """Test the checkouts search view. Read group."""

    anonymous = False
    groups = ['group_a']


class TestCheckoutsSearchWriteGroup(TestCheckoutsSearchAnonymous):
    """Test the checkouts search view. Write group."""

    anonymous = False
    groups = ['group_b']


class TestCheckoutsSearchAllGroups(TestCheckoutsSearchAnonymous):
    """Test the checkouts search view. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestBuildsListAnonymous(utils.KCIDBTestCase):
    """Test the builds list view."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_multiple_policies.yaml',
    ]
    anonymous = True
    groups = []

    def test_builds_list(self):
        """Test builds list."""
        builds = models.KCIDBBuild.objects.filter(checkout__id__in=self.checkouts_authorized['read'])
        response = self.client.get('/kcidb/builds')
        self.assertContextEqual(
            response.context,
            {
                'builds': builds,
            },
        )

    def test_builds_list_aggregated(self):
        """Test builds list returns aggregated results."""
        response = self.client.get('/kcidb/builds')
        self.assertTrue(
            hasattr(
                response.context['builds'][0],
                'stats_tests_fail_count'
            )
        )

    def test_builds_list_filter(self):
        """Test builds list filtered."""
        builds = models.KCIDBBuild.objects.filter(checkout__id__in=self.checkouts_authorized['read'])
        response = self.client.get('/kcidb/builds?filter_architectures=2')
        self.assertContextEqual(response.context, {'builds': builds.filter(architecture=2)})

        response = self.client.get('/kcidb/builds?filter_architectures=3')
        self.assertContextEqual(response.context, {'builds': builds.filter(architecture=3)})

        response = self.client.get('/kcidb/builds?filter_architectures=2&filter_architectures=3')
        self.assertContextEqual(response.context, {'builds': builds.filter(architecture__in=[2, 3])})

    def test_builds_list_checkout_filter(self):
        """Test builds list filtered by checkout params."""
        builds = models.KCIDBBuild.objects.filter(checkout__id__in=self.checkouts_authorized['read'])
        response = self.client.get('/kcidb/builds?filter_gittrees=tree_1')
        self.assertContextEqual(response.context, {'builds': builds.filter(checkout__tree__name='tree_1')})

        response = self.client.get('/kcidb/builds?filter_gittrees=tree_2')
        self.assertContextEqual(response.context, {'builds': builds.filter(checkout__tree__name='tree_2')})

        response = self.client.get('/kcidb/builds?filter_gittrees=tree_1&filter_gittrees=tree_2')
        self.assertContextEqual(
            response.context, {'builds': builds.filter(checkout__tree__name__in=['tree_1', 'tree_2'])}
        )


class TestBuildsListNoGroups(TestBuildsListAnonymous):
    """TestBuildList with no groups."""

    anonymous = False
    groups = []


class TestBuildsListReadGroups(TestBuildsListAnonymous):
    """TestBuildList with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestBuildsListWriteGroups(TestBuildsListAnonymous):
    """TestBuildList with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestBuildsListAllGroups(TestBuildsListAnonymous):
    """TestBuildList with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestBaselinesListAnonymous(utils.KCIDBTestCase):
    """Test the checkouts list view."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/basic.yaml',
    ]
    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized[method],
        ).exclude(
            gitlabjob__pipeline__variables__key='mr_url'
        )
        no_auth_checkouts = models.KCIDBCheckout.objects.exclude(
            id__in=self.checkouts_authorized[method],
            gitlabjob__pipeline__variables__key='mr_url'
        )

        checks = [
            (auth_checkouts, 'No authorized checkouts'),
            (no_auth_checkouts, 'No unauthorized checkouts'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def setUp(self):
        """Set up data."""
        origin = models.KCIDBOrigin.objects.get(name='redhat')
        public_policy = models.Policy.objects.get(name='public')
        restricted_policy = models.Policy.objects.get(name='restricted')

        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'public_{index}',
                    policy=public_policy,
                    git_repository_url='tree_1',
                    start_time=timezone.now(),
                )
                for index in range(10)
            ]
        )
        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'restricted_{index}',
                    policy=restricted_policy,
                    git_repository_url='tree_2',
                    start_time=timezone.now(),
                )
                for index in range(10)
            ]
        )
        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'unavailable_{index}',
                    policy=None,
                    git_repository_url='tree_3',
                    start_time=timezone.now(),
                )
                for index in range(10)
            ]
        )

        super().setUp()

    def test_baselines_detection(self):
        """
        Ensure we're only showing baselines.

        Add a trigger_variable with key=mr_url to all checkouts and make sure they
        are not returned on the results.
        """
        self._ensure_test_conditions('read')

        pipeline = models.Pipeline.objects.first()
        models.TriggerVariable.objects.create(
            key='mr_url', value='foo', pipeline=pipeline
        )

        # Link the checkouts to the trigger variable.
        for checkout in models.KCIDBCheckout.objects.all():
            models.GitlabJob.objects.create(
                job_id=checkout.iid,
                pipeline=pipeline,
                kcidb_checkout=checkout,
            )

        response = self.client.get('/kcidb/baselines')
        self.assertContextEqual(response.context, {'checkouts': []})

    def test_baselines_list(self):
        """Test list baselines."""
        self._ensure_test_conditions('read')
        checkouts = (
            models.KCIDBCheckout.objects
            .filter(id__in=self.checkouts_authorized['read'])
            .order_by('git_repository_url', 'git_repository_branch', '-iid')
            .distinct('git_repository_url', 'git_repository_branch')
        )
        response = self.client.get('/kcidb/baselines')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': checkouts,
                'architectures': models.ArchitectureEnum,
            },
        )

    def test_baselines_annotation(self):
        """Test checkouts are correctly annotated."""
        response = self.client.get('/kcidb/baselines')
        checkout = response.context['checkouts'][0]
        for checkout in response.context['checkouts']:
            for arch in models.ArchitectureEnum:
                self.assertEqual(
                    checkout.by_architecture[arch.name]['builds']['ran'],
                    getattr(checkout, f'stats_{arch.name}_builds_ran_count'))
                self.assertEqual(
                    checkout.by_architecture[arch.name]['builds']['failed'],
                    getattr(checkout, f'stats_{arch.name}_builds_failed_count'))
                self.assertEqual(
                    checkout.by_architecture[arch.name]['tests']['ran'],
                    getattr(checkout, f'stats_{arch.name}_tests_ran_count'))
                self.assertEqual(
                    checkout.by_architecture[arch.name]['tests']['failed'],
                    getattr(checkout, f'stats_{arch.name}_tests_failed_count'))
                self.assertEqual(
                    checkout.by_architecture[arch.name]['tests']['failed_waived'],
                    getattr(checkout, f'stats_{arch.name}_tests_failed_waived_count'))
                self.assertEqual(
                    checkout.by_architecture[arch.name]['known_issues'],
                    (
                        getattr(checkout, f'stats_{arch.name}_builds_with_issues_count') +
                        getattr(checkout, f'stats_{arch.name}_tests_with_issues_count')
                    )
                )

    def test_baselines_time_filter(self):
        """Test list baselines only shows checkouts newer than 60 days."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/baselines')
        self.assertTrue(len(response.context['checkouts']))

        # Less than 60 days, should be included
        models.KCIDBCheckout.objects.update(start_time=timezone.now() - datetime.timedelta(days=59))
        response = self.client.get('/kcidb/baselines')
        self.assertTrue(len(response.context['checkouts']))

        # More than 60 days, should not be included
        models.KCIDBCheckout.objects.update(start_time=timezone.now() - datetime.timedelta(days=61))
        response = self.client.get('/kcidb/baselines')
        self.assertFalse(len(response.context['checkouts']))


class TestBaselinesListReadGroups(TestBaselinesListAnonymous):
    """TestBuildList with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestBaselinesListWriteGroups(TestBaselinesListAnonymous):
    """TestBuildList with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestBaselinesListAllGroups(TestBaselinesListAnonymous):
    """TestBuildList with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']
