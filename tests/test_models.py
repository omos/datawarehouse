"""Test the models module."""
import datetime
from unittest import mock

import dateutil
from django.test.utils import override_settings
from freezegun import freeze_time

from datawarehouse import models
from tests import utils


class ModelsTestCase(utils.TestCase):
    """Unit tests for the models module."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/fixtures/patches.yaml',
    ]

    def test_patch_gui_url(self):
        """Test that the GUI URLs are correctly provided by the model."""
        self.assertEqual(models.Patch.objects.first().gui_url, 'url 1')


class TestIssue(utils.TestCase):
    """Test issue model."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/issues.yaml',
    ]

    def setUp(self):
        """Set up data."""
        self.checkout = models.KCIDBCheckout.objects.get(iid=1)
        self.build_1 = models.KCIDBBuild.objects.get(iid=1)
        self.build_2 = models.KCIDBBuild.objects.get(iid=2)
        self.test_1 = models.KCIDBTest.objects.get(iid=1)
        self.test_2 = models.KCIDBTest.objects.get(iid=2)

        self.issue = models.Issue.objects.get(id=1)

    def test_checkouts_empty(self):
        """Test checkouts property."""
        self.assertEqual(0, self.issue.get_checkouts(mock.MagicMock()).count())

    def test_checkouts_direct(self):
        """Test checkouts property."""
        self.checkout.issues.set([self.issue])
        self.assertEqual(
            'public_checkout',
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

    def test_checkouts_indirect_build(self):
        """Test checkouts property."""
        self.build_1.issues.set([self.issue])
        self.assertEqual(
            'public_checkout',
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

        # Another build shouldn't change anything, it's already related.
        self.build_2.issues.set([self.issue])
        self.assertEqual(
            'public_checkout',
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

    def test_checkouts_indirect_test(self):
        """Test checkouts property."""
        self.test_1.issues.set([self.issue])
        self.assertEqual(
            'public_checkout',
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

        # Another test shouldn't change anything, it's already related.
        self.test_2.issues.set([self.issue])
        self.assertEqual(
            'public_checkout',
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

    def test_checkouts_mixed(self):
        """Test checkouts property."""
        self.checkout.issues.set([self.issue])
        self.build_1.issues.set([self.issue])
        self.test_1.issues.set([self.issue])
        self.assertEqual(
            'public_checkout',
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

    def test_created_edited_timestamps(self):
        """Test created_on and last_edited_on timestamps are correctly set."""
        created_on = datetime.datetime(2010, 1, 2, 9, 0, tzinfo=dateutil.tz.UTC)
        edited_on = datetime.datetime(2010, 1, 2, 9, 1, tzinfo=dateutil.tz.UTC)

        with freeze_time(created_on):
            # Create new copy of an existing issue
            issue = models.Issue.objects.last()
            issue.id = None
            issue.ticket_url += '2'
            issue.save()

        self.assertEqual(created_on, issue.created_on)
        self.assertEqual(created_on, issue.last_edited_on)

        with freeze_time(edited_on):
            issue.save()

        self.assertEqual(created_on, issue.created_on)
        self.assertEqual(edited_on, issue.last_edited_on)


class TestIssueRegex(utils.TestCase):
    """Test IssueRegex model."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/fixtures/issue_regexes.yaml'
    ]

    def test_created_edited_timestamps(self):
        """Test created_on and last_edited_on timestamps are correctly set."""
        created_on = datetime.datetime(2010, 1, 2, 9, 0, tzinfo=dateutil.tz.UTC)
        edited_on = datetime.datetime(2010, 1, 2, 9, 1, tzinfo=dateutil.tz.UTC)

        with freeze_time(created_on):
            # Create new copy of an existing issue_regex
            issue_regex = models.IssueRegex.objects.last()
            issue_regex.id = None
            issue_regex.save()

        self.assertEqual(created_on, issue_regex.created_on)
        self.assertEqual(created_on, issue_regex.last_edited_on)

        with freeze_time(edited_on):
            issue_regex.save()

        self.assertEqual(created_on, issue_regex.created_on)
        self.assertEqual(edited_on, issue_regex.last_edited_on)


class TestTestMaintainer(utils.TestCase):
    """Test TestMaintainer model."""

    def test_create_from_dict(self):
        """Test create_from_address method."""
        test_cases = [
            {'name': 'Cosme Fulanito', 'email': 'cosme@fulanito.com', 'gitlab': 'cosme'},
            {'name': 'Cosme Fulanito', 'email': 'cosme@fulanito.com', 'gitlab': None},
        ]

        for data in test_cases:
            maintainer = models.TestMaintainer.create_from_dict(data)
            self.assertEqual(maintainer.email, data.get('email'))
            self.assertEqual(maintainer.name, data.get('name'))
            self.assertEqual(maintainer.gitlab_username, data.get('gitlab'))

    def test_create_from_dict_update(self):
        """Test create_from_dict, with changes so it saves."""
        models.TestMaintainer.objects.create(name='foo', email='foo@bar.com')
        with mock.patch('datawarehouse.models.test_models.TestMaintainer.save') as save_mock:
            models.TestMaintainer.create_from_dict({
                'name': 'foo',
                'email': 'foo@bar.com',
                'gitlab': 'username'
            })
            self.assertTrue(save_mock.called)

    def test_create_from_dict_no_update(self):
        """Test create_from_dict, no changes no save."""
        models.TestMaintainer.objects.create(name='foo', email='foo@bar.com')
        with mock.patch('datawarehouse.models.test_models.TestMaintainer.save') as save_mock:
            models.TestMaintainer.create_from_dict({
                'name': 'foo',
                'email': 'foo@bar.com',
                'gitlab': None,
            })
            self.assertFalse(save_mock.called)

    @override_settings(GITLAB_URL='https://gitlab.com')
    def test_gitlab_profile_url(self):
        """Test gitlab_profile_url property."""
        maintainer = models.TestMaintainer.objects.create(
            name='Some Name',
            email='some@email.com',
            gitlab_username='foobar',
        )
        self.assertEqual('https://gitlab.com/foobar', maintainer.gitlab_profile_url)

    @override_settings(GITLAB_URL='https://gitlab.com')
    def test_gitlab_profile_url_no_username(self):
        """Test gitlab_profile_url property when maintainer has no username."""
        maintainer = models.TestMaintainer.objects.create(
            name='Some Name',
            email='some@email.com',
            gitlab_username=None
        )
        self.assertEqual(None, maintainer.gitlab_profile_url)

    @override_settings(GITLAB_URL=None)
    def test_gitlab_profile_url_no_gitlab_url(self):
        """Test gitlab_profile_url property when GITLAB_URL is not set."""
        maintainer = models.TestMaintainer.objects.create(
            name='Some Name',
            email='some@email.com',
            gitlab_username='foobar',
        )
        self.assertEqual(None, maintainer.gitlab_profile_url)


class TestBeakerTask(utils.TestCase):
    """Test BeakerTask model."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def setUp(self):
        """Set up."""
        self.task = models.BeakerTask.objects.create(
            task_id=1,
            recipe_id=2
        )

    @mock.patch('datawarehouse.models.test_models.settings.BEAKER_URL', 'https://beaker')
    def test_recipe_url(self):
        """Test recipe_url property."""
        self.assertEqual('https://beaker/recipes/2', self.task.recipe_url)

    @mock.patch('datawarehouse.models.test_models.settings.BEAKER_URL', 'https://beaker')
    def test_task_url(self):
        """Test task_url property."""
        self.assertEqual('https://beaker/recipes/2#task1', self.task.task_url)

    def test_create_from_misc(self):
        """Test create_from_misc."""
        test = models.KCIDBTest.objects.first()
        task = models.BeakerTask.create_from_misc(
            test,
            {'beaker': {'task_id': 123, 'recipe_id': 321}}
        )

        self.assertEqual(123, task.task_id)
        self.assertEqual(321, task.recipe_id)
        self.assertEqual(test, task.kcidb_test)


class TestPipeline(utils.TestCase):
    """Test Pipeline model."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/pipeline_kcidb.yaml',
    ]

    def test_web_url(self):
        """Test web_url property."""
        pipeline = models.Pipeline.objects.get(pipeline_id=1)
        self.assertEqual(
            'https://gitlab.com/cki-project/brew-pipeline/-/pipelines/1',
            pipeline.web_url
        )


class TestProject(utils.TestCase):
    """Test Project model."""

    fixtures = [
        'tests/fixtures/basic.yaml',
    ]

    def test_web_url(self):
        """Test web_url property."""
        project = models.Project.objects.get(id=1)
        self.assertEqual(
            'https://gitlab.com/cki-project/brew-pipeline',
            project.web_url
        )


class TestGitlabJob(utils.TestCase):
    """Test GitlabJob model."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/pipeline_kcidb.yaml',
    ]

    def test_web_url(self):
        """Test web_url property."""
        gitlab_job = models.GitlabJob.objects.get(job_id=1)
        self.assertEqual(
            'https://gitlab.com/cki-project/brew-pipeline/-/jobs/1',
            gitlab_job.web_url
        )


class TestTest(utils.TestCase):
    """Test Test model."""

    def test_set_maintainers(self):
        """Test set_maintainers method."""
        test = models.Test.objects.create(name='some test')
        test.set_maintainers([
            {'name': 'Some One', 'email': 'some@one.com'},
            {'name': 'Some Other', 'email': 'some@other.com'}
        ])

        self.assertEqual(
            sorted([('Some One', 'some@one.com'), ('Some Other', 'some@other.com')]),
            sorted([(m.name, m.email) for m in test.maintainers.all()])
        )

    def test_get_and_update(self):
        """Test get_and_update method creates object."""
        with mock.patch('datawarehouse.models.Test.set_maintainers') as set_maintainers:
            test = models.Test.get_and_update(name='test_name', universal_id='test_path')
            self.assertEqual('test_name', test.name)
            self.assertEqual('test_path', test.universal_id)
            self.assertFalse(set_maintainers.called)

    def test_get_and_update_updates(self):
        """Test get_and_update method updates fields."""
        with mock.patch('datawarehouse.models.Test.set_maintainers') as set_maintainers:
            test = models.Test.get_and_update(name='test_name', universal_id=None)
            self.assertEqual('test_name', test.name)
            self.assertEqual(None, test.universal_id)
            self.assertFalse(set_maintainers.called)

            # Update it
            test_updated = models.Test.get_and_update(
                name='test_name',
                universal_id='test_path',
                maintainers=[
                    {'name': 'Some One', 'email': 'some@one.com'},
                ]
            )
            self.assertEqual(test.id, test_updated.id)
            self.assertEqual('test_name', test_updated.name)
            self.assertEqual('test_path', test_updated.universal_id)

            set_maintainers.assert_called_with(
                [{'name': 'Some One', 'email': 'some@one.com'}]
            )

    def test_get_and_update_with_maintainers(self):
        """Test get_and_update method."""
        with mock.patch('datawarehouse.models.Test.set_maintainers') as set_maintainers:
            test = models.Test.get_and_update(
                name='test_name',
                universal_id='test_path',
                maintainers=[
                    {'name': 'Some One', 'email': 'some@one.com'},
                ]
            )
            self.assertEqual('test_name', test.name)
            self.assertEqual('test_path', test.universal_id)

            set_maintainers.assert_called_with(
                [{'name': 'Some One', 'email': 'some@one.com'}]
            )


@override_settings(ARTIFACTS_DEFAULT_VALID_FOR_DAYS=123)
@override_settings(ARTIFACTS_KNOWN_VALID_FOR={'some.s3.host/BUCKET-NAME': 99})
class TestArtifact(utils.TestCase):
    """Test Artifact class."""

    def test_get_valid_for(self):
        # pylint: disable=protected-access
        """Test _get_valid_for method."""
        self.assertEqual(
            99, models.Artifact._get_valid_for('some.s3.host/BUCKET-NAME')
        )
        self.assertEqual(
            123, models.Artifact._get_valid_for('some.s3.host/OTHER-BUCKET-NAME')
        )

    def test_save_calculate_valid_for(self):
        """Test save sets the correct valid_for value."""
        artifact = models.Artifact.objects.create(
            name='test_file',
            url='https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log'
        )
        self.assertEqual(99, artifact.valid_for)

        # Unknown url host
        artifact = models.Artifact.objects.create(
            name='test_file',
            url='https://some.other.s3.host/BUCKET-NAME/some/path/to/the/file.log'
        )
        self.assertEqual(123, artifact.valid_for)

    def test_save_calculate_expiry_date(self):
        """Test save sets the correct expiry date."""
        created_on = datetime.datetime(2010, 1, 1, 10, 0, tzinfo=dateutil.tz.UTC)

        with freeze_time(created_on):
            artifact = models.Artifact.objects.create(
                name='test_file',
                url='https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log'
            )

        self.assertEqual(created_on + datetime.timedelta(days=99), artifact.expiry_date)

    def test_save_set_valid_for(self):
        """Test save sets the correct expiry date when valid_for is set."""
        created_on = datetime.datetime(2010, 1, 1, 10, 0, tzinfo=dateutil.tz.UTC)

        with freeze_time(created_on):
            artifact = models.Artifact.objects.create(
                name='test_file',
                url='https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log',
                valid_for=10,
            )

        self.assertEqual(10, artifact.valid_for)
        self.assertEqual(created_on + datetime.timedelta(days=10), artifact.expiry_date)
