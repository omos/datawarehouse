"""Test the rabbitmq module."""
from unittest import mock
from unittest.mock import call
from unittest.mock import patch

from django import test
import pika

from datawarehouse import models
from datawarehouse import rabbitmq


class MessageQueueTest(test.TestCase):
    """Unit tests for the MessageQueue module."""

    def setUp(self):
        """Set Up."""
        self.messagequeue = rabbitmq.MessageQueue('host', 123, 'user', 'password')
        self.messages = [
            ({'msg': index}, f'exchange_{index}') for index in range(3)
        ]

        for body, exchange in self.messages:
            self.messagequeue.add(body, exchange)

    def test_add(self):
        """Test add method."""
        self.assertEqual(3, models.MessagePending.objects.count())
        self.assertEqual(
            [f'{{"msg": {index}}}' for index in range(3)],
            list(models.MessagePending.objects.values_list('body', flat=True))
        )

    def test_send(self):
        """Test send method. Queue many messages and send them at once."""
        self.messagequeue.queue = mock.MagicMock()

        # Send all the messages.
        self.messagequeue.send()
        self.messagequeue.queue.send_message.assert_has_calls(
            [call(data=body, exchange=exchange, queue_name='') for body, exchange in self.messages]
        )

        # No messages pending.
        self.assertEqual(0, models.MessagePending.objects.count())

        # No messages in queue.
        self.messagequeue.queue.send_message.reset_mock()
        self.messagequeue.send()
        self.assertFalse(self.messagequeue.queue.send_message.called)

    @patch('datawarehouse.rabbitmq.pika.BlockingConnection', mock.Mock())
    def test_send_fails(self):
        # pylint: disable=protected-access
        """Test send method fails. Messages are kept in queue."""
        self.messagequeue.queue = mock.MagicMock()
        self.messagequeue.queue.send_message.side_effect = pika.exceptions.AMQPError()

        # Send all the messages. All fail.
        self.messagequeue.send()

        # All messages still pending.
        self.assertEqual(3, models.MessagePending.objects.count())

    @patch('datawarehouse.rabbitmq.pika.BlockingConnection', mock.Mock())
    def test_send_fails_middle(self):
        # pylint: disable=protected-access
        """Test send method fails while sending. Messages are kept in queue."""
        def mock_publish(**kwargs):
            """Mock publish. Fail on msg 1."""
            if kwargs['data']['msg'] == 1:
                raise pika.exceptions.AMQPError()

        self.messagequeue.queue = mock.MagicMock()
        self.messagequeue.queue.send_message = mock_publish

        # Send all the messages. Some fail.
        self.messagequeue.send()

        # Some messages still pending.
        self.assertEqual(2, models.MessagePending.objects.count())

    @patch('django.db.connection.close')
    def test_send_thread(self, close_connection):
        """Test send_thread closes the connectino."""
        self.messagequeue.send = mock.Mock()
        self.messagequeue.send_thread()

        self.assertTrue(self.messagequeue.send.called)
        self.assertTrue(close_connection.called)
