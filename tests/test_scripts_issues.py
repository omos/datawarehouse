"""Test scripts/issues.py."""
from datawarehouse import models
from datawarehouse import scripts
from tests import utils


class TestUpdateIssueOccurrencesRelatedCheckout(utils.TestCase):
    """
    Test update_issue_occurrences_related_checkout.

    Ensure IssueOccurrence.related_checkout is populated after
    assigning an issue to a KCIDB object.
    """

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/issues.yaml',
    ]

    def setUp(self):
        """Set up test."""
        self.issue = models.Issue.objects.last()

    def test_checkout(self):
        """"Test update_issue_occurrences_related_checkout with checkout."""
        checkout = models.KCIDBCheckout.objects.last()

        # Create IssueOccurrence to avoid triggering the signal
        models.IssueOccurrence.objects.create(issue=self.issue, kcidb_checkout=checkout)
        scripts.update_issue_occurrences_related_checkout(checkout, (self.issue.id, ))

        issue_occurrence = models.IssueOccurrence.objects.last()
        self.assertEqual(issue_occurrence.kcidb_checkout, checkout)
        self.assertEqual(issue_occurrence.related_checkout, checkout)

    def test_build(self):
        """"Test update_issue_occurrences_related_checkout with build."""
        build = models.KCIDBBuild.objects.last()

        # Create IssueOccurrence to avoid triggering the signal
        models.IssueOccurrence.objects.create(issue=self.issue, kcidb_build=build)
        scripts.update_issue_occurrences_related_checkout(build, (self.issue.id, ))

        issue_occurrence = models.IssueOccurrence.objects.last()
        self.assertEqual(issue_occurrence.kcidb_build, build)
        self.assertEqual(issue_occurrence.related_checkout, build.checkout)

    def test_test(self):
        """"Test update_issue_occurrences_related_checkout with test."""
        test = models.KCIDBTest.objects.last()

        # Create IssueOccurrence to avoid triggering the signal
        models.IssueOccurrence.objects.create(issue=self.issue, kcidb_test=test)
        scripts.update_issue_occurrences_related_checkout(test, (self.issue.id, ))

        issue_occurrence = models.IssueOccurrence.objects.last()
        self.assertEqual(issue_occurrence.kcidb_test, test)
        self.assertEqual(issue_occurrence.related_checkout, test.build.checkout)

    def test_multiple(self):
        """"Test update_issue_occurrences_related_checkout when adding multiple objects."""
        test = models.KCIDBTest.objects.last()
        issues = models.Issue.objects.all()

        # Create IssueOccurrence to avoid triggering the signal
        for issue in issues:
            models.IssueOccurrence.objects.create(issue=issue, kcidb_test=test)
        scripts.update_issue_occurrences_related_checkout(test, (i.id for i in issues))

        for issue_occurrence in models.IssueOccurrence.objects.all():
            self.assertEqual(issue_occurrence.kcidb_test, test)
            self.assertEqual(issue_occurrence.related_checkout, test.build.checkout)
