"""Test the scripts.misc module."""
import datetime
from unittest import mock

import django.contrib.auth.models as auth_models
from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from datawarehouse.scripts import misc
from datawarehouse.utils import datetime_bool
from tests import utils


class ScriptsMiscTest(utils.TestCase):
    """Unit tests for the scripts.misc module."""

    @staticmethod
    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification')
    def test_send_kcidb_objects_for_retriage(send_notif):
        """Test send_kcidb_object_for_retriage."""
        def get_date(days_ago):
            """Get timestamp from {days_ago}."""
            return timezone.now() - datetime.timedelta(days=days_ago)

        origin = models.KCIDBOrigin.objects.create(name='redhat')

        checkout_1 = models.KCIDBCheckout.objects.create(
            origin=origin, id='rev1',
            start_time=get_date(2),
            valid=False
        )
        models.KCIDBCheckout.objects.create(
            origin=origin, id='rev2',
            start_time=get_date(2),
            valid=True,
        )
        models.KCIDBCheckout.objects.create(
            origin=origin, id='rev3',
            start_time=get_date(4),
            valid=False,
        )
        build_1 = models.KCIDBBuild.objects.create(
            origin=origin, checkout=checkout_1, id='redhat:build-1',
            valid=False, start_time=get_date(2)
        )
        models.KCIDBBuild.objects.create(
            origin=origin, checkout=checkout_1, id='redhat:build-2',
            valid=True, start_time=get_date(2)
        )
        models.KCIDBBuild.objects.create(
            origin=origin, checkout=checkout_1, id='redhat:build-3',
            valid=False, start_time=get_date(4)
        )
        test_1 = models.KCIDBTest.objects.create(
            origin=origin, build=build_1, id='redhat:test-1',
            status=models.ResultEnum.FAIL,
            start_time=get_date(2)
        )
        models.KCIDBTest.objects.create(
            origin=origin, build=build_1, id='redhat:test-2',
            status=models.ResultEnum.PASS,
            start_time=get_date(2)
        )
        models.KCIDBTest.objects.create(
            origin=origin, build=build_1, id='redhat:test-3',
            status=models.ResultEnum.FAIL,
            start_time=get_date(4)
        )

        misc.send_kcidb_object_for_retriage(3)
        send_notif.assert_has_calls(
            [
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'checkout',
                    'object': serializers.KCIDBCheckoutSerializer(checkout_1).data,
                    'id': checkout_1.id,
                    'iid': checkout_1.iid,
                }]),
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'build',
                    'object': serializers.KCIDBBuildSerializer(build_1).data,
                    'id': build_1.id,
                    'iid': build_1.iid,
                }]),
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'test',
                    'object': serializers.KCIDBTestSerializer(test_1).data,
                    'id': test_1.id,
                    'iid': test_1.iid,
                }]),
            ]
        )


class TestRegressionNotifications(utils.TestCase):
    """Unit tests for issue_regression methods."""

    fixtures = [
        'tests/fixtures/issue_regressions.yaml',
    ]

    @mock.patch('datawarehouse.utils.async_send_email.delay')
    def test_notify_issue_regression_subscribers(self, mock_send_mail):
        """Test notify_issue_regression method for subscribed users."""
        checkout = models.KCIDBCheckout.objects.get(id='checkout_1')
        issue = models.Issue.objects.get(description='issue_1')

        misc.notify_issue_regression(checkout, issue)

        # No user is subscribed
        self.assertFalse(mock_send_mail.called)

        # Subscribe all users to issue regressions
        for user in auth_models.User.objects.all():
            models.UserSubscriptions.objects.get_or_create(
                user=user,
                issue_regression_subscribed_at=datetime_bool(True),
                issue_regression_visibility=models.SubscriptionVisibility.CC
            )

        misc.notify_issue_regression(checkout, issue)

        users = auth_models.User.objects.filter(username__in=('user_4', 'user_5'))
        subject = f'{ issue.kind.tag } | Issue regression detected'
        message = (
            'Hello,\n\n'
            'You are receiving this email because a regression was detected\n'
            'and tagged in DataWarehouse.\n\n'
            f'Issue: { issue.description}\n'
            f'URL: { issue.web_url }\n'
            f'Seen in: { checkout.web_url }\n\n'
            '--\n'
            'The DataWarehouse team.\n'
        )

        mock_send_mail.assert_called_with(subject, message, mock.ANY)
        self.assertEqual(
            set(mock_send_mail.call_args_list[0][0][2]['cc']),
            set(u.email for u in users)
        )

    @mock.patch('datawarehouse.utils.async_send_email.delay')
    def test_notify_issue_regression_subscribers_cc_bcc(self, mock_send_mail):
        """Test notify_issue_regression method for subscribed users respects visibility."""
        checkout = models.KCIDBCheckout.objects.get(id='checkout_1')
        issue = models.Issue.objects.get(description='issue_1')

        models.UserSubscriptions.objects.create(
            user=auth_models.User.objects.get(username='user_4'),
            issue_regression_subscribed_at=datetime_bool(True),
            issue_regression_visibility=models.SubscriptionVisibility.CC
        )

        models.UserSubscriptions.objects.create(
            user=auth_models.User.objects.get(username='user_5'),
            issue_regression_subscribed_at=datetime_bool(True),
            issue_regression_visibility=models.SubscriptionVisibility.BCC
        )

        misc.notify_issue_regression(checkout, issue)

        mock_send_mail.assert_called_with(
            mock.ANY, mock.ANY,
            {'cc': [auth_models.User.objects.get(username='user_4').email],
             'bcc': [auth_models.User.objects.get(username='user_5').email]}
        )

    @mock.patch('datawarehouse.utils.async_send_email.delay')
    def test_notify_issue_regression_maintainers(self, mock_send_mail):
        """Test notify_issue_regression method for test maintainers."""
        test = models.KCIDBTest.objects.get(id='test_1')
        issue = models.Issue.objects.get(description='issue_1')

        misc.notify_issue_regression(test, issue)

        subject = f'{ issue.kind.tag } | Issue regression detected'
        message = (
            'Hello,\n\n'
            'You are receiving this email because a regression was detected\n'
            'and tagged in DataWarehouse.\n\n'
            f'Issue: { issue.description}\n'
            f'URL: { issue.web_url }\n'
            f'Seen in: { test.web_url }\n\n'
            '--\n'
            'The DataWarehouse team.\n'
        )

        mock_send_mail.assert_called_with(
            subject, message, {'to': [test.test.maintainers.first().email], 'cc': [], 'bcc': []}
        )

    @mock.patch('datawarehouse.scripts.misc.notify_issue_regression')
    @mock.patch('datawarehouse.scripts.misc.settings.FF_NOTIFY_ISSUE_REGRESSION', True)
    def test_verify_issue_regression(self, mock_notify):
        """Test verify_issue_regression method."""
        checkout = models.KCIDBCheckout.objects.get(id='checkout_1')
        issue = models.Issue.objects.get(description='issue_1')

        # Issue not resolved, not a regression
        misc.verify_issue_regression(checkout, issue)
        self.assertFalse(mock_notify.called)

        # Issue resolved, but already tagged in the checkout, not a regression
        issue.resolved_on = timezone.now()
        issue.save()
        checkout.issues.add(issue)
        misc.verify_issue_regression(checkout, issue)
        self.assertFalse(mock_notify.called)

        # Issue resolved, not tagged in the checkout, is a regression
        checkout.issues.remove(issue)
        misc.verify_issue_regression(checkout, issue)
        self.assertTrue(mock_notify.called)

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.scripts.misc.notify_issue_regression')
    @mock.patch('datawarehouse.scripts.misc.settings.FF_NOTIFY_ISSUE_REGRESSION', True)
    def test_verify_issue_regression_resolved_on(self, mock_notify):
        """
        Test verify_issue_regression method.

        Ensure notifications are not sent for runs started before the issue
        was resolved.
        """
        def get_date(days):
            """Get timestamp from now + days."""
            return timezone.now() + datetime.timedelta(days=days)

        checkout = models.KCIDBCheckout.objects.get(id='checkout_1')
        issue = models.Issue.objects.get(description='issue_1')

        cases = [
            # Checkout.start_time,  Issue.resolved_on,  Expected notification
            (None,          None,           False),  # Not resolved, no notification
            (get_date(0),   None,           False),  # Not resolved, no notification
            (None,          get_date(0),    True),   # Resolved but no start_time, notification.
            (get_date(1),   get_date(0),    True),   # Ocurred after resolved, notification.
            (get_date(0),   get_date(1),    False),  # Ocurred before resolved, no notification.
        ]

        for checkout_dt, issue_dt, expected in cases:
            checkout.start_time = checkout_dt
            checkout.save()
            issue.resolved_on = issue_dt
            issue.save()

            misc.verify_issue_regression(checkout, issue)
            self.assertEqual(mock_notify.called, expected)
            mock_notify.reset_mock()

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.scripts.misc.notify_issue_regression')
    @mock.patch('datawarehouse.scripts.misc.settings.FF_NOTIFY_ISSUE_REGRESSION', True)
    def test_verify_issue_regression_all_objects(self, mock_notify):
        """Test verify_issue_regression method works with checkout, build and test."""
        issue = models.Issue.objects.get(description='issue_1')
        issue.resolved_on = timezone.now()
        issue.save()

        objects = [
            models.KCIDBCheckout.objects.last(),
            models.KCIDBBuild.objects.last(),
            models.KCIDBTest.objects.last(),
        ]

        for obj in objects:
            misc.verify_issue_regression(obj, issue)
            self.assertTrue(mock_notify.called)
            mock_notify.reset_mock()

    @mock.patch('datawarehouse.scripts.misc.notify_issue_regression')
    @mock.patch('datawarehouse.scripts.misc.settings.FF_NOTIFY_ISSUE_REGRESSION', False)
    def test_verify_issue_regression_disable(self, mock_notify):
        """Test verify_issue_regression method. FF_NOTIFY_ISSUE_REGRESSION is disabled."""
        checkout = models.KCIDBCheckout.objects.get(id='checkout_1')
        issue = models.Issue.objects.get(description='issue_1')

        # Issue resolved, not tagged in the checkout, is a regression, but it's disabled.
        misc.verify_issue_regression(checkout, issue)
        self.assertFalse(mock_notify.called)
