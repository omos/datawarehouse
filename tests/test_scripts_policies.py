"""Test scripts/policies.py."""
from unittest import mock

from django.contrib.auth import get_user_model
import django.contrib.auth.models as auth_models
from django.test import override_settings

from datawarehouse import models
from datawarehouse import scripts
from tests import utils

User = get_user_model()


class TestUpdateIssuePolicy(utils.TestCase):
    """
    Test update_issue_policy.

    Ensure Issue.policy is updated after new IssueOccurrence.
    """

    fixtures = [
        'tests/fixtures/issue_policy_auto.yaml',
    ]

    @staticmethod
    def _associate_issue(issue, checkout_id):
        """Associate issue to checkout."""
        checkout = models.KCIDBCheckout.objects.get(id=checkout_id)
        # Setting related_checkout we avoid having to test it for checkout, build and test
        models.IssueOccurrence.objects.create(issue=issue, related_checkout=checkout)

        scripts.update_issue_policy((issue.id, ))
        issue.refresh_from_db()

    def test_issue_auto_public_internal(self):
        """
        Test issue with policy_auto_public=True is updated.

        When related to internal checkout, keep it internal.
        """
        issue = models.Issue.objects.get(policy_auto_public=True)
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, 'internal_checkout')
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

    def test_issue_auto_public_public(self):
        """
        Test issue with policy_auto_public=True is updated.

        When related to public checkout, make it public.
        """
        issue = models.Issue.objects.get(policy_auto_public=True)
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, 'public_checkout')
        self.assertEqual(issue.policy.name, models.Policy.PUBLIC)

    def test_issue_auto_public_mixed(self):
        """
        Test issue with policy_auto_public=True is updated.

        When related to multiple checkout, make it public if any is public.
        """
        issue = models.Issue.objects.get(policy_auto_public=True)
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, 'internal_checkout')
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, 'public_checkout')
        self.assertEqual(issue.policy.name, models.Policy.PUBLIC)

    def test_issue_auto_public_mixed_inverse_order(self):
        """
        Test issue with policy_auto_public=True is updated.

        When related to multiple checkout, make it public if any is public.
        """
        issue = models.Issue.objects.get(policy_auto_public=True)
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, 'public_checkout')
        self.assertEqual(issue.policy.name, models.Policy.PUBLIC)

        self._associate_issue(issue, 'internal_checkout')
        self.assertEqual(issue.policy.name, models.Policy.PUBLIC)

    def test_issue_no_update(self):
        """Test issue with policy_auto_public=False is not updated."""
        issue = models.Issue.objects.get(policy_auto_public=False)
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, 'public_checkout')
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

    def test_issue_auto_public_multiple(self):
        """
        Test issue with policy_auto_public=True is updated.

        Test call with multiple issues.
        """
        issues = models.Issue.objects.all()
        # Set all issues as INTERNAL with auto update.
        issues.update(
            policy=models.Policy.objects.get(name=models.Policy.INTERNAL),
            policy_auto_public=True
        )
        self.assertTrue(issues.count() > 1)

        checkout = models.KCIDBCheckout.objects.get(id='public_checkout')
        for issue in issues:
            models.IssueOccurrence.objects.create(issue=issue, related_checkout=checkout)

        scripts.update_issue_policy({i.id for i in issues})

        for issue in issues:
            issue.refresh_from_db()
            self.assertEqual(issue.policy.name, models.Policy.PUBLIC)


@override_settings(LDAP_CONFIG={'server_url': 'ldap.url'})
@mock.patch('datawarehouse.scripts.policies.ldap.LDAPConnection.connect', mock.Mock())
class TestUpdateLDAPGroupMembers(utils.TestCase):
    """Test update_ldap_group_members."""

    fixtures = [
        'tests/fixtures/user_ldap_sync.yaml',
    ]

    @override_settings(FF_LDAP_GROUP_SYNC=False)
    @mock.patch('datawarehouse.scripts.policies.ldap.connection')
    def test_disabled(self, mock_conn):
        """Test it does nothing when FF_LDAP_GROUP_SYNC=False."""
        scripts.update_ldap_group_members()
        self.assertFalse(mock_conn.called)

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch('datawarehouse.scripts.policies.User.objects.filter')
    @mock.patch('datawarehouse.scripts.policies.ldap.connection')
    def test_enabled_no_links(self, mock_conn, mock_user):
        """Test it does nothing if FF_LDAP_GROUP_SYNC=True but no links."""
        scripts.update_ldap_group_members()
        self.assertTrue(mock_conn.called)
        self.assertFalse(mock_user.called)

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch('datawarehouse.scripts.policies.ldap.LDAPConnection.get_users')
    def test_enabled(self, mock_get_users):
        """Test each LDAPGroupLink is updated."""
        mock_get_users.return_value = []
        group_a = auth_models.Group.objects.get(name='group_a')
        group_b = auth_models.Group.objects.get(name='group_b')

        models.LDAPGroupLink.objects.create(
            group=group_a, filter_query='query group_a',
        )
        models.LDAPGroupLink.objects.create(
            group=group_b, filter_query='query group_b',
        )

        scripts.update_ldap_group_members()

        mock_get_users.assert_has_calls([
            mock.call('query group_a'),
            mock.call('query group_b'),
        ], any_order=True)

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch('datawarehouse.scripts.policies.ldap.LDAPConnection.get_users')
    def test_ldap_users(self, mock_get_users):
        """Test ldap users are correctly added."""
        group = auth_models.Group.objects.get(name='group_a')
        self.assertFalse(group.user_set.exists())

        models.LDAPGroupLink.objects.create(
            group=group,
            filter_query='some query but it is mocked',
        )
        mock_get_users.return_value = [
            ('test_user_1', 'test_user_1@mail.com'),
        ]

        scripts.update_ldap_group_members()

        self.assertEqual(
            set(User.objects.filter(username='test_user_1')),
            set(group.user_set.all())
        )

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch('datawarehouse.scripts.policies.ldap.LDAPConnection.get_users')
    def test_ldap_unknown(self, mock_get_users):
        """Test unkown ldap users do nothing."""
        group = auth_models.Group.objects.get(name='group_a')
        self.assertFalse(group.user_set.exists())

        models.LDAPGroupLink.objects.create(
            group=group,
            filter_query='some query but it is mocked',
        )
        mock_get_users.return_value = [
            ('other_user_1', 'other_user_1@mail.com'),
            ('other_user_2', 'other_user_2@mail.com'),
        ]

        scripts.update_ldap_group_members()

        self.assertFalse(group.user_set.exists())

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch('datawarehouse.scripts.policies.ldap.LDAPConnection.get_users')
    def test_ldap_mismatch_email(self, mock_get_users):
        """Test users with different emails."""
        group = auth_models.Group.objects.get(name='group_a')

        self.assertFalse(group.user_set.exists())

        models.LDAPGroupLink.objects.create(
            group=group,
            filter_query='some query but it is mocked',
        )
        mock_get_users.return_value = [
            ('test_user_1', 'wrong_mail_1@mail.com'),
            ('test_user_2', 'wrong_mail_2@mail.com'),
        ]

        scripts.update_ldap_group_members()

        self.assertFalse(group.user_set.exists())

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch('datawarehouse.scripts.policies.ldap.LDAPConnection.get_users')
    def test_extra_users(self, mock_get_users):
        """Test extra_users are added despite the query result."""
        user = User.objects.get(username='test_user_1')
        group = auth_models.Group.objects.get(name='group_a')
        self.assertFalse(user.groups.exists())

        link = models.LDAPGroupLink.objects.create(
            group=group,
            filter_query='some query but it is mocked',
        )
        link.extra_users.set([user])
        mock_get_users.return_value = []

        scripts.update_ldap_group_members()

        self.assertEqual(set([user]), set(group.user_set.all()))

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch('datawarehouse.scripts.policies.ldap.LDAPConnection.get_users')
    def test_delete_users(self, mock_get_users):
        """Test synchronization removes users once they're no longer linked."""
        user_3 = User.objects.get(username='test_user_3')
        group = auth_models.Group.objects.get(name='group_a')

        self.assertFalse(group.user_set.exists())

        link = models.LDAPGroupLink.objects.create(
            group=group,
            filter_query='some query but it is mocked',
        )
        link.extra_users.add(user_3)
        mock_get_users.return_value = [
            ('test_user_1', 'test_user_1@mail.com'),
            ('test_user_2', 'test_user_2@mail.com'),
        ]

        scripts.update_ldap_group_members()

        self.assertEqual(
            set(User.objects.filter(username__in=('test_user_1', 'test_user_2', 'test_user_3'))),
            set(group.user_set.all())
        )

        # User test_user_2 and test_user_3 are removed
        link.extra_users.set([])
        mock_get_users.return_value = [
            ('test_user_1', 'test_user_1@mail.com'),
        ]
        scripts.update_ldap_group_members()
        self.assertEqual(
            set(User.objects.filter(username__in=('test_user_1', ))),
            set(group.user_set.all())
        )
