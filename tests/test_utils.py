"""Test the utils module."""

import datetime
import os
import time
from unittest import mock

from captcha.models import CaptchaStore
import dateutil
from django import test
from django.conf import settings
from django.contrib.auth import get_user_model
import django.contrib.auth.models as auth_models
from django.http.request import QueryDict
from django.test import override_settings
from django.utils.timezone import make_aware
from freezegun import freeze_time
import responses

from datawarehouse import models
from datawarehouse import utils


class UtilsTestCase(test.TestCase):
    """Unit tests for the utils module."""

    TESTS_DIR = os.path.dirname(os.path.abspath(__file__))

    @responses.activate
    def test_parse_patches_from_urls(self):
        """Test the extraction of the patch subject."""
        patch_urls = ['https://server/patch1', 'https://server/patch2']
        subjects = ['panic: ensure preemption is disabled during panic()',
                    'USB: rio500: Remove Rio 500 kernel driver']

        def response_callback(resp):
            """Delay the first GET request to modify the responses order."""
            if resp.url.endswith('patch1'):
                time.sleep(0.1)
            return resp

        with responses.RequestsMock(response_callback=response_callback) as r_mock:
            r_mock.add(responses.GET, url=patch_urls[0],
                       body='From 20bb759a66be52cf4a9ddd17fddaf509e11490cd Mon Sep 17 00:00:00 2001\n' +
                       'From: Somebody <somebody@someplace.org>\n' +
                       'Date: Sun, 6 Oct 2019 17:58:00 -0700\n' +
                       f'Subject: {subjects[0]}\n' +
                       '\n' +
                       'From: Somebody <somebody@someplace.org>\n' +
                       '\n' +
                       'commit 20bb759a66be52cf4a9ddd17fddaf509e11490cd upstream.\n')
            r_mock.add(responses.GET, url=patch_urls[1],
                       body='From 015664d15270a112c2371d812f03f7c579b35a73 Mon Sep 17 00:00:00 2001\n' +
                       'From: Other <other@someplace.org>\n' +
                       'Date: Mon, 23 Sep 2019 18:18:43 +0200\n' +
                       f'Subject: {subjects[1]}\n' +
                       '\n' +
                       'From: Other <other@someplace.org>\n' +
                       '\n' +
                       'commit 015664d15270a112c2371d812f03f7c579b35a73 upstream.\n')

            result = utils.parse_patches_from_urls(patch_urls)
            expected = [{'url': url, 'subject': subject} for url, subject in zip(patch_urls, subjects)]
            self.assertEqual(expected, result)

    def test_timestamp_to_datetime(self):
        """Test timestamp_to_datetime."""
        self.assertEqual(None, utils.timestamp_to_datetime(None))

        test_datetime = datetime.datetime.now()
        test_datetime_aware = make_aware(test_datetime)

        self.assertEqual(
            test_datetime_aware,
            utils.timestamp_to_datetime(str(test_datetime))
        )

        test_timestamp = '2020-03-27T12:33:24.725Z'
        test_timestamp_datetime = datetime.datetime(2020, 3, 27, 12, 33, 24, 725000, tzinfo=dateutil.tz.UTC)
        self.assertEqual(test_timestamp_datetime, utils.timestamp_to_datetime(test_timestamp))

        test_timestamp = '2020-03-27'
        test_timestamp_datetime = datetime.datetime(2020, 3, 27, 0, 0, 0, 0, tzinfo=dateutil.tz.UTC)
        self.assertEqual(test_timestamp_datetime, utils.timestamp_to_datetime(test_timestamp))

        test_timestamp = '2020/03/27'
        test_timestamp_datetime = datetime.datetime(2020, 3, 27, 0, 0, 0, 0, tzinfo=dateutil.tz.UTC)
        self.assertEqual(test_timestamp_datetime, utils.timestamp_to_datetime(test_timestamp))

        test_timestamp = '2020-03-27T12:33:24.725+3'
        test_timestamp_datetime = datetime.datetime(2020, 3, 27, 12, 33, 24, 725000,
                                                    tzinfo=dateutil.tz.tzoffset(None, 10800))
        self.assertEqual(test_timestamp_datetime, utils.timestamp_to_datetime(test_timestamp))

    @mock.patch('datawarehouse.utils.settings.RABBITMQ_CONFIGURED', True)
    def test_send_kcidb_notification(self):
        """Test send_kcidb_notification."""
        utils.MSG_QUEUE = mock.Mock()
        utils.TIMER_MSG_QUEUE_SEND = mock.Mock()
        utils.send_kcidb_notification({'something': 'important'})
        utils.MSG_QUEUE.bulk_add.assert_called_with(
            [({'something': 'important'}, 'cki.exchange.datawarehouse.kcidb')]
        )
        self.assertTrue(utils.TIMER_MSG_QUEUE_SEND.start.called)

    @mock.patch('datawarehouse.utils.settings.RABBITMQ_CONFIGURED', False)
    def test_send_kcidb_notification_disabled(self):
        """Test send_kcidb_notification."""
        utils.MSG_QUEUE = mock.Mock()
        utils.send_kcidb_notification({'something': 'important'})
        self.assertFalse(utils.MSG_QUEUE.add.called)
        self.assertFalse(utils.MSG_QUEUE.send.called)

    def test_clean_dict(self):
        """Test clean_dict helper function."""
        self.assertEqual(
            {'a': 0, 'b': 1},
            utils.clean_dict({'a': 0, 'b': 1, 'c': None})
        )

    def test_filter_view(self):
        """Test filter_view function."""
        filters = {
            'filter_bar': {'field': 'bar'},
            'filter_baz': {'field': 'baz', 'is_list': True},
            'filter_unknown': {'field': 'unk'},
        }
        query = QueryDict('filter_bar=value_for_bar&filter_baz=value_for_baz')
        request = mock.Mock(GET=query)

        queryset = mock.Mock()
        _, applied_filters = utils.filter_view(request, queryset, filters)

        self.assertEqual(
            {'filter_bar': 'value_for_bar', 'filter_baz': ['value_for_baz']},
            applied_filters
        )

        queryset.filter.assert_has_calls(
            [mock.call(bar='value_for_bar')],
            [mock.call(baz__in=['value_for_baz'])],
        )

    def test_filter_view_empty(self):
        """Test filter_queryset_view function. No filters."""
        query = QueryDict('bar=bar&foo=foo')
        request = mock.Mock(GET=query)

        queryset = mock.Mock()
        _, applied_filters = utils.filter_view(request, queryset, {})

        self.assertEqual({}, applied_filters)
        self.assertFalse(queryset.filter.called)

    def test_filter_checkouts_view(self):
        """Test filter_checkouts_view function."""
        query = QueryDict('bar=bar&filter_email=foo&filter_gittrees=git1&filter_gittrees=git2')
        request = mock.Mock(GET=query)

        checkouts = mock.Mock()
        returned_checkouts, filters = utils.filter_checkouts_view(request, checkouts)

        self.assertEqual(
            {'filter_email': 'foo', 'filter_gittrees': ['git1', 'git2']},
            filters
        )

        checkouts.filter.assert_has_calls(
            [mock.call(contacts__email__icontains='foo')],
            [mock.call(tree__name__in=['git1', 'git2'])],
        )

    def test_filter_checkouts_view_empty(self):
        """Test filter_checkouts_view function. No filters."""
        query = QueryDict('bar=bar&foo=foo')
        request = mock.Mock(GET=query)

        checkouts = mock.Mock()
        returned_checkouts, filters = utils.filter_checkouts_view(request, checkouts)

        self.assertEqual({}, filters)
        self.assertFalse(checkouts.filter.called)

    def test_filter_checkouts_view_present_empty(self):
        """Test filter_checkouts_view function. Filter key is present but empty."""
        query = QueryDict('bar=bar&foo=foo&filter_email=')
        request = mock.Mock(GET=query)

        checkouts = mock.Mock()
        returned_checkouts, filters = utils.filter_checkouts_view(request, checkouts)

        self.assertEqual({}, filters)
        self.assertFalse(checkouts.filter.called)

    def test_filter_checkouts_view_related_object(self):
        """Test filter_checkouts_view with a related object."""
        query = QueryDict('bar=bar&filter_email=foo&filter_gittrees=git1&filter_gittrees=git2')
        request = mock.Mock(GET=query)

        checkouts = mock.Mock()
        _, filters = utils.filter_checkouts_view(request, checkouts, path_to_checkout='build__checkout__')

        self.assertEqual(
            {'filter_email': 'foo', 'filter_gittrees': ['git1', 'git2']},
            filters
        )

        checkouts.filter.assert_has_calls(
            [mock.call(build__checkout__contacts__email__icontains='foo')],
            [mock.call(build__checkout__tree__name__in=['git1', 'git2'])],
        )

    def test_filter_builds_view(self):
        """Test filter_builds_view function."""
        query = QueryDict('bar=bar&filter_architectures=aarch64')
        request = mock.Mock(GET=query)

        queryset = mock.Mock()
        _, filters = utils.filter_builds_view(request, queryset)

        self.assertEqual(
            {'filter_architectures': ['aarch64']},
            filters
        )

        queryset.filter.assert_has_calls(
            [mock.call(architecture__in=['aarch64'])],
        )

    def test_filter_builds_view_related_object(self):
        """Test filter_builds_view function with a related object."""
        query = QueryDict('bar=bar&filter_architectures=aarch64')
        request = mock.Mock(GET=query)

        queryset = mock.Mock()
        _, filters = utils.filter_builds_view(request, queryset, path_to_build='build__')

        self.assertEqual(
            {'filter_architectures': ['aarch64']},
            filters
        )

        queryset.filter.assert_has_calls(
            [mock.call(build__architecture__in=['aarch64'])],
        )

    @staticmethod
    @mock.patch('datawarehouse.utils.EmailMessage')
    def test_async_send_email_recipients_string(email_mock):
        """Test async_send_email does the right thing. Recipients is a string."""
        utils.async_send_email(
            'subject_1', 'message_1', 'addr1@mail.com'
        )

        email_mock.assert_has_calls([
            mock.call(
                subject='subject_1',
                body='message_1',
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=['addr1@mail.com'],
                cc=[],
                bcc=[],
                headers={}
            ),
            mock.call().send()
        ])

    @staticmethod
    @mock.patch('datawarehouse.utils.EmailMessage')
    def test_async_send_email_recipients_list(email_mock):
        """Test async_send_email does the right thing. Recipients is a list."""
        utils.async_send_email(
            'subject_1', 'message_1', ['addr1@mail.com', 'addr2@mail.com']
        )

        email_mock.assert_has_calls([
            mock.call(
                subject='subject_1',
                body='message_1',
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=['addr1@mail.com', 'addr2@mail.com'],
                cc=[],
                bcc=[],
                headers={}
            ),
            mock.call().send()
        ])

    @staticmethod
    @mock.patch('datawarehouse.utils.EmailMessage')
    def test_async_send_email_recipients_dict(email_mock):
        """Test async_send_email does the right thing. Recipients is a dict."""
        utils.async_send_email(
            'subject_1', 'message_1',
            {'to': ['addr1@mail.com', 'addr2@mail.com'],
             'cc': ['addr3@mail.com'],
             'bcc': ['addr4@mail.com']}
        )

        email_mock.assert_has_calls([
            mock.call(
                subject='subject_1',
                body='message_1',
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=['addr1@mail.com', 'addr2@mail.com'],
                cc=['addr3@mail.com'],
                bcc=['addr4@mail.com'],
                headers={}
            ),
            mock.call().send()
        ])

    @staticmethod
    @mock.patch('datawarehouse.utils.async_send_email.delay')
    def test_notify_user(send_mock):
        """Test notify_user calls async_send_email."""
        user, _ = get_user_model().objects.get_or_create(username='test', email='test@mail.com')

        utils.notify_user('subject', 'message', user)
        send_mock.assert_called_with('subject', 'message', 'test@mail.com')

    @freeze_time("2010-01-02 09:00:00")
    def test_datetime_bool(self):
        """Test datetime_bool method."""
        cases = [
            # value, returned timestamp
            (True, '2010-01-02T09:00:00+00:00'),
            ('something not empty', '2010-01-02T09:00:00+00:00'),
            (False, None),
            ('', None),
        ]

        for value, timestamp in cases:
            self.assertEqual(
                utils.timestamp_to_datetime(timestamp),
                utils.datetime_bool(value)
            )

    def test_query_id_or_iid(self):
        """Test query_id_or_iid."""
        cases = [
            ('1234', {'iid': '1234'}),
            ('1000000000', {'iid': '1000000000'}),
            ('redhat:1234', {'id': 'redhat:1234'}),
        ]

        for value, expected in cases:
            self.assertEqual(utils.query_id_or_iid(value), expected, value)


class CaptchaUtilsTestCase(test.TestCase):
    """Unit tests for captcha utility methods."""

    def setUp(self):
        """SetUp."""
        self.captcha = utils.get_captcha()
        self.store = CaptchaStore.objects.get(hashkey=self.captcha['key'])

    def test_get_captcha(self):
        """Test get_captcha method."""
        self.assertEqual(self.captcha['key'], self.store.hashkey)
        self.assertEqual(f"/captcha/image/{self.captcha['key']}/", self.captcha['image'])

        response = self.client.get(self.captcha['image'])
        self.assertEqual(200, response.status_code)

        CaptchaStore.objects.get(hashkey=self.captcha['key']).delete()
        response = self.client.get(self.captcha['image'])
        self.assertEqual(410, response.status_code)

    def test_verify_captcha_test_mode(self):
        """Test verify_captcha method."""
        self.assertFalse(utils.verify_captcha(self.captcha['key'], self.store.challenge))

        self.assertFalse(utils.verify_captcha("non-existing-key", "some-challenge"))
        self.assertTrue(utils.verify_captcha("non-existing-key", "passed"))

    @override_settings(CAPTCHA_TEST_MODE=False)
    def test_verify_captcha(self):
        """Test verify_captcha method, without test mode."""
        self.assertTrue(utils.verify_captcha(self.captcha['key'], self.store.challenge))
        # Challenge is deleted after verifying, should not match twice.
        self.assertFalse(utils.verify_captcha(self.captcha['key'], self.store.challenge))

        self.assertFalse(utils.verify_captcha("non-existing-key", "some-challenge"))
        self.assertFalse(utils.verify_captcha("non-existing-key", "passed"))


class CacheAnonymousTestCase(test.TestCase):
    """Unit tests for cache_anonymous."""

    @staticmethod
    @utils.cache_anonymous()
    def view(request, *args, **kwargs):
        """Mock view."""
        return 'called'

    @override_settings(FF_CACHE_ANONYMOUS=False)
    @mock.patch('datawarehouse.utils.cache_page')
    def test_disabled(self, mock_cache):
        """Test cache_page is not called when disabled."""
        request = mock.Mock()
        request.user = auth_models.AnonymousUser()
        self.assertEqual('called', self.view(request))

        self.assertFalse(mock_cache.called)

    @override_settings(FF_CACHE_ANONYMOUS=True)
    @mock.patch('datawarehouse.utils.cache_page')
    def test_anonymous(self, mock_cache):
        """Test mock_cache is called for AnonymousUser."""
        request = mock.Mock()
        request.user = auth_models.AnonymousUser()
        self.view(request)

        self.assertTrue(mock_cache.called)
        mock_cache.assert_has_calls([
            mock.call(300),
            mock.call()(CacheAnonymousTestCase.view.__wrapped__),
            mock.call()()(request),
        ])

    @override_settings(FF_CACHE_ANONYMOUS=True)
    @mock.patch('datawarehouse.utils.cache_page')
    def test_authenticated(self, mock_cache):
        """Test mock_cache is not called for User."""
        request = mock.Mock()
        request.user = auth_models.User.objects.create_user(username='test')
        self.assertEqual('called', self.view(request))

        self.assertFalse(mock_cache.called)


class TestGroupIssueOccurrences(test.TestCase):
    """Test group_issue_occurrences method."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/multiple_issue_occurrences.yaml',
    ]

    def test_empty(self):
        """Test with empty list of issue occurrences."""
        self.assertEqual(
            [], utils.group_issue_occurrences([])
        )

    def test_call(self):
        """Test calling with a list of issue occurrences."""
        issue_occurrences = models.IssueOccurrence.objects.all()

        issue_1 = models.Issue.objects.get(description='Issue Public')
        issue_2 = models.Issue.objects.get(description='Issue Public 2')
        issue_3 = models.Issue.objects.get(description='Issue Public 3')

        self.assertEqual(
            [{'issue': issue_3,
              'checkouts': [],
              'builds': [models.IssueOccurrence.objects.get(id=4)],
              'tests': [models.IssueOccurrence.objects.get(id=3)]},
             {'issue': issue_2,
              'checkouts': [],
              'builds': [models.IssueOccurrence.objects.get(id=2)],
              'tests': []},
             {'issue': issue_1,
              'checkouts': [models.IssueOccurrence.objects.get(id=1)],
              'builds': [],
              'tests': [models.IssueOccurrence.objects.get(id=5)]}],
            utils.group_issue_occurrences(issue_occurrences)
        )
