"""Test the views module."""
from django.db.models import Count

from datawarehouse import models
from datawarehouse.utils import datetime_bool
from tests import utils


class ViewsTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the views module."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/base_simple.yaml',
        'tests/fixtures/base_many_tests.yaml',
    ]

    def setUp(self):
        """Set up tests."""
        self.hosts = models.BeakerResource.objects.all().order_by('id')
        self.tests = models.Test.objects.all().order_by('id')
        self.checkout = models.KCIDBCheckout.objects.get()
        self.test_runs = models.KCIDBTest.objects.filter(build__checkout=self.checkout)

    def test_details_test(self):
        """Test all elements from a certain test details query."""
        response = self.client.get('/details/test/1')
        self.assertContextEqual(response.context, {
            'item': self.tests[0],
            'type': 'test',
            'results': models.ResultEnum,
            'result_filter': None,
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(test=self.tests[0])},
            ],
        })

    def test_details_test_another(self):
        """Test that getting a different test works."""
        response = self.client.get('/details/test/2')
        self.assertContextEqual(response.context, {
            'item': self.tests[1],
        })

    def test_details_tests_result(self):
        """Test filtering by results."""
        response = self.client.get('/details/test/1?result=PASS')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(test=self.tests[0], status=models.ResultEnum.PASS)},
            ],
        })

        response = self.client.get('/details/test/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(test=self.tests[0], status=models.ResultEnum.FAIL)},
            ],
        })

    def test_details_tests_table(self):
        """Test returned table."""
        response = self.client.get('/details/test/2')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(kcidbtest__test__id=2).distinct()
        })

        self.assertEqual(response.context['table'][0].total_runs, 3)
        self.assertEqual(response.context['table'][0].FAIL, 1)
        self.assertEqual(response.context['table'][0].PASS, 1)
        self.assertEqual(response.context['table'][0].ERROR, 1)

    def test_details_tests_table_result(self):
        """Test returned table when filtering by result."""
        response = self.client.get('/details/test/1?result=PASS')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(
                kcidbtest__test__id=1,
                kcidbtest__status=models.ResultEnum.PASS
            ).distinct()
        })

        response = self.client.get('/details/test/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(
                kcidbtest__test__id=1,
                kcidbtest__status=models.ResultEnum.FAIL
            ).distinct()
        })

    def test_details_host(self):
        """Test all elements from a certain host details query."""
        response = self.client.get('/details/host/1')
        self.assertContextEqual(response.context, {
            'item': self.hosts[0],
            'type': 'host',
            'results': models.ResultEnum,
            'result_filter': None,
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(environment=self.hosts[0])},
            ],
        })

    def test_details_host_another(self):
        """Test that getting a different host works."""
        response = self.client.get('/details/host/2')
        self.assertContextEqual(response.context, {
            'item': self.hosts[1],
        })

    def test_details_hosts_result(self):
        """Test filtering by results."""
        response = self.client.get('/details/host/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(environment=self.hosts[0], status=models.ResultEnum.FAIL)},
            ],
        })

        response = self.client.get('/details/host/1?result=PASS')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(environment=self.hosts[0], status=models.ResultEnum.PASS)},
            ],
        })

    def test_details_hosts_table(self):
        """Test returned table."""
        response = self.client.get('/details/host/1')
        self.assertContextEqual(response.context, {
            'table': self.tests.filter(kcidbtest__environment__id=1).distinct()
        })

        self.assertEqual(response.context['table'][0].total_runs, 3)
        self.assertEqual(response.context['table'][0].FAIL, 1)
        self.assertEqual(response.context['table'][0].PASS, 1)
        self.assertEqual(response.context['table'][0].ERROR, 1)

    def test_details_hosts_table_result(self):
        """Test returned table when filtering by result."""
        response = self.client.get('/details/host/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'table': self.tests.filter(
                kcidbtest__environment__id=1,
                kcidbtest__status=models.ResultEnum.FAIL
            ).distinct()
        })

        response = self.client.get('/details/host/1?result=PASS')
        self.assertContextEqual(response.context, {
            'table': self.tests.filter(
                kcidbtest__environment__id=1,
                kcidbtest__status=models.ResultEnum.PASS
            ).distinct()
        })


class TestIssueCreateEditAnonymous(utils.KCIDBTestCase):
    """Issue new and edit test case. Anonymous."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml'
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_policies = models.Policy.objects.filter(id__in=self.policies_authorized[method])
        no_auth_policies = models.Policy.objects.exclude(id__in=self.policies_authorized[method])

        checks = [
            (auth_policies, 'No authorized policies'),
            (no_auth_policies, 'No unauthorized policies'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_issue_new(self):
        """Test creating new issues."""
        self._ensure_test_conditions('write')
        authorized_policies = models.Policy.objects.filter(
            id__in=self.policies_authorized['write']
        )

        for policy in models.Policy.objects.all():
            authorized = policy in authorized_policies and not self.anonymous

            issue_kind = models.IssueKind.objects.first()
            post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                    'kind_id': issue_kind.id, 'policy_id': policy.id})

            status_code = 302 if authorized else 403
            response = self.assert_authenticated_post(status_code, 'add_issue', *post_args, user=self.user)

            if not authorized:
                continue

            # Check that it was created ok.
            issue = models.Issue.objects.get(ticket_url='http://some.url')
            self.assertEqual('foo bar', issue.description)
            self.assertEqual(issue_kind, issue.kind)
            self.assertEqual(policy, issue.policy)
            self.assertFalse(issue.policy_auto_public)
            self.assertEqual(self.user, issue.created_by)
            self.assertIsNone(issue.last_edited_by)

            # Same ticket_url. Fail.
            post_args = ('/issue', {'description': 'bar bar', 'ticket_url': 'http://some.url',
                                    'kind_id': issue_kind.id, 'policy_id': policy.id})
            response = self.assert_authenticated_post(400, 'add_issue', *post_args, user=self.user)
            self.assertEqual(b'Issue already exists with ticket URL http://some.url', response.content)

            # Another ticket_url. Ok
            post_args = ('/issue', {'description': 'bar bar', 'ticket_url': 'http://other.url',
                                    'kind_id': issue_kind.id, 'policy_id': policy.id})
            response = self.assert_authenticated_post(302, 'add_issue', *post_args, user=self.user)

            # Cleanup
            models.Issue.objects.all().delete()

    def test_issue_whitespace_removal(self):
        """Test issues handle trailing spaces on description and ticket_url."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        post_args = ('/issue', {'description': '   foo bar ', 'ticket_url': ' http://some.url ',
                                'kind_id': issue_kind.id, 'policy_id': policy.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertEqual(issue.description, 'foo bar')
        self.assertEqual(issue.ticket_url, 'http://some.url')

        # Change the values.
        post_args[1]['issue_id'] = issue.id
        post_args[1]['description'] = ' foo foo    '
        post_args[1]['ticket_url'] = ' http://other.url '
        self.assert_authenticated_post(302, 'change_issue', *post_args)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertEqual(issue.description, 'foo foo')
        self.assertEqual(issue.ticket_url, 'http://other.url')

    def test_issue_new_kernel_bug_origin_tree(self):
        """Test creating new kernel bug issues, with an origin tree."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="Kernel bug", tag="kb", kernel_code_related=True)
        git_tree = models.GitTree.objects.create(name="mainline")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'origin_tree_id': git_tree.id,
                                'policy_id': policy.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Check that the origin tree is correct
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertEqual(git_tree.id, issue.origin_tree.id)

    def test_issue_new_non_kernel_bug_should_ignore_origin_tree(self):
        """Test creating new kernel bug issues, with an origin tree."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="Kernel bug", tag="kb", kernel_code_related=False)
        git_tree = models.GitTree.objects.create(name="mainline")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'origin_tree_id': git_tree.id,
                                'policy_id': policy.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Check that the origin tree is correct
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertIsNone(issue.origin_tree)

    def test_issue_new_policy_auto_public(self):
        """Test setting policy_auto_public flag on issue creation."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="Kernel bug", tag="kb", kernel_code_related=True)
        git_tree = models.GitTree.objects.create(name="mainline")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'origin_tree_id': git_tree.id,
                                'policy_id': policy.id,
                                'policy_auto_public': 'on'})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Check that the origin tree is correct
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertTrue(issue.policy_auto_public)

    def test_issue_edit(self):
        """Test editing issues."""
        self._ensure_test_conditions('write')
        authorized_policies = models.Policy.objects.filter(
            id__in=self.policies_authorized['write']
        )

        for policy in models.Policy.objects.all():
            authorized = policy in authorized_policies and not self.anonymous

            policy_2 = models.Policy.objects.create(
                name='another_one',
                read_group=policy.read_group,
                write_group=policy.write_group
            )
            issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
            issue_kind_2 = models.IssueKind.objects.create(description="fail 2", tag="2")
            post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                    'kind_id': issue_kind.id, 'policy_id': policy.id})

            status_code = 302 if authorized else 403
            self.assert_authenticated_post(status_code, 'add_issue', *post_args, user=self.user)

            if not authorized:
                policy_2.delete()
                continue

            # Change the values.
            issue = models.Issue.objects.get(ticket_url='http://some.url')
            post_args = ('/issue', {'issue_id': issue.id, 'description': 'var var',
                                    'ticket_url': 'http://other.url', 'kind_id': issue_kind_2.id,
                                    'policy_id': policy_2.id})
            self.assert_authenticated_post(302, 'change_issue', *post_args, user=self.user)

            # Check new values.
            issue = models.Issue.objects.get(id=issue.id)
            self.assertEqual(issue.description, 'var var')
            self.assertEqual(issue.ticket_url, 'http://other.url')
            self.assertEqual(issue.kind, issue_kind_2)
            self.assertEqual(issue.policy, policy_2)
            self.assertEqual(issue.last_edited_by, self.user)

            # Cleanup
            issue.delete()
            policy_2.delete()

    def test_issue_edit_can_add_origin_tree_on_kernel_bugs(self):
        """Test editing issues."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1", kernel_code_related=True)
        git_tree = models.GitTree.objects.create(name="mainline")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id, 'policy_id': policy.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Change the values.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        post_args = ('/issue', {'issue_id': issue.id,
                                'description': 'foo bar',
                                'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'origin_tree_id': git_tree.id,
                                'policy_id': policy.id})
        self.assert_authenticated_post(302, 'change_issue', *post_args)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertEqual(git_tree.id, issue.origin_tree.id)

    def test_issue_edit_origin_tree_on_non_kernel_bugs_should_be_ignored(self):
        """Test editing issues."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1", kernel_code_related=False)
        git_tree = models.GitTree.objects.create(name="mainline")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id, 'policy_id': policy.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Change the values.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        post_args = ('/issue', {'issue_id': issue.id, 'description': 'foo bar',
                                'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'origin_tree_id': git_tree.id,
                                'policy_id': policy.id})
        self.assert_authenticated_post(302, 'change_issue', *post_args)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertIsNone(issue.origin_tree)

    def test_issue_edit_url_check(self):
        """Test when editing issues it's not possible to duplicate the URL."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        self.assert_authenticated_post(
            302, 'add_issue', '/issue', {'description': 'foo bar',
                                         'ticket_url': 'http://some.url', 'kind_id': issue_kind.id,
                                         'policy_id': policy.id})

        self.assert_authenticated_post(
            302, 'add_issue', '/issue', {'description': 'foo bar', 'ticket_url': 'http://other.url',
                                         'kind_id': issue_kind.id, 'policy_id': policy.id})

        # Change the values. The url is already used by the second issue.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        response = self.assert_authenticated_post(
            400, 'change_issue', '/issue', {'issue_id': issue.id, 'description': 'foo bar',
                                            'ticket_url': 'http://other.url', 'kind_id': issue_kind.id,
                                            'policy_id': policy.id})
        self.assertEqual(b'Issue already exists with ticket URL http://other.url', response.content)

    def test_issue_edit_policy_auto_public(self):
        """Test setting policy_auto_public flag on issue edit."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1", kernel_code_related=False)
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id, 'policy_id': policy.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertFalse(issue.policy_auto_public)

        # Change the values.
        post_args = ('/issue', {'issue_id': issue.id, 'description': 'foo bar',
                                'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'policy_id': policy.id,
                                'policy_auto_public': 'on'})
        self.assert_authenticated_post(302, 'change_issue', *post_args)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertTrue(issue.policy_auto_public)


class TestIssueCreateEditReadGroup(TestIssueCreateEditAnonymous):
    """Issue new and edit test case. Read group."""

    anonymous = False
    groups = ['group_a']


class TestIssueCreateEditWriteGroup(TestIssueCreateEditAnonymous):

    """Issue new and edit test case. Write group."""
    anonymous = False
    groups = ['group_b']


class TestIssueCreateEditAllGroups(TestIssueCreateEditAnonymous):
    """Issue new and edit test case. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueRegexAnonymous(utils.KCIDBTestCase):
    """Issue Regex test case. Anonymous."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml'
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_issues = models.Issue.objects.filter(id__in=self.issues_authorized[method])
        no_auth_issues = models.Issue.objects.exclude(id__in=self.issues_authorized[method])

        checks = [
            (auth_issues, 'No authorized issues'),
            (no_auth_issues, 'No unauthorized issues'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_create(self):
        """Test create IssueRegex."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            post_args = (
                '/issue/-/regex',
                {
                    'action': 'new',
                    'issue_id_select': issue.id,
                    'text_match': ' foo',
                    'file_name_match': ' bar',
                    'test_name_match': ' foobar ',
                }
            )

            status_code = 302 if authorized else 403
            self.assert_authenticated_post(status_code, 'add_issueregex', *post_args, user=self.user)

            if not authorized:
                continue

            issue_regex = models.IssueRegex.objects.get(issue=issue)
            self.assertEqual('foo', issue_regex.text_match)
            self.assertEqual('bar', issue_regex.file_name_match)
            self.assertEqual('foobar', issue_regex.test_name_match)
            self.assertEqual(issue, issue_regex.issue)

            self.assertEqual(self.user, issue_regex.created_by)
            self.assertIsNone(issue_regex.last_edited_by)

    def test_create_incomplete(self):
        """Test create IssueRegex without all the fields."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            post_args = (
                '/issue/-/regex',
                {
                    'action': 'new',
                    'issue_id_select': issue.id,
                    'text_match': ' foo',
                }
            )

            status_code = 302 if authorized else 403
            self.assert_authenticated_post(status_code, 'add_issueregex', *post_args, user=self.user)

            if not authorized:
                continue

            issue_regex = models.IssueRegex.objects.get(issue=issue)
            self.assertEqual('foo', issue_regex.text_match)
            self.assertEqual(None, issue_regex.file_name_match)
            self.assertEqual(None, issue_regex.test_name_match)
            self.assertEqual(issue, issue_regex.issue)

    def test_edit(self):
        """Test edit IssueRegex."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            issue_regex = models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
            )
            issue_2 = models.Issue.objects.create(
                description='foo foo',
                kind=issue.kind,
                ticket_url=f'http://some.other.url.{issue.id}',
                policy=issue.policy,
            )

            post_args = (
                '/issue/-/regex',
                {
                    'action': 'edit',
                    'issue_regex_id': issue_regex.id,
                    'issue_id_select': issue_2.id,
                    'text_match': 'barbar ',
                    'file_name_match': ' bar',
                    'test_name_match': ' foobar',
                }
            )

            status_code = 302 if authorized else 403
            self.assert_authenticated_post(status_code, 'change_issueregex', *post_args, user=self.user)

            if not authorized:
                continue

            issue_regex = models.IssueRegex.objects.get(id=issue_regex.id)
            self.assertEqual('barbar', issue_regex.text_match)
            self.assertEqual('bar', issue_regex.file_name_match)
            self.assertEqual('foobar', issue_regex.test_name_match)
            self.assertEqual(issue_2, issue_regex.issue)

            self.assertEqual(self.user, issue_regex.last_edited_by)
            self.assertEqual(None, issue_regex.created_by)

    def test_edit_incomplete(self):
        """Test edit IssueRegex."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            issue_regex = models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
            )

            post_args = (
                '/issue/-/regex',
                {
                    'action': 'edit',
                    'issue_regex_id': issue_regex.id,
                    'issue_id_select': issue.id,
                    'text_match': 'barbar ',
                }
            )

            status_code = 302 if authorized else 403
            self.assert_authenticated_post(status_code, 'change_issueregex', *post_args, user=self.user)

            if not authorized:
                continue

            issue_regex = models.IssueRegex.objects.get(id=issue_regex.id)
            self.assertEqual('barbar', issue_regex.text_match)
            self.assertEqual(None, issue_regex.file_name_match)
            self.assertEqual(None, issue_regex.test_name_match)

    def test_delete(self):
        """Test delete IssueRegex."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            issue_regex = models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
            )
            post_args = (
                '/issue/-/regex',
                {
                    'action': 'delete',
                    'issue_regex_id': issue_regex.id,
                }
            )

            status_code = 302 if authorized else 403
            self.assert_authenticated_post(status_code, 'delete_issueregex', *post_args, user=self.user)

            self.assertEqual(
                not authorized,  # If authorized, the model doesnt exists anymore
                models.IssueRegex.objects.filter(id=issue_regex.id).exists()
            )

    def test_get_single(self):
        """Test get IssueRegex."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        for issue in models.Issue.objects.all():
            issue_regex = models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
            )

            response = self.client.get(f'/issue/-/regex/{issue_regex.id}')

            if issue not in authorized_issues:
                self.assertEqual(404, response.status_code)
                continue

            self.assertContextEqual(
                response.context,
                {
                    'issue_regex': issue_regex,
                    'issues': authorized_issues,
                },
            )

    def test_get_single_resolved(self):
        """Test get IssueRegex for resolved issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        for issue in models.Issue.objects.all():
            issue_regex = models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
            )

            # Resolve the issue related to the regex.
            issue.resolved_on = datetime_bool(True)
            issue.save()

            response = self.client.get(f'/issue/-/regex/{issue_regex.id}')

            if issue not in authorized_issues:
                self.assertEqual(404, response.status_code)
                continue

            self.assertContextEqual(
                response.context,
                {
                    'issue_regex': issue_regex,
                    # The issue is in authorized_issues, must be included on the context.
                    'issues': authorized_issues,
                },
            )

            # Restore to non resolved
            issue.resolved_on = None
            issue.save()

    def test_get_list(self):
        """Test get all IssueRegex."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        for issue in models.Issue.objects.all():
            models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
            )
        response = self.client.get('/issue/-/regex')

        self.assertContextEqual(
            response.context,
            {
                'issue_regexes': models.IssueRegex.objects.filter(issue__in=authorized_issues),
                'issues': authorized_issues,
            },
        )


class TestIssueRegexReadGroup(TestIssueRegexAnonymous):
    """Issue regex test case. Read groups."""

    anonymous = False
    groups = ['group_a']


class TestIssueRegexWriteGroup(TestIssueRegexAnonymous):
    """Issue regex test case. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueRegexAllGroups(TestIssueRegexAnonymous):
    """Issue regex test case. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueResolveAnonymous(utils.KCIDBTestCase):
    """Issue resolve test case. Anonymous."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml'
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_issues = models.Issue.objects.filter(id__in=self.issues_authorized[method])
        no_auth_issues = models.Issue.objects.exclude(id__in=self.issues_authorized[method])

        checks = [
            (auth_issues, 'No authorized issues'),
            (no_auth_issues, 'No unauthorized issues'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_issue_resolve(self):
        """Test resolving issues."""
        self._ensure_test_conditions('write')

        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous
            self.assertIsNone(issue.last_edited_by)

            # Resolve it
            status_code = 302 if authorized else 404
            self.assert_authenticated_post(status_code, 'change_issue', f'/issue/{issue.id}/resolve', user=self.user)
            issue.refresh_from_db()
            resolved = True if authorized else False  # Shouldn't change if not authorized
            self.assertEqual(resolved, issue.resolved)
            self.assertEqual(self.user if authorized else None, issue.last_edited_by)

            # Un-resolve it
            self.assert_authenticated_post(status_code, 'change_issue', f'/issue/{issue.id}/resolve', user=self.user)
            issue.refresh_from_db()
            self.assertEqual(False, issue.resolved)
            self.assertEqual(self.user if authorized else None, issue.last_edited_by)


class TestIssueResolveReadGroup(TestIssueResolveAnonymous):
    """Issue resolve test case. Read group."""

    anonymous = False
    groups = ['group_a']


class TestIssueResolveWriteGroup(TestIssueResolveAnonymous):
    """Issue resolve test case. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueResolveAllGroups(TestIssueResolveAnonymous):
    """Issue resolve test case. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueViewAnonymous(utils.KCIDBTestCase):
    """Issue views test case. Anonymous."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml'
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_issues = models.Issue.objects.filter(id__in=self.issues_authorized[method])
        no_auth_issues = models.Issue.objects.exclude(id__in=self.issues_authorized[method])

        checks = [
            (auth_issues, 'No authorized issues'),
            (no_auth_issues, 'No unauthorized issues'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_get_list_all(self):
        """Test get all Issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_on=None)

        response = self.client.get('/issue/-/list')
        self.assertContextEqual(
            response.context,
            {
                'issues': authorized_issues,
                'resolved': None,
                'search': '',
            },
        )

        authorized_issues.update(resolved_on=datetime_bool(True))

        response = self.client.get('/issue/-/list')
        self.assertContextEqual(
            response.context,
            {
                'issues': authorized_issues,
                'resolved': None,
                'search': '',
            },
        )

    def test_get_list_unresolved(self):
        """Test get all Issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_on=None)

        response = self.client.get('/issue/-/list?resolved=false')
        self.assertContextEqual(
            response.context,
            {
                'issues': authorized_issues,
                'resolved': False,
                'search': '',
            },
        )

        # All unresolved, resolved should be empty
        response = self.client.get('/issue/-/list?resolved=true')
        self.assertContextEqual(
            response.context,
            {
                'issues': [],
                'resolved': True,
                'search': '',
            },
        )

    def test_get_list_resolved(self):
        """Test get all Issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_on=datetime_bool(True))

        response = self.client.get('/issue/-/list?resolved=true')
        self.assertContextEqual(
            response.context,
            {
                'issues': authorized_issues,
                'resolved': True,
                'search': '',
            },
        )

        # All resolved, unresolved should be empty
        response = self.client.get('/issue/-/list?resolved=false')
        self.assertContextEqual(
            response.context,
            {
                'issues': [],
                'resolved': False,
                'search': '',
            },
        )

    def test_get_list_search(self):
        """Test get all Issues. Search argument."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_on=datetime_bool(True))

        response = self.client.get('/issue/-/list?resolved=true&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description='Issue Public'),
                'resolved': True,
                'search': 'Issue Public',
            },
        )

        response = self.client.get('/issue/-/list?resolved=true&search=https://issue.public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(ticket_url='https://issue.public'),
                'resolved': True,
                'search': 'https://issue.public',
            },
        )

    def test_get_list_search_resolved(self):
        """Test search for resolved issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_on=datetime_bool(True))

        response = self.client.get('/issue/-/list?resolved=true&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description='Issue Public'),
                'resolved': True,
                'search': 'Issue Public',
            },
        )

        response = self.client.get('/issue/-/list?resolved=false&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': [],
                'resolved': False,
                'search': 'Issue Public',
            },
        )

    def test_get_list_search_unresolved(self):
        """Test search for unresolved issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_on=None)

        response = self.client.get('/issue/-/list?resolved=true&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': [],
                'resolved': True,
                'search': 'Issue Public',
            },
        )

        response = self.client.get('/issue/-/list?resolved=false&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description='Issue Public'),
                'resolved': False,
                'search': 'Issue Public',
            },
        )

    def test_get_list_search_all(self):
        """Test search for all issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_on=None)

        response = self.client.get('/issue/-/list?search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description='Issue Public'),
                'resolved': None,
                'search': 'Issue Public',
            },
        )

        authorized_issues.update(resolved_on=datetime_bool(True))

        response = self.client.get('/issue/-/list?search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description='Issue Public'),
                'resolved': None,
                'search': 'Issue Public',
            },
        )

    def test_get_single(self):
        """Test get a single issues."""
        self._ensure_test_conditions('read')

        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        for issue in models.Issue.objects.all():
            response = self.client.get(f'/issue/{issue.id}')

            if issue not in authorized_issues:
                self.assertEqual(404, response.status_code)
                continue

            authorized_checkouts = models.KCIDBCheckout.objects.filter(
                id__in=self.checkouts_authorized['read'],
                issues=issue,
            )

            authorized_tests = models.KCIDBTest.objects.filter(
                build__checkout__in=authorized_checkouts
            )

            authorized_hosts = (
                authorized_tests.values('environment__fqdn')
                                .annotate(total_hits=Count('environment__fqdn'))
                                .order_by('-total_hits')
            )

            authorized_archs = (
                authorized_tests.values('build__architecture')
                                .annotate(total_hits=Count('build__architecture'))
                                .order_by('-total_hits')
            )

            for arch in authorized_archs:
                arch['name'] = dict(models.pipeline_models.ArchitectureEnum.choices)[arch['build__architecture']]

            self.assertContextEqual(
                response.context,
                {
                    'issue': issue,
                    'checkouts': authorized_checkouts,
                    'affected_hosts': authorized_hosts,
                    'affected_archs': authorized_archs,
                    'tests': authorized_tests,
                    'issues': authorized_issues,
                    'issue_kinds': models.IssueKind.objects.all(),
                    'git_trees': models.GitTree.objects.filter(
                        kcidbcheckout__id__in=self.checkouts_authorized['read']),
                    'policies': models.Policy.objects.filter(id__in=self.policies_authorized['write']),
                },
            )


class TestIssueViewReadGroup(TestIssueViewAnonymous):
    """Issue resolve test case. Read group."""

    anonymous = False
    groups = ['group_a']


class TestIssueViewWriteGroup(TestIssueViewAnonymous):
    """Issue resolve test case. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueViewAllGroups(TestIssueViewAnonymous):
    """Issue resolve test case. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']
